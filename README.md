# General
ReVelatioNs - Resonant Velocity Tuning of Solitary States in Complex Networks.

Content: Numerical experiments and supplementary material for [paper in preparation](https://arxiv.org/abs/2401.06483).

Authors: Jakob Niehues (corresponding), Serhiy Yanchuk, Rico Berner, Jürgen Kurths, Frank Hellmann and Mehrnaz Anvari

Scope: Test your own power grid models for solitary states using predictions of our findings about structure function relationship.

The thesis can be found in the folder "thesis" and at https://doi.org/10.13140/RG.2.2.24856.88320.

See also https://linktr.ee/jakobniehues

# previous versions

Network simulations
https://gitlab.pik-potsdam.de/jakobn/normal-form

Toy models and self-consistency
https://gitlab.pik-potsdam.de/jakobn/toy-model

# Zenodo
[Zenodo](https://zenodo.org/records/12636090)
