# To Do

rerun after adding m to some Z and derivative fcts

rerun after fixing mistake in derivative formula!

Look at some more trajectories

Tsit5() to speed up
node parameter change:
try changing P at the solitary node, see if you get less intersections
continuation study? (further work)
predictions for std/amplitude of modulation

write equations for Mehrnaz etc. (amplitude factors)

global vs local?

redo and expand testing

rerun network generation

rerun scripts and save plots etc.

rerun omega search with more steps , e.g. 0.1

plot toy model graphs

fix saving location of simulation data

check colorant undef error in network_generation.jl lines 238 and 248

check if all to all and star graph work for n=0, and how plots look

check if loading of saved dicts works (how to fix if necessary)

save also op from now on

make "if idx is scalar" then "idx = [idx]" or make type requirements

file an issue on TreeNodeClassification GitHub: corner case of tree yields roots (should be inner tree nodes?)

docstrings

## Root Finding
make roots faster by giving explicit derivative

optional: make frequency the first argument, so you can pass parameters more easily

## Network Generation

test check_eigenvalues_MM for system with solitary mass that differs from rest

implement inhomogeneous P and K in generation

at some point: pull request to SyntheticPowergrids, switch to that

maybe: implement several solitary oscillators later (try to make things as general as possible from the start, with the reduced networks; we will have to see about Z)

## Network Reduction
try other approximations of the Laplacian

Toy models include Slack nodes: they should be cut out from Laplacian as dimension but kept in coupling s to their neighbors

put check_Laplacian_properties in testing section, check in paper simulation and data analysis

## Several Solitaries
set several solitaries aside for now

make equity approximate for several solitaries (delta fct)

derive criteria for stability in several dimensions

make function for several distinct solitaries (avoid delta function), and for several solitaries with same frequency (mind phase shift)

what if solitaries are neighbors? Just mean zero coupling?

# Insights
## Generate Networks
keep in mind: so far only SwingEq (SwingEqLVS has to be used here) and homogeneous damping and inertia

Slack nodes: need to be skipped when checking op for frequencies = 0

## Generate Laplacian
Jacobian via ForwardDiff and rhs (NetworkDynamics) gives Jacobian in terms of (Re u_1, Im u_1, \omega_1, Re u_2, etc.)

Better way is to access op[:,:\varphi] as vector of phase angles and generate Laplacian by iterating over e.g. lower triangle etc.

## Self-consistency function
you can pull the sum over neighbors in and pre-compute the amplitude_factors, that are the sum (over all neighbor indices) of the squared eigenvector components and squared coupling strengths

## Several Solitaries:
if frequencies differ, the sum over solitary indices reduces to the same as in the case of only one solitary

if two solitary frequencies are actually the same, the new summand only differs in the amplitude_factors!

you can just compute the synchronization phase velocity and then the corotating phase velocities from there

## Saving and Loading
save and load with FileIO, else Types might not be recognized, e.g. node_colors

# Errors and Bugs
230317 find_operationspoint :rootfind stopped working, :nlsolve works

you need to use op.vec not op for solve(ODEProblem(rhs(og), op.vec, t_span), Rodas4()) or Tsit5()

you now have to check succes via SciMLBase.successful_retcode(sol); sol.retcode== :Success is broken now!