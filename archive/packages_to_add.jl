# comparison to ...wo_activate.jl
# Plots is missing
# GraphPlot also because it is commented out


run 
# julia +1.6.3
# ] activate ~/PIK_Nextcloud/jakobn/revelations
# include(joinpath(@__DIR__,"packages_to_add.jl"))
# 
using Pkg

Pkg.activate("~/PIK_Nextcloud/jakobn/revelations")

Pkg.add(Pkg.PackageSpec(;name="PowerDynamics", version="3.1.4"))
Pkg.add(Pkg.PackageSpec(;name="SyntheticNetworks", version="0.3.0"))
Pkg.add(Pkg.PackageSpec(;name="StatsBase", version="0.33.21"))
Pkg.add(Pkg.PackageSpec(;name="Graphs", version="1.8.0"))
# is in Standard Libraray Pkg.add(Pkg.PackageSpec(;name="LinearAlgebra", version=""))
Pkg.add(Pkg.PackageSpec(;name="ForwardDiff", version="0.10.34"))
Pkg.add(Pkg.PackageSpec(;name="DifferentialEquations", version="7.7.0"))
Pkg.add(Pkg.PackageSpec(;name="Colors", version="0.12.10"))

Pkg.add(url="https://github.com/PIK-ICoNe/TreeNodeClassification")
# needed to install version "0.1.0", due to wrong naming (see Github issue https://github.com/PIK-ICoNe/TreeNodeClassification/issues/9 )
# actually this is 1.0.0
# ] status in default environment returns: [55be299f] TreeNodeClassification v0.1.0 `https://github.com/PIK-ICoNe/TreeNodeClassification#main`

###  not needed? Pkg.add(Pkg.PackageSpec(;name="GraphPlot", version="0.5.2"))
Pkg.add(Pkg.PackageSpec(;name="GraphMakie", version="0.3.6"))
Pkg.add(Pkg.PackageSpec(;name="CairoMakie", version="0.8.13"))
Pkg.add(Pkg.PackageSpec(;name="FixedPointNumbers", version="0.8.4"))
Pkg.add(Pkg.PackageSpec(;name="OrderedCollections", version="1.4.1"))
Pkg.add(Pkg.PackageSpec(;name="JLD", version="0.13.3"))
Pkg.add(Pkg.PackageSpec(;name="FileIO", version="1.16.0"))
Pkg.add(Pkg.PackageSpec(;name="DataStructures", version="0.18.13"))
Pkg.add(Pkg.PackageSpec(;name="Images", version="0.25.2"))
# also standard??? Or part of LinearAlgebra.jl which is stndard? Pkg.add(Pkg.PackageSpec(;name="Random", version=""))
Pkg.add(Pkg.PackageSpec(;name="IntervalArithmetic", version="0.20.8"))
Pkg.add(Pkg.PackageSpec(;name="IntervalRootFinding", version="0.5.11"))
# also standard??? Pkg.add(Pkg.PackageSpec(;name="Distributed", version=""))
Pkg.add(Pkg.PackageSpec(;name="Distributions", version="0.25.80"))
Pkg.add(Pkg.PackageSpec(;name="Roots", version="2.0.10"))
Pkg.add(Pkg.PackageSpec(;name="LaTeXStrings", version="1.3.0"))


# Pkg.add(Pkg.PackageSpec(;name="", version=""))