# outdated (prior to bugfix)
"""
using CairoMakie
using JLD
using OrderedCollections
using LinearAlgebra

include(joinpath(@__DIR__, "../src/self_consistency.jl"))
include(joinpath(@__DIR__,"../src/laplacian.jl"))
include(joinpath(@__DIR__, "../src/network_generation.jl"))

# as example: the star graph

ensemble_path = joinpath(@__DIR__,"../ensembles/")
ensemble_path_local = joinpath(ensemble_path,"star_graph_toy_models/")
data_path = joinpath(@__DIR__,"../data/")
data_path_local = joinpath(data_path, "../random_pgs")
plot_path = joinpath(@__DIR__,"../plots/")


size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
scaler = 1
size_pt = scaler .* 72 .* size_inches

H = .5; P = 1.; D = .1; Ω = 1.; Γ = 1.; V = 1.;
node_parameters_dict = Dict(:H=>H, :P=>P, :D=>D, :Ω=>Ω, :Γ=>Γ, :V=>V)
K = 6.

n = 6
N = n+2

one_solitary_idx = 1

pg, RHS, op = find_power_grid_path(ensemble_path_local, "star_graph_toy_model_n_"*string(n))
N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))

solitary_node = pg.nodes[one_solitary_idx]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])

reduced_eigenvalues
# they match the expected analytical eigenvalues
K .* [0, 1, 1, 1, 1, 1, n+1]

amplitude_factors = all_amplitude_factors[1]
# they match the expected analytical amplitude factors:
K^2 .* [1/(n+1), 0, 0, 0, 0, 0, n^2/(n^2+n)]

# check corotating_frequency
α = node_parameters_dict[:D]
m = 2* node_parameters_dict[:H]
α_solitary = solitary_node.D
m_solitary = 2 * solitary_node.H
P_solitary = solitary_node.P
example_frequency = 6.1
corotating_frequency(example_frequency, α, α_solitary, N, N_slacks)


function analytic_mean_power_flow_star_graph_toy_model(K, solitary_frequency, m, α, N)
    ω_c = corotating_frequency(solitary_frequency, α, α_solitary, N, 0)
    p = 0.5 * K^2 * α * ω_c * (1/ (n+1) / ( (m * ω_c^2)^2 + α^2 * ω_c^2 ) + n / (n+1) / ( (K*(n+1) - m * ω_c^2)^2 + α^2 * ω_c^2 ) )
end

function semi_analytic_truncated_mean_power_flow_star_graph_toy_model(reduced_eigenvalues, amplitude_factors, solitary_frequency, m, α, N)
    ω_c = corotating_frequency(solitary_frequency, α, α_solitary, N, 0)
    p = 0.5 * α * ω_c * (amplitude_factors[1] / ( (reduced_eigenvalues[1] - m * ω_c^2)^2 + α^2 * ω_c^2 ) + amplitude_factors[end] / ( (reduced_eigenvalues[end] - m * ω_c^2)^2 + α^2 * ω_c^2 ) )
end

ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

ω_range = ω_min:0.001:ω_max
p_range = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, α, α_solitary, N, N_slacks), ω_range)


Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range)
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range, p_range)

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))

Fig

# does it look like the analytical solution? no.

analytic_p_range = map(ω -> analytic_mean_power_flow_star_graph_toy_model(K, ω, m, α, N), ω_range)
Makie.lines!(ax, ω_range, analytic_p_range, label=L"analytic $\langle p \rangle$")
Fig

semi_analytic_truncated_p_range = map(ω -> semi_analytic_truncated_mean_power_flow_star_graph_toy_model(reduced_eigenvalues, amplitude_factors, ω, m, α, N), ω_range)
Makie.lines!(ax, ω_range, semi_analytic_truncated_p_range, label=L"semi-analytic truncated $\langle p \rangle$")
Fig
# is the same as analytical

# does the sum do the right thing?
reduced_eigenvalues[[1,end]]
amplitude_factors[[1,end]]
truncated_p_range = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues[[1,end]], amplitude_factors[[1,end]], ω, α, α_solitary, N, N_slacks), ω_range)
Makie.lines!(ax, ω_range, truncated_p_range, label=L"truncated $\langle p \rangle$")
Fig

# contribution of small modes
reduced_eigenvalues[2:end-1]
amplitude_factors[2:end-1]
small_modes_p_range = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues[2:end-1], amplitude_factors[2:end-1], ω, α, α_solitary, N, N_slacks), ω_range)
Makie.lines!(ax, ω_range, small_modes_p_range, label=L"small modes $\langle p \rangle$")
Fig

# must be a bug!
# why don't they add up?

zero_mode_p_range = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues[1], amplitude_factors[1], ω, α, α_solitary, N, N_slacks), ω_range)
high_mode_p_range = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues[end], amplitude_factors[end], ω, α, α_solitary, N, N_slacks), ω_range)

Fig_2 = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax_2 = Makie.Axis(Fig_2[1,1], xlabel=L"$\omega$")
Makie.lines!(ax_2, ω_range, small_modes_p_range, label=L"small modes $\langle p \rangle$", linestyle=:dot)
Makie.lines!(ax_2, ω_range, zero_mode_p_range, label=L"zero mode $\langle p \rangle$", linestyle = :dash)
Makie.lines!(ax_2, ω_range, high_mode_p_range, label=L"high mode $\langle p \rangle$", linestyle = :solid)
Makie.lines!(ax_2, ω_range, small_modes_p_range + zero_mode_p_range + high_mode_p_range, label=L"all modes $\langle p \rangle$", linestyle = :dashdot)
Makie.xlims!(ax_2, [ω_min, ω_max])
Makie.ylims!(ax_2, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
leg_2 = Makie.Legend(Fig_2, ax_2)
Fig_2

save(joinpath(plot_path,"Fig_2.pdf"), Fig_2)

# test sum in self_consistency functions
ω_c = corotating_frequency(example_frequency, α, α_solitary, N, N_slacks)

function foo(α)
    ω_c = corotating_frequency(example_frequency, α, α_solitary, N, N_slacks)
    p = 0.5 * α * ω_c *
        10000
end

foo(α)

reduced_eigenvalues
amplitude_factors
m * ω_c^2
reduced_eigenvalues .- m * ω_c^2
(reduced_eigenvalues .- m * ω_c^2).^2
(reduced_eigenvalues .- m * ω_c^2).^2 .+ α^2 * ω_c^2
1 ./ ( (reduced_eigenvalues .- m * ω_c^2).^2 .+ α^2 * ω_c^2)
ones(n+1) ./ ( (reduced_eigenvalues .- m * ω_c^2).^2 .+ α^2 * ω_c^2) 
amplitude_factors ./ ( (reduced_eigenvalues .- m * ω_c^2).^2 .+ α^2 * ω_c^2) 

broadcast(/, amplitude_factors, (reduced_eigenvalues .- m * ω_c^2).^2 .+ α^2 * ω_c^2)

map(/, amplitude_factors, (reduced_eigenvalues .- m * ω_c^2).^2 .+ α^2 * ω_c^2)
sum(map(/, amplitude_factors, (reduced_eigenvalues .- m * ω_c^2).^2 .+ α^2 * ω_c^2))

amplitude_factors * 1 ./ ((reduced_eigenvalues .- m * ω_c^2).^2 .+ α^2 * ω_c^2)

dot(amplitude_factors, 1 ./ ((reduced_eigenvalues .- m * ω_c^2).^2 .+ α^2 * ω_c^2) )

"""
