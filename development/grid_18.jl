# What about grid 18?
# this makes the grid generation reproducable (tested it!)
grid_idx = 18
Random.seed!(grid_idx)
# pg, op = random_PD_grid(N, node_parameters_dict, K; )

n_attempts=100

growth_parameters=[1, 1/5, 3/10, 1/3, 1/10, 0.0]
active_power_distribution="random_half"
balancing_method="None"
balancing_parameters=[Nothing]

# decide node type and unpack parameters
node_type = SwingEqLVS
# H, P, D, Ω, Γ, V = node_parameters
# node_parameters_dict = Dict(:H=>H, :P=>P, :D=>D, :Ω=>Ω, :Γ=>Γ, :V=>V)
    


if active_power_distribution == "random_half"
    @assert (N%2)==0 "With balancing method random_half, N has to be even to ensure power balance with half of the nodes having power +(-)P." 
    P_vec = ones(N)
    turn_idx = sample(1:N, N÷2, replace=false)
    for n in turn_idx
        P_vec[n] = -1
    end
    P_vec = P .* P_vec
    @assert sum(P_vec)==0 "power imbalance!"
else
    @assert (false) "Active power distribution "* active_power_distribution * "not yet implemented."
end

n0, p, q, r, s, u = growth_parameters
counter=1
# n_attempts to generate a stable grid
pg, op = while counter <= n_attempts
    RPG = RandomPowerGrid(N, n0, p, q, r, s, u)  # last parameter u is currently not implemented
    pg_graph = generate_graph(RPG)      
    e = Graphs.edges(pg_graph)

    # collects all edges and turns them into StaticLines
    from_vec = Graphs.src.(e)
    to_vec = Graphs.dst.(e)

    lines = Array{Any}(undef, length(e))
    nodes = Array{Any}(undef, Graphs.nv(pg_graph))

    for l in 1:length(e)
        lines[l] = StaticLine(from = from_vec[l], to = to_vec[l], Y = -1im*K) 
    end

        
            
    if balancing_method == "None"
        for n in 1:Graphs.nv(pg_graph)
            nodes[n] = node_type(;node_parameters_dict...,P=P_vec[n])
        end
        newnodes = nodes
    else @assert (false) "Balancing method "* balancing_method * "not yet implemented."
        # newnodes[end] = SlackAlgebraic(U = complex(1.0))
    end

    pg = PowerGrid(newnodes, lines)
    
    try # necessary because nlsolve does not converge for grid 18 at first try in newer version 
        op = find_operationpoint(pg, sol_method=:nlsolve)
        # check if acceptable operation point and return if so
        if all(isapprox.(op[:, :v], 1.0, atol=0.01)) && all(isapprox.(op[:, :ω], 0, atol=1e-9))
            _, stable = check_eigenvalues_MM(pg, op)
            if stable 
                sol = simulate_from_state(pg, op.vec, (0.,25.))
                if SciMLBase.successful_retcode(sol)
                    return pg, op
                else
                    print("Simulation unsuccessful.")
                end
            end
        end
    catch
    end
    counter += 1
end 
    
pg

write_powergrid(pg, joinpath( ensemble_path, "random_pgs", "grid_"*string(grid_idx)*".json"), Json)
# pg_graph = graph_convert(pg.graph)
pg_graph = pg.graph
classes[grid_idx] = full_node_classification(pg_graph, N, 5) # second argument: maxiter, third argument: treshold (above a sprout is dense)
where_dense_sprouts[grid_idx] = findall(i->i=="Dense Sprout",classes[grid_idx])
length_dense_sprouts = length(where_dense_sprouts[grid_idx])
n_dense_sprouts[grid_idx] = length_dense_sprouts
where_dense_roots[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
dense_root_degrees[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
degrees = Graphs.degree(pg_graph)
mean_degrees[grid_idx] = mean(degrees)
node_colors[grid_idx] = color_nodes(pg)
node_markers[grid_idx] = power_shape_nodes(pg)

# dense root degrees, counting for each dense sprout
position = 1
for node_idx in where_dense_sprouts[grid_idx]
    root = Graphs.neighbors(pg_graph, node_idx)[1]
    where_dense_roots[grid_idx][position] = root
    root_neighbors = Graphs.neighbors(pg_graph, root)
    dense_root_degrees[grid_idx][position] = length(root_neighbors)
    position += 1
end

# counting each dense root only once
where_dense_roots_tmp = where_dense_roots[grid_idx]
where_dense_roots_unique_tmp = OrderedSet.(where_dense_roots_tmp)
position = 1
length_dense_roots_unique = length(where_dense_roots_unique_tmp)
dense_root_degrees_unique[grid_idx] = Vector{Int}(undef, length_dense_roots_unique)
position = 1
for node_idx in where_dense_roots[grid_idx]
    dense_root_degrees_unique[grid_idx][position] = degrees[node_idx]
    position += 1
end

# gplot = my_graph_plot(pg,[])
# FileIO.save(plot_path*"grid_"*string(grid_idx)*".pdf", gplot)







find_operationpoint(pg, sol_method=:rootfind)
find_operationpoint(pg, ic_guess, sol_method=:nlsolve)


# compare old and new grids
ic_guess = [0.9919549228167832, 0.12659159174114154, -7.687911765980147e-17, 0.9999550285077433, -0.009483720898702074, -6.977363279877563e-17, 0.9571909716583068, -0.2894571536097062, -6.399185709651159e-17, 0.9962080860313004, 0.08700258229420291, -7.625620465955216e-17, 0.9989838409725779, -0.045069784508931836, -7.661506447012622e-17, 0.9978966445984704, 
        -0.06482504685012166, -6.883412653264711e-17, 0.9733673637571162, -0.2292509000473682, -6.056324579604008e-17, 0.7751691443350538, -0.6317537476508286, -5.259103815656742e-16, 0.9197804796223084, 0.392433267328042, -9.755672726581811e-17, 0.9976659663568184, -0.06828337699119634, -6.880632141858046e-17, 0.9435891842399613, 0.3311184854177891, -9.163908580135036e-17, 0.9930635545950538, -0.11757880988955044, -6.707513446283975e-17, 0.7823384863715885, 0.622853508251995, -5.289422792221767e-17, 0.9986970039843994, 0.05103228617836819, -8.186807046527434e-17, 0.8752050685026175, 0.48375209360511817, -8.812701239308594e-17, 0.9774212673205481, -0.21129994365711433, -9.494830499533074e-17, 0.9834970089663465, 0.18092438573685013, -7.754268447396978e-17, 0.9970546424427109, -0.07669445862279903, -7.058340777994409e-17, 0.8233554233537895, 0.5675260759065688, 1.9365778848900604e-17, 0.913626631334242, 0.4065542749951738, -9.552264706153488e-17, 0.9999733125119966, 0.007305769212822939, -7.763937326680429e-17, 0.9444590219708812, -0.32862920718921623, -4.779977651434448e-17, 0.9964793378761182, -0.08383870935301418, -6.802199116575134e-17, 0.9868094815453742, -0.16188590776270856, -6.889263487852936e-17, 0.9904067479536816, -0.13818275437918814, -6.823225719458926e-17, 0.9864207464873042, -0.16423797033401263, -6.312580387101054e-17, 0.7151588277401144, -0.6989619811587692, -6.61660004502993e-16, 0.9603750420455494, 0.2787109230296074, -8.379376991140918e-17, 0.9810840989618695, -0.19358200010378768, -7.426926095473435e-17, 0.9605433589316709, 0.2781302853201529, -8.641410914033706e-17, 0.9875495748436938, 0.15730809650506217, -6.931502361417592e-17, 0.9869791939073145, -0.16084797416838706, -7.303040444203499e-17, 0.9923711044971867, 0.12328662116807378, -8.203137694344327e-17, 0.9435891842399613, 0.3311184854177892, -9.258157597532803e-17, 0.9965149957397349, 0.08341380740526443, -6.79166391423405e-17, 0.7634170117102217, -0.6459059267659972, -5.966414198173699e-16, 0.8920468051898034, 0.45194302445183426, -1.009682464751313e-16, 0.7139942288297246, -0.7001515844428643, -6.092932365443265e-16, 0.9914759076580083, -0.13029015516812745, -7.085419885330941e-17, 0.8415102388425276, 0.5402411664462442, -6.083130065287431e-18, 0.7197895840907494, 
        -0.6941923037851057, -6.384301562200337e-16, 0.9917886573788418, -0.12788768155956395, -6.654232322233673e-17, 0.9924119490440827, -0.12295740479748034, -6.638384324933799e-17, 0.9571909716583068, -0.28945715360970614, -7.505806107187386e-17, 0.9993237834457082, 0.03676922408430437, -6.89798951759363e-17, 0.7236114560597603, 0.690207548972829, 2.625871498167633e-17, 0.9435891842399613, 0.3311184854177892, -1.004141475628399e-16, 0.9512372323213305, -0.3084602532671938, -6.86065587708332e-17, 0.929318392263041, -0.36927946842688514, -6.599198279034514e-17, 0.6590347926054019, -0.7521124531182564, -7.362283595044905e-16, 0.7104207641959186, -0.7037771932929437, -1.4502490123805807e-16, 0.7907201274931858, -0.6121778172860941, -3.767532146623477e-16, 0.9920458849338152, -0.1258767738142726, -6.498268026490031e-17, 0.9285337092515977, -0.37124809869341513, -1.620521416172997e-16, 0.804246174218361, 0.5942962992103661, -6.998184679930896e-17, 0.9991793427272755, -0.04050482770110252, -7.697445305590686e-17, 0.9731650370801582, 0.23010825844541263, -8.393395508475585e-17, 0.973367363757116, -0.22925090004736817, -5.884982129025833e-17, 0.6133030729492042, 0.7898476693078704, 1.0916105013132029e-16, 0.9979616470499688, -0.06381654187841831, -6.168919437817385e-17, 0.9950924718068854, 0.09894934336957205, -6.852551796367269e-17, 0.7298274545031466, 0.683631396772752, 9.338357814707548e-17, 0.9980413054357344, -0.06255839387437563, -7.65018751172066e-17, 0.8604476256857133, -0.5095388929726808, -9.062739267465985e-17, 0.9745244661689055, 0.2242812182020951, -8.789038944956349e-17, 0.6342194781672217, -0.7731530595640846, -8.024814850172138e-16, 0.9991793427272754, -0.04050482770110252, -7.833869748326613e-17, 0.9683054463411622, -0.24976901846716104, -6.111098526845542e-17, 0.9849237330992592, 0.17298913254255457, -8.540501938771777e-17, 0.47308370691328716, 0.8810174835116398, 1.0184559720897377e-14, 0.9999039114812996, -0.013862460257964167, -6.870471340203015e-17, 0.9784533135830824, -0.20646818916795026, -7.758136809876606e-17, 0.9992314177643404, 0.039199154999470645, -6.891961110592344e-17, 0.6091639354805704, -0.7930443239251076, -5.955242450107465e-16, 0.7530766265918066, -0.6579328191244994, -3.621267360596745e-16, 0.9934635719810682, 0.11414959985310225, -8.844430078148538e-17, 0.999864522485727, -0.01646015414251168, -7.731309993273137e-17, 0.8955601670639134, -0.4449404310337082, -6.788079258548657e-17, 0.8764776197764584, -0.4814426051265099, -1.6317750085909642e-16, 0.5831881212101182, -0.8123371315404835, -9.928676933896757e-16, 0.9659715978290169, -0.25864816293115683, -7.27987962357397e-17, 0.5601517983074037, 0.8283899823470829, -1.4552411830866575e-16, 0.9855778969752137, 0.16922236552513106, -1.0280457751665717e-16, 0.9986970039843993, 0.051032286178368184, -8.150232533575953e-17, 0.8865225176334437, -0.46268545009419393, -1.40672758041276e-16, 0.41425212451436083, 0.9101621708988662, 8.144276229569294e-15, 0.6328880977633577, -0.7742432794086661, -7.738351303872503e-16, 0.7172517164170352, 0.6968141612344165, 1.8030456839319068e-17, 0.9932339068154384, -0.11613098790653355, -8.136795193582004e-17, 0.9395870642913157, 0.34231001828230695, -8.345431181435742e-17, 0.7634896516271054, -0.6458200615173918, -3.7441092989088667e-16, 0.9350983005475053, -0.35438844269131264, -7.573570764390352e-17, 0.9860206888395304, -0.16662293113621604, -6.84970747120752e-17, 0.955893402926293, -0.29371381009751324, -7.106597402147615e-17, 0.9979616470499687, -0.06381654187841826, -6.11893258769844e-17, 0.9897091243894217, 0.14309384717845297, -8.241947286065534e-17, 0.9571909716583067, -0.2894571536097061, -8.435784259493196e-17, 0.9580384772414506, -0.2866396276248086, -6.602795357292497e-17, 0.6056810086607285, 0.7957075566737631, 3.702699890574686e-17, 0.7172517164170351, 0.6968141612344165, 1.8030474778798602e-17]

# grid 18 are different, probably due to several tries for stable op
op_grid_18 = FileIO.load(joinpath(ensemble_path,"random_pgs", "op_grid_18.jld"))
op_old = op_grid_18["op"]
op_old.grid == pg
pg_old = op_old.grid
pg_old.graph == pg.graph
pg_old.nodes == pg.nodes
pg_old.lines == pg.lines

grid_match = [find_power_grid_path(joinpath(ensemble_path, "random_pgs"), grid_idx)[1] == find_power_grid_path("C:/Users/niehu/Dropbox/UNI/Masterarbeit/Julia/toy-model/data/ensemble", grid_idx)[1] for grid_idx in setdiff(1:n_grids, 18)]
graph_match = [find_power_grid_path(joinpath(ensemble_path, "random_pgs"), grid_idx)[1].graph == find_power_grid_path("C:/Users/niehu/Dropbox/UNI/Masterarbeit/Julia/toy-model/data/ensemble", grid_idx)[1].graph for grid_idx in setdiff(1:n_grids, 18)]
nodes_match = [find_power_grid_path(joinpath(ensemble_path, "random_pgs"), grid_idx)[1].nodes == find_power_grid_path("C:/Users/niehu/Dropbox/UNI/Masterarbeit/Julia/toy-model/data/ensemble", grid_idx)[1].nodes for grid_idx in setdiff(1:n_grids, 18)]
lines_match = [find_power_grid_path(joinpath(ensemble_path, "random_pgs"), grid_idx)[1].lines == find_power_grid_path("C:/Users/niehu/Dropbox/UNI/Masterarbeit/Julia/toy-model/data/ensemble", grid_idx)[1].lines for grid_idx in setdiff(1:n_grids, 18)]

print(grid_match)
print(lines_match)
print(graph_match)
print(nodes_match)
# graphs, lines and nodes (thus also P distribution) match for 1:100 w/o 18
# for some reason power grids do not match
SwingEqLVS(;node_parameters_dict..., P=-1) == SwingEqLVS(;node_parameters_dict..., P=1)

grid_idx = 1
pg_1_new = find_power_grid_path(joinpath(ensemble_path, "random_pgs"), grid_idx)[1]
pg_1_old = find_power_grid_path("C:/Users/niehu/Dropbox/UNI/Masterarbeit/Julia/toy-model/data/ensemble", grid_idx)[1]
pg_1_old == pg_1_new
pg_1_new.graph == pg_1_old.graph
pg_1_new_P_vec = [node.P for node in pg_1_new.nodes]
pg_1_old_P_vec = [node.P for node in pg_1_old.nodes]
pg_1_new_P_vec .== pg_1_old_P_vec
pg_1_new_P_vec == pg_1_old_P_vec
pg_1_new.nodes == pg_1_old.nodes
pg_1_new.lines == pg_1_old.lines
println(pg_1_new)
println(pg_1_old)
