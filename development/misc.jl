using PowerDynamics
using SyntheticNetworks
using TreeNodeClassification
using StatsBase
using Graphs
using LinearAlgebra
# using ForwardDiff
using DifferentialEquations # or # using OrdinaryDiffEq
using Roots
using IntervalRootFinding
using IntervalArithmetic
using Colors


include(joinpath(@__DIR__,"../src/network_generation.jl"))
include(joinpath(@__DIR__,"../src/laplacian.jl"))
include(joinpath(@__DIR__,"../src/self_consistency.jl"))
# initialize parameters

node_type = SwingEqLVS
# this is fixed in random_PD_grid at the moment

H = .5; P = 1.; D = .1; Ω = 1.; Γ = 1.; V = 1.;
node_parameters_dict = Dict(:H=>H, :P=>P, :D=>D, :Ω=>Ω, :Γ=>Γ, :V=>V)
K = 6.


# old ansatz for reduced Laplacian
# this has some problems:
    # deletat! has a ! and replaces to original Vector
    # some indexes are not the same in the original and reduced network
    # some lines have to be skipped because they connect to the solitary node
    # easier to compute Laplacian and reduce it

    reduced_phases = deleteat!(phases, solitary_idxs)
    N_reduced = length(reduced_phases)

    pg.lines
    pg.lines[1].to
    pg.lines[1].from
    pg.lines[1].Y
    -imag(pg.lines[1].Y)

    for line in pg.lines
        from_idx = line.from
        to_idx = line.to
        K = -imag(line.Y)
        reduced_Laplacian[i,j] = - K * cos(reduced_phases[from_idx] - reduced_phases[to_idx])
    end
    # problem: you have to use a reduced coupling matrix
    # and know the new indices of the lines (some change after reducing)
    # easier if you make Laplacian of whole and reduce then




    
# implement Slack nodes: delete the row and column but keep diagonal entries
slack_idxs = findall(x -> (typeof(x) == SlackAlgebraic), pg.nodes)
dynamic_idxs = setdiff(1:N, slack_idxs)
dynamic_Laplacian = Laplacian[dynamic_idxs, dynamic_idxs]

all(all.(broadcast(idx -> (idx .!== [1,2,3]), [6,4,5] ) ) )

[2,5,6,7,8,9] .- sum(broadcast(x -> (x .< [2,5,6,7,8,9]), [1,3,4]) )





# build prepare_self_consistency_equation function
n = 7
pg, op = two_node_toy_model(n, node_parameters_dict, K)
solitary_idxs = [1]

original_solitary_idxs = solitary_idxs

slack_idxs = findall(x -> (typeof(x) == SlackAlgebraic), pg.nodes)
    N_slacks = length(slack_idxs)
    @assert all(all.(broadcast(idx -> (idx .!== slack_idxs), solitary_idxs) ) ) "Slack and solitary idxs can not match!"
    dynamic_Laplacian = steady_state_Laplacian(pg, op)
    if N_slacks == 0
        dynamic_solitary_idxs = original_solitary_idxs
    else
        dynamic_solitary_idxs = original_solitary_idxs .- broadcast(idx -> sum(idx .> slack_idxs), original_solitary_idxs)
    end

dynamic_solitary_idxs

    reduced_Laplacian = reduce_Laplacian(dynamic_Laplacian, dynamic_solitary_idxs)
    K_matrix = coupling_matrix(pg)
    reduced_eigenvalues, reduced_eigenvectors = eigen(reduced_Laplacian)
    N = length(pg.nodes)
    N_solitaries = length(dynamic_solitary_idxs)
    N_sync = N - N_solitaries
    N_sync_dynamic = N_sync - N_slacks
    all_amplitude_factors = Vector{Matrix{Float64}}(undef, N_solitaries)
idx = 1
    #for idx in 1:N_solitaries
        original_solitary_idx = original_solitary_idxs[idx]
        cut_idxs = sort(vcat(original_solitary_idxs, slack_idxs))
        solitary_neighbor_idxs_original = setdiff( neighbors(pg.graph, original_solitary_idx), cut_idxs)
        solitary_neighbor_idxs_reduced_dynamic = solitary_neighbor_idxs_original .- broadcast(idx -> sum(idx .> cut_idxs), solitary_neighbor_idxs_original)
        reduced_eigenvector_components_squared = reduced_eigenvectors[solitary_neighbor_idxs_reduced_dynamic, :].^2
        coupling_strengths_squared = K_matrix[original_solitary_idx, solitary_neighbor_idxs_original].^2
        all_amplitude_factors[idx] = sum(reduced_eigenvector_components_squared .* repeat(coupling_strengths_squared, 1, N_sync_dynamic), dims=1)
    #end
    
    repeat(coupling_strengths_squared, 1, N_sync)

    reduced_eigenvalues, all_amplitude_factors


    ####

    K_matrix = coupling_matrix(pg)
    # always a vector
    K_matrix[1, [2,3,4]]
    K_matrix[[2,3,4],1]

    K_matrix[1, [2,3,4]] === K_matrix[[2,3,4],1]
    K_matrix[1, [2,3,4]] ==  K_matrix[[2,3,4],1]

    [1 2 3]