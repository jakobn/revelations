#
using FileIO
using JLD
using Plots


include(joinpath(@__DIR__,"../src/network_generation.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))
# include(joinpath(@__DIR__,"../src/simulation_functions.jl"))
include(joinpath(@__DIR__, "../src/plotting_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")
ensemble_path_local = joinpath(ensemble_path,"random_pgs/")
data_path = joinpath(@__DIR__,"../data/")
data_path_local = joinpath(data_path, "random_pgs/")
# plot_path = joinpath(@__DIR__,"../plots/")
plot_path = joinpath(@__DIR__,"../plots/prl_v4/")

generation_parameters = JLD.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

fontsize = 12
size_inches = ((3+3/8), 2/3 * (3+3/8))
size_pt = 72 .* size_inches # switched to higher resolution from 72, but then font was tiny (could be rescaled accordingly)


grid_idx = 1

pg, RHS, op = find_power_grid_path(ensemble_path_local, grid_idx)

## more data points
name = "_200t_50s_10dphi_120dw"

simulation_parameters = FileIO.load(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*name*".jld"))
node_idx_range = simulation_parameters["node_idx_range"]
dϕ_idx_range = 1:length(simulation_parameters["perturbation_range_ϕ"])
dω_idx_range = 1:length(simulation_parameters["perturbation_range_ω"])

topology_data = FileIO.load(joinpath(ensemble_path_local, "topology_data.jld"))
classes = topology_data["classes"][grid_idx]
node_colors =  topology_data["node_colors"][grid_idx];


tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*name*".jld"))
mean_ωs = tail_statistics["mean_ωs"]
# dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic

mean_ωs_flat = vcat(mean_ωs...)

n_bins = 200
mean_ωs_hist = fit(StatsBase.Histogram, mean_ωs_flat, nbins=n_bins)
mean_ωs_hist_normalized = normalize(mean_ωs_hist, mode=:probability)
edges = mean_ωs_hist_normalized.edges[1]
centers = edges[1:end-1] .+ (edges[2] - edges[1])/2
weights = mean_ωs_hist_normalized.weights

min = minimum(log10.(weights[findall(x -> (x > 0), weights)]))
max = maximum(log10.(weights[findall(x -> (x > 0), weights)]))
fillto = min - 2

Fig = Makie.Figure(resolution=size_pt, fontsize=fontsize);
ax = Makie.Axis(Fig[1, 1], xlabel = L"mean frequency $\omega_s$", ylabel = L"$\log_{10}$(fraction)") #ylabel = "⟨ωᵢ⟩" does not work
Makie.barplot!(centers, log10.(weights), fillto=fillto)
Makie.ylims!(min - 0.2, 0)
display(Fig)

### we should filter out everything close to the synchronous state, and restrict ourselves to the 1-solitary states in the next step
threshold = 1. # count as nonzero mean frequency
desynchronized_ωs_bool = abs.(mean_ωs) .> threshold
how_many_desynchronized_ωs = sum(desynchronized_ωs_bool, dims=4)
where_desynchronized = findall(i -> (i>0), desynchronized_ωs_bool)
where_one_solitaries = findall(i -> isequal(1, i), how_many_desynchronized_ωs)
where_one_solitaries_with_node_i_idx = vcat(conditional_cartesian_idx_vector(where_desynchronized, where_one_solitaries, 1:3)... )
one_solitaries_mean_ωs = vcat(conditional_nodes_indexer(where_desynchronized, mean_ωs, where_one_solitaries, 1:3)...)
condition_cartesian_idx_vector = map(x -> CartesianIndex(0,0,0,x), 1:N)
one_solitaries_mean_ωs_by_node_i_idx = conditional_nodes_indexer(where_one_solitaries_with_node_i_idx, mean_ωs, condition_cartesian_idx_vector, 4)

length.(one_solitaries_mean_ωs_by_node_i_idx)

# Which nodes show 1-solitaries - remarkable that only high indices -> tree nodes!
solitary_idxs = (1:N)[length.(one_solitaries_mean_ωs_by_node_i_idx).>0]



# we did this already in the simulation_grid_1.jl file
data = FileIO.load(joinpath(data_path_local, "solitary_frequency_data_grid_"*string(grid_idx)*name*".jld"))
data["ωs"] == one_solitaries_mean_ωs    
data["idxs"]

desynchronized_mean_ωs = mean_ωs[where_desynchronized]

n_bins = 200
mean_ωs_hist = fit(StatsBase.Histogram, desynchronized_mean_ωs, nbins=n_bins)
mean_ωs_hist_normalized = normalize(mean_ωs_hist, mode=:probability)
edges = mean_ωs_hist_normalized.edges[1]
centers = edges[1:end-1] .+ (edges[2] - edges[1])/2
weights = mean_ωs_hist_normalized.weights

min = minimum(log10.(weights[findall(x -> (x > 0), weights)]))
max = maximum(log10.(weights[findall(x -> (x > 0), weights)]))
fillto = min - 2


Fig = Makie.Figure(resolution=size_pt, fontsize=fontsize);
ax = Makie.Axis(Fig[1, 1], xlabel = L"mean frequency $\omega_s$", ylabel = L"$\log_{10}$(fraction)") #ylabel = "⟨ωᵢ⟩" does not work
Makie.barplot!(centers, log10.(weights), fillto=fillto)
Makie.ylims!(-2.4, 0)
display(Fig)

# +only 1-solitaries 

n_bins = 200
mean_ωs_hist = fit(StatsBase.Histogram, one_solitaries_mean_ωs, nbins=n_bins)
mean_ωs_hist_normalized = normalize(mean_ωs_hist, mode=:probability)
edges = mean_ωs_hist_normalized.edges[1]
centers = edges[1:end-1] .+ (edges[2] - edges[1])/2
weights = mean_ωs_hist_normalized.weights

sum(weights)

min = minimum(log10.(weights[findall(x -> (x > 0), weights)]))
max = maximum(log10.(weights[findall(x -> (x > 0), weights)]))
fillto = min - 2


Fig = Makie.Figure(resolution=size_pt, fontsize=fontsize);
ax = Makie.Axis(Fig[1, 1], xlabel = L"mean frequency $\omega_s$", ylabel = L"$\log_{10}$(fraction)") #ylabel = "⟨ωᵢ⟩" does not work
Makie.barplot!(centers, log10.(weights), fillto=fillto)
Makie.ylims!(-3, max+0.1)
display(Fig)


# try stacked plots with node node_colors
## we have to re-order the nodes by topological class

min_ω = minimum(mean_ωs) - 0.1# this is new to account for half-open bins
max_ω = maximum(mean_ωs) + 0.1
n_bins = 200
edges = [min_ω : (max_ω - min_ω)/n_bins : max_ω ...]
centers = edges[1:end-1] .+ (max_ω - min_ω)/n_bins


all_weights = map(i -> (fit(StatsBase.Histogram, one_solitaries_mean_ωs_by_node_i_idx[i], edges).weights), node_idx_range)
sum(length.(one_solitaries_mean_ωs_by_node_i_idx))
norm = sum(map(i -> sum(all_weights[i]), node_idx_range) ) # is now same as above, because we widened bins beyond min and max frequency in data
all_weights_normalized = all_weights ./ norm
sum(sum(all_weights_normalized))
cumulative_all_weights_normalized = map(i -> (sum(all_weights_normalized[1:i])), node_idx_range)


# reorder data after topolocial classes
class_values = classification_values(classes)
all_weights_normalized_with_class_values = Pair.(all_weights_normalized, class_values)

all_weights_normalized_sorted_by_class = sort_by_class(all_weights_normalized, classes)
cumulative_all_weights_normalized_sorted_by_class = map(i -> (sum(all_weights_normalized_sorted_by_class[1:i])), node_idx_range)
node_colors_sorted_by_class = sort_by_class(node_colors, classes)



Fig = Makie.Figure(resolution=size_pt, fontsize=fontsize);
ax = Makie.Axis(Fig[1, 1], xlabel = L"mean frequency $\omega_s$",
    yticks=(map(x -> 10^x, -3. : 1. : -1.), ["-3","-2","-1"]),
    ylabel = L"$\log_{10}$(fraction)", yscale=log10) #ylabel = "⟨ωᵢ⟩" does not work
# only works if we use all nodes
ax.xgridvisible = false
ax.ygridvisible = false
for node_i_idx in node_idx_range
    if node_i_idx == 1
        fillto = 0
    else
        fillto = cumulative_all_weights_normalized_sorted_by_class[node_i_idx-1]
        fillto = replace(fillto, 0 => 0.00000000001)
    end

    color=node_colors_sorted_by_class[node_i_idx]
    ydata = cumulative_all_weights_normalized_sorted_by_class[node_i_idx]

    Makie.barplot!(centers, ydata, fillto=fillto, color=color)

end
#Makie.ylims!(10^(-3), 10^(max+0.1))
Makie.ylims!(10^(-2.5), 10^(-0.9))

display(Fig)

save(joinpath(plot_path, "mean_omegas_hist_1_solitaries_classes"*name*"_v2.pdf"), Fig)
hidedecorations!()


# note: barplot does not draw bars from -Inf to some finite fillto value (nice!)
"""
Fig = Makie.Figure(resolution=size_pt, fontsize=fontsize);
ax = Makie.Axis(Fig[1, 1], xlabel = L"mean frequency $\omega_s$",
    yscale = log10,
    ylabel = L"$$(fraction)") #ylabel = "⟨ωᵢ⟩" does not work
barplot!([-1,0,1], exp.(log(10) .* [-Inf,-Inf,-1]),fillto = exp.(log(10) .* [-1,-2,-1000]) )
Makie.ylims!(exp.(log(10) .* [-3,0])... ) 
Makie.xlims!(-1,2)
display(Fig)
"""





# broader: full page width

fontsize = 12
size_inches_broad = (2*(3+3/8), 2/3 * (3+3/8))
size_pt_broad = 72 .* size_inches_broad # switched to higher resolution from 72, but then font was tiny (could be rescaled accordingly)


Fig = Makie.Figure(resolution=size_pt_broad, fontsize=fontsize);
ax = Makie.Axis(Fig[1, 1], xlabel = L"mean frequency $\omega_s$",
    yticks=(map(x -> 10^x, -3. : 1. : -1.), ["-3","-2","-1"]),
    ylabel = L"$\log_{10}$(fraction)", yscale=log10) #ylabel = "⟨ωᵢ⟩" does not work
# only works if we use all nodes
ax.xgridvisible = false
ax.ygridvisible = false
for node_i_idx in node_idx_range
    if node_i_idx == 1
        fillto = 0
    else
        fillto = cumulative_all_weights_normalized_sorted_by_class[node_i_idx-1]
        fillto = replace(fillto, 0 => 0.00000000001)
    end

    color=node_colors_sorted_by_class[node_i_idx]
    ydata = cumulative_all_weights_normalized_sorted_by_class[node_i_idx]

    Makie.barplot!(centers, ydata, fillto=fillto, color=color)

end
#Makie.ylims!(10^(-3), 10^(max+0.1))
Makie.ylims!(10^(-2.5), 10^(-0.9))

display(Fig)

save(joinpath(plot_path, "mean_omegas_hist_1_solitaries_classes"*name*"_v3.pdf"), Fig)





# prl_v4: half page and only absolute value of freqs
mean_ωs

sign(pg.nodes[74].P)

mean_ωs_power_sign = copy(mean_ωs)
for node_idx in node_idx_range
    power_sign = sign(pg.nodes[node_idx].P)
    mean_ωs_power_sign[:,:,:,node_idx] = mean_ωs[:,:,:,node_idx] .* power_sign
end


n_bins = 200
#mean_ωs_hist = fit(StatsBase.Histogram, one_solitaries_mean_ωs, nbins=n_bins)
#mean_ωs_hist_normalized = normalize(mean_ωs_hist, mode=:probability)
#edges = mean_ωs_hist_normalized.edges[1]
#centers = edges[1:end-1] .+ (edges[2] - edges[1])/2
#weights = mean_ωs_hist_normalized.weights



threshold = 1.0 # count as nonzero mean frequency
desynchronized_ωs_bool = abs.(mean_ωs_power_sign) .> threshold
how_many_desynchronized_ωs = sum(desynchronized_ωs_bool, dims=4)
where_desynchronized = findall(i -> (i>0), desynchronized_ωs_bool)
where_one_solitaries = findall(i -> isequal(1, i), how_many_desynchronized_ωs)
where_one_solitaries_with_node_i_idx = vcat(conditional_cartesian_idx_vector(where_desynchronized, where_one_solitaries, 1:3)... )
one_solitaries_mean_ωs_power_sign = vcat(conditional_nodes_indexer(where_desynchronized, mean_ωs_power_sign, where_one_solitaries, 1:3)...)
condition_cartesian_idx_vector = map(x -> CartesianIndex(0,0,0,x), 1:N)
one_solitaries_mean_ωs_power_sign_by_node_i_idx = conditional_nodes_indexer(where_one_solitaries_with_node_i_idx, mean_ωs_power_sign, condition_cartesian_idx_vector, 4)


min_ω = minimum(one_solitaries_mean_ωs_power_sign) - 0.1# this is new to account for half-open bins
max_ω = maximum(one_solitaries_mean_ωs_power_sign) + 0.1
n_bins = 100
edges = [min_ω : (max_ω - min_ω)/n_bins : max_ω ...]
centers = edges[1:end-1] .+ (max_ω - min_ω)/n_bins

all_weights = map(i -> (fit(StatsBase.Histogram, one_solitaries_mean_ωs_power_sign_by_node_i_idx[i], edges).weights), node_idx_range)
sum(length.(one_solitaries_mean_ωs_power_sign_by_node_i_idx))
norm = sum(map(i -> sum(all_weights[i]), node_idx_range) ) # is now same as above, because we widened bins beyond min and max frequency in data
all_weights_normalized = all_weights ./ norm
sum(sum(all_weights_normalized))
cumulative_all_weights_normalized = map(i -> (sum(all_weights_normalized[1:i])), node_idx_range)



# reorder data after topolocial classes
class_values = classification_values(classes)
all_weights_normalized_with_class_values = Pair.(all_weights_normalized, class_values)

all_weights_normalized_sorted_by_class = sort_by_class(all_weights_normalized, classes)
cumulative_all_weights_normalized_sorted_by_class = map(i -> (sum(all_weights_normalized_sorted_by_class[1:i])), node_idx_range)
node_colors_sorted_by_class = sort_by_class(node_colors, classes);


fontsize = 12
size_inches_one_column = (0.5 * 2*(3+3/8), 2/3 * (3+3/8))
size_pt_one_column = 72 .* size_inches_one_column # switched to higher resolution from 72, but then font was tiny (could be rescaled accordingly)


Fig = Makie.Figure(resolution=size_pt_one_column, fontsize=fontsize);
ax = Makie.Axis(Fig[1, 1], xlabel = L"mean frequency $|\omega_s|$",
    xticks=([0,5,10],["0","5","10"]),
    yticks=(map(x -> 10^x, -3. : 1. : -1.), ["-3","-2","-1"]),
    ylabel = L"$\log_{10}$(fraction)", yscale=log10) #ylabel = "⟨ωᵢ⟩" does not work
# only works if we use all nodes
ax.xgridvisible = false
ax.ygridvisible = false
for node_i_idx in node_idx_range
    if node_i_idx == 1
        fillto = 0
    else
        fillto = cumulative_all_weights_normalized_sorted_by_class[node_i_idx-1]
        fillto = replace(fillto, 0 => 0.00000000001)
    end

    color=node_colors_sorted_by_class[node_i_idx]
    ydata = cumulative_all_weights_normalized_sorted_by_class[node_i_idx]

    Makie.barplot!(centers, ydata, fillto=fillto, color=color)

end
#Makie.ylims!(10^(-3), 10^(max+0.1))
Makie.ylims!(10^(-3.2), 10^(-0.6))
Makie.xlims!(-2.0, 12.0)
#Makie.ylims!(10^(-5), 10^(-0.1))
display(Fig)

save(joinpath(plot_path, "mean_omegas_hist_1_solitaries_classes"*name*"_v4.pdf"), Fig)
