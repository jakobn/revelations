"""
Plot theoretical prediction (contour of self-consistency equation) and numerical results
"""
#
using FileIO
using JLD
using CairoMakie
using GLMakie

include(joinpath(@__DIR__,"../src/network_generation.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))
include(joinpath(@__DIR__,"../src/simulation_functions.jl"))
include(joinpath(@__DIR__, "../src/plotting_functions.jl"))
include(joinpath(@__DIR__, "../src/self_consistency.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")
data_path = joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")

size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
scaler = 1
size_pt = scaler .* 72 .* size_inches

alpha = 0.02
marker1 = :xcross
color1 = (palette(:tab10)[1], alpha);
size1 = 12 * scaler

K = 6.
m = 1.
P_solitary = 1.
α = 0.1
α_solitary = .1
P_solitary = 1.

# two-node toy model
ensemble_name = "two_node_toy_models"
ensemble_path_local = joinpath(ensemble_path, ensemble_name*"/")
data_path_local = joinpath(data_path, ensemble_name*"/")

n_initial_ϕ = 10
n_initial_ω = 12
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -5.
ω_max_incl = 6.

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

grid_idx_range = 0:30
ns = grid_idx_range

mean_ωs_by_n = Vector{Array{Float64, 4}}(undef, length(ns))
std_ωs_by_n = Vector{Array{Float64, 4}}(undef, length(ns))
for n_idx in 1:length(ns)
    grid_idx = ns[n_idx]
    tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_two_node_toy_model_n_"*string(grid_idx)*"_200t_50s_10dphi_10dw.jld"))
    mean_ωs_by_n[n_idx] = tail_statistics["mean_ωs"]
    std_ωs_by_n[n_idx] = tail_statistics["std_ωs"]
end

ns_Z = -.5 : 0.03 : 30.5
ωs_Z = 0.01 : 0.01 : 10.5

analytic_mean_power_flow_fct = analytic_mean_power_flow_two_node_toy_model
reduced_eigenvalues_fct = analytic_eigenvalues_two_node_toy_model
analytic_amplitude_factors_fct = analytic_amplitude_factors_two_node_toy_model
N = 3
N_slacks = 1

Zs = [analytic_self_consistency_function(analytic_mean_power_flow_fct, K, ω, m, P_solitary, α, α_solitary, n) for n in ns_Z, ω in ωs_Z]
Zs_alt = [self_consistency_equation_1_solitary(reduced_eigenvalues_fct(K, n), analytic_amplitude_factors_fct(K, n), ω, m, P_solitary, α, α_solitary, N, N_slacks) for n in ns_Z, ω in ωs_Z]
Zs_stability = [self_consistency_stability_1_solitary(reduced_eigenvalues_fct(K, n), analytic_amplitude_factors_fct(K, n), ω, m, α, α_solitary, N, N_slacks) for n in ns_Z, ω in ωs_Z]
Zs_derivative = [self_consistency_derivative_1_solitary(reduced_eigenvalues_fct(K, n), analytic_amplitude_factors_fct(K, n), ω, m, α, α_solitary, N, N_slacks) for n in ns_Z, ω in ωs_Z]


RGB{Float64}(0.4,0.4,0.4)
RGBAf(0.4,0.4,0.4,0.6)

# test if both methods yield the same: they do!
maximum(abs.(Zs .- Zs_alt))

Zs_unstable = conditional_matrix_replacer(Zs, Zs_stability, missing)
Zs_stable = conditional_matrix_replacer(Zs, 1 .-Zs_stability, missing)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")
Makie.xlims!(ax1, [ns_Z[1], ns_Z[end]])
Makie.ylims!(ax1, [ωs_Z[1]-0.5, ωs_Z[end]])

Z_stable_contour = Makie.contour!(ns_Z, ωs_Z, Zs_stable, levels=[0], color=:green, linewidth=1.5) # color does not get transferred to Legend, have to set markers manually
Z_unstable_contour = Makie.contour!(ns_Z, ωs_Z, Zs_unstable, levels=[0], color=(:red, 0.7), linewidth=0.5) # linestyle=:dash does not work: only legend gets the linestyle, not the line itself :(

for n_idx in 1:length(ns)
    n = ns[n_idx]
    data = vcat(mean_ωs_by_n[n_idx][:,:,:,1]...)
    if n > 0
        data = data[findall(x -> (x > 1.), data)]
    end
    Makie.scatter!(n * ones(length(data)), data, marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_scatter =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

ns_modes = 0.01 : 0.01 : 30.
network_modes_data = map(n -> static_frequency.( sqrt.(reduced_eigenvalues_fct(K, n)), α, α_solitary, n+2, N_slacks), ns_modes)
network_modes_data = reduce(hcat ,network_modes_data) # reshape from vector of vectors to transposeed matrix

for l in 1:size(network_modes_data)[1]
    Makie.lines!(ax1, ns_modes, network_modes_data[l,:], color=:black, linewidth=1, linestyle=:dot)
end
# network_modes_lines

marker_Zs_stable = [LineElement(color=:green, linestyle=nothing, linewidth=1.5)]
marker_Zs_unstable = [LineElement(color=(:red, 0.7), linestyle=nothing, linewidth=0.5)]

# Legend(f_ω[1,1], [marker_Zs_stable, marker_Zs_unstable, mean_ω_1_scatter], ["prediction: stable", "prediction: unstable", "simulation"],# bgcolor=false, framevisible=false,
#   halign=0.85, valign=0.4, tellwidth=false, tellheight=false)
# Legend(f_ω[2,1], [marker_Zs_stable, marker_Zs_unstable, mean_ω_1_scatter], ["prediction: stable", "prediction: unstable", "simulation"],
#     orientation=:horizontal, tellheight=true, tellwidth=false)
Legend(f_ω[1,1], [marker_Zs_stable, marker_Zs_unstable, mean_ω_1_scatter], ["prediction: stable", "prediction: unstable", "simulation"],# bgcolor=false, framevisible=false,
  halign=1., valign=0.1, orientation=:horizontal, tellwidth=false, tellheight=false)
f_ω
save(joinpath(plot_path, "bifurcation_diagram_two_node_toy_model.png"), f_ω, px_per_unit=4)
save(joinpath(plot_path, "bifurcation_diagram_two_node_toy_model.pdf"), f_ω)





# star-graph toy model
ensemble_name = "star_graph_toy_models"
ensemble_path_local = joinpath(ensemble_path, ensemble_name*"/")
data_path_local = joinpath(data_path, ensemble_name*"/")

n_initial_ϕ = 10
n_initial_ω = 12
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -5.
ω_max_incl = 6.

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

grid_idx_range = 0:30
ns = grid_idx_range

mean_ωs_by_n = Vector{Array{Float64, 4}}(undef, length(ns))
std_ωs_by_n = Vector{Array{Float64, 4}}(undef, length(ns))
for n_idx in 1:length(ns)
    grid_idx = ns[n_idx]
    tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_two_node_toy_model_n_"*string(grid_idx)*"_200t_50s_10dphi_10dw.jld"))
    mean_ωs_by_n[n_idx] = tail_statistics["mean_ωs"]
    std_ωs_by_n[n_idx] = tail_statistics["std_ωs"]
end

ns_Z = -.5 : 0.03 : 30.5
ωs_Z = 0.01 : 0.01 : 10.5

analytic_mean_power_flow_fct = analytic_mean_power_flow_star_graph_toy_model
reduced_eigenvalues_fct = analytic_eigenvalues_star_graph_toy_model
analytic_amplitude_factors_fct = analytic_amplitude_factors_star_graph_toy_model
# N = 3 here: N=n+2
N_slacks = 0

Zs = [analytic_self_consistency_function(analytic_mean_power_flow_fct, K, ω, m, P_solitary, α, α_solitary, n) for n in ns_Z, ω in ωs_Z]
Zs_alt = [self_consistency_equation_1_solitary(reduced_eigenvalues_fct(K, n), analytic_amplitude_factors_fct(K, n), ω, m, P_solitary, α, α_solitary, n+2, N_slacks) for n in ns_Z, ω in ωs_Z]
Zs_stability = [self_consistency_stability_1_solitary(reduced_eigenvalues_fct(K, n), analytic_amplitude_factors_fct(K, n), ω, m, α, α_solitary, n+2, N_slacks) for n in ns_Z, ω in ωs_Z]
Zs_derivative = [self_consistency_derivative_1_solitary(reduced_eigenvalues_fct(K, n), analytic_amplitude_factors_fct(K, n), ω, m, α, α_solitary, n+2, N_slacks) for n in ns_Z, ω in ωs_Z]

# test if both methods yield the same: they do!
maximum(abs.(Zs .- Zs_alt))

Zs_unstable = conditional_matrix_replacer(Zs, Zs_stability, missing)
Zs_stable = conditional_matrix_replacer(Zs, 1 .-Zs_stability, missing)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")
Makie.xlims!(ax1, [ns_Z[1], ns_Z[end]])
Makie.ylims!(ax1, [ωs_Z[1]-0.5, ωs_Z[end]])

Z_stable_contour = Makie.contour!(ns_Z, ωs_Z, Zs_stable, levels=[0], color=:green, linewidth=1.5) # color does not get transferred to Legend, have to set markers manually
Z_unstable_contour = Makie.contour!(ns_Z, ωs_Z, Zs_unstable, levels=[0], color=(:red, 0.7), linewidth=0.5) # linestyle=:dash does not work: only legend gets the linestyle, not the line itself :(

for n_idx in 1:length(ns)
    n = ns[n_idx]
    data = vcat(mean_ωs_by_n[n_idx][:,:,:,1]...)
    if n >= 0 # this time remove all data of sync state
        data = data[findall(x -> (x > 1.), data)]
    end
    Makie.scatter!(n * ones(length(data)), data, marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_scatter =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

ns_modes = 0.01 : 0.01 : 30.
network_modes_data = map(n -> static_frequency.( sqrt.(reduced_eigenvalues_fct(K, n)), α, α_solitary, n+2, N_slacks), ns_modes)
network_modes_data = reduce(hcat ,network_modes_data) # reshape from vector of vectors to transposeed matrix

for l in 1:size(network_modes_data)[1]
    Makie.lines!(ax1, ns_modes, network_modes_data[l,:], color=:black, linewidth=1, linestyle=:dot)
end
# network_modes_lines

marker_Zs_stable = [LineElement(color=:green, linestyle=nothing, linewidth=1.5)]
marker_Zs_unstable = [LineElement(color=(:red, 0.7), linestyle=nothing, linewidth=0.5)]

# Legend(f_ω[1,1], [marker_Zs_stable, marker_Zs_unstable, mean_ω_1_scatter], ["prediction: stable", "prediction: unstable", "simulation"],# bgcolor=false, framevisible=false,
#   halign=0.85, valign=0.4, tellwidth=false, tellheight=false)
Legend(f_ω[1,1], [marker_Zs_stable, marker_Zs_unstable, mean_ω_1_scatter], ["prediction: stable", "prediction: unstable", "simulation"],# bgcolor=false, framevisible=false,
halign=1., valign=0.145, orientation=:horizontal, tellwidth=false, tellheight=false)
f_ω
save(joinpath(plot_path, "bifurcation_diagram_"*ensemble_name[1:end-1]*".png"), f_ω, px_per_unit=4)
save(joinpath(plot_path, "bifurcation_diagram_"*ensemble_name[1:end-1]*".pdf"), f_ω)




# try with data from all-to-all toy model: looks the same, as expected
data_path_all_to_all = joinpath(data_path, "all_to_all_toy_models/")
mean_ωs_by_n_all_to_all = Vector{Array{Float64, 4}}(undef, length(ns))
std_ωs_by_n_all_to_all = Vector{Array{Float64, 4}}(undef, length(ns))
for n_idx in 1:length(ns)
    grid_idx = ns[n_idx]
    tail_statistics = FileIO.load(joinpath(data_path_all_to_all, "tail_statistics_grid_two_node_toy_model_n_"*string(grid_idx)*"_200t_50s_10dphi_10dw.jld"))
    mean_ωs_by_n_all_to_all[n_idx] = tail_statistics["mean_ωs"]
    std_ωs_by_n_all_to_all[n_idx] = tail_statistics["std_ωs"]
end

#check that data is different
mean_ωs_by_n == mean_ωs_by_n_all_to_all

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")
Makie.xlims!(ax1, [ns_Z[1], ns_Z[end]])
Makie.ylims!(ax1, [ωs_Z[1]-0.5, ωs_Z[end]])

Z_stable_contour = Makie.contour!(ns_Z, ωs_Z, Zs_stable, levels=[0], color=:green, linewidth=1.5) # color does not get transferred to Legend, have to set markers manually
Z_unstable_contour = Makie.contour!(ns_Z, ωs_Z, Zs_unstable, levels=[0], color=(:red, 0.7), linewidth=0.5) # linestyle=:dash does not work: only legend gets the linestyle, not the line itself :(

for n_idx in 1:length(ns)
    n = ns[n_idx]
    data = vcat(mean_ωs_by_n_all_to_all[n_idx][:,:,:,1]...)
    if n >= 0 # this time remove all data of sync state
        data = data[findall(x -> (x > 1.), data)]
    end
    Makie.scatter!(n * ones(length(data)), data, marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_scatter =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

marker_Zs_stable = [LineElement(color=:green, linestyle=nothing, linewidth=1.5)]
marker_Zs_unstable = [LineElement(color=(:red, 0.7), linestyle=nothing, linewidth=0.5)]

Legend(f_ω[1,1], [marker_Zs_stable, marker_Zs_unstable, mean_ω_1_scatter], ["prediction: stable", "prediction: unstable", "simulation"],# bgcolor=false, framevisible=false,
  halign=0.85, valign=0.4, tellwidth=false, tellheight=false)
f_ω