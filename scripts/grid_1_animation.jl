#
using Plots
using LaTeXStrings
using Images

include(joinpath(@__DIR__, "../src/network_generation.jl"))
include(joinpath(@__DIR__, "../src/plotting_functions.jl"))
include(joinpath(@__DIR__, "../src/simulation_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")
ensemble_path_local = joinpath(ensemble_path,"random_pgs/")
data_path = joinpath(@__DIR__,"../data/")
data_path_local = joinpath(data_path, "random_pgs")
# plot_path = joinpath(@__DIR__,"../plots/")
plot_path = joinpath(@__DIR__,"../plots/prl_v4/")

topology_data = FileIO.load(joinpath(ensemble_path_local,"topology_data.jld"))
generation_parameters = FileIO.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

##### make shorter : 125s
scaler = 2
fontsize = scaler * 12
# size_inches = scaler .* (2*(3+3/8),  2*(3+3/8))
# size_pt = Int.(72 .* size_inches)
# size_pt = size_pt .- mod.(size_pt,2) .+ (1,0)# to ensure even width and height for mp4
size_pt = (970,970)
# check which values work (annoying!)
# (970,970) works and (1942,970)
# for i in -2:2, j in -2:2
# size_pt = (972+i,972+j)
# save("test_"*"$(size_pt[1])"*"x"*"$(size_pt[2])"*".png",Plots.plot(1, size=size_pt))
# # end
# for i in -2:2, j in -2:2
#     size_pt = (2*972+i,972 + j)
#     save("test_"*"$(size_pt[1])"*"x"*"$(size_pt[2])"*".png",Plots.plot(1, size=size_pt))
# end

### start off with minimal two node toy model (harm simpler)




grid_idx = 1
node_colors = topology_data["node_colors"][grid_idx];

where_dense_sprouts = topology_data["where_dense_sprouts"]
# node 74
one_solitary_idx = where_dense_sprouts[grid_idx][2]

pg, RHS, op = find_power_grid_path(ensemble_path_local, grid_idx)

fps = 24
resolution_factor = 10
t_step_1 = 1/(fps*resolution_factor) # 1 time unit gets this amount of frames to become 1 second
time_ratio = 10/10 # 40 time units become 10 seconds (because 1 time unit is 0.25 s and we have time_ratio=1)
t_start = 0
t_pert_reso = 10
t_pert_natu = 50
t_end = 125
t_span_sync = t_start : t_step_1 * time_ratio : t_pert_reso
t_span_reso = t_pert_reso: t_step_1 * time_ratio : t_pert_natu
t_span_natu = t_pert_natu : t_step_1 * time_ratio : t_end
plot_idxs = collect(1:N)


sol_sync = simulate_from_state(pg, op.vec, (t_start, t_pert_reso))
pg_sol_sync = PowerGridSolution(sol_sync, pg)
φ_sol_sync = pg_sol_sync(t_span_sync, :, :φ)
ω_sol_sync = pg_sol_sync(t_span_sync, :, :ω)
p_sol_sync = pg_sol_sync(t_span_sync, :, :p)

y0_reso = State(pg, sol_sync.u[end])
dϕ_reso = 0.
dω_reso = 6.
sol_reso = simulate_from_perturbation_var(pg, y0_reso, [dϕ_reso, dω_reso], single_node_perturbation_ϕ_ω, one_solitary_idx, (t_pert_reso,t_pert_natu))
pg_sol_reso = PowerGridSolution(sol_reso, pg)
φ_sol_reso = pg_sol_reso(t_span_reso, :, :φ)
ω_sol_reso = pg_sol_reso(t_span_reso, :, :ω)
p_sol_reso = pg_sol_reso(t_span_reso, :, :p)

y0_natu = State(pg, sol_reso.u[end])
dϕ_natu = 0.
dω_natu = 6.
sol_natu = simulate_from_perturbation_var(pg, y0_natu, [dϕ_natu, dω_natu], single_node_perturbation_ϕ_ω, one_solitary_idx, (t_pert_natu,t_end))
pg_sol_natu = PowerGridSolution(sol_natu, pg)
φ_sol_natu = pg_sol_natu(t_span_natu, :, :φ)
ω_sol_natu = pg_sol_natu(t_span_natu, :, :ω)
p_sol_natu = pg_sol_natu(t_span_natu, :, :p)

# Plots.plot(pg_sol_sync, plot_idxs, :φ, legend=false);
# Plots.plot!(pg_sol_reso, plot_idxs, :φ, legend=false);
# Plots.plot!(pg_sol_natu, plot_idxs, :φ, legend=false)

# Plots.plot(pg_sol_sync, plot_idxs, :ω, legend=false);
# Plots.plot!(pg_sol_reso, plot_idxs, :ω, legend=false);
# Plots.plot!(pg_sol_natu, plot_idxs, :ω, legend=false)


φ_trajectories = hcat(φ_sol_sync[:, 1:end-1], φ_sol_reso[:, 1:end-1], φ_sol_natu)
ω_trajectories = hcat(ω_sol_sync[:, 1:end-1], ω_sol_reso[:, 1:end-1], ω_sol_natu)
p_trajectories = hcat(p_sol_sync[:, 1:end-1], p_sol_reso[:, 1:end-1], p_sol_natu)
time_span_all = vcat(t_span_sync[1:end-1], t_span_reso[1:end-1], t_span_natu)

x_trajectories = cos.(φ_trajectories)
y_trajectories = sin.(φ_trajectories)

# Plots.plot(time_span_all, ω_trajectories', legend=false, linecolor=node_colors')

# Plots.plot(time_span_all, p_trajectories[one_solitary_idx,:], legend=false, linecolor=node_colors[one_solitary_idx])

moving_average(vs,n) = [sum(@view vs[i:(i+n-1)])/n for i in 1:(length(vs)-(n-1))]

# mean over 10.0 time units
averaging_points = Int(10. /(t_step_1*time_ratio))
p_solitary_mean = moving_average(p_trajectories[one_solitary_idx,:], averaging_points)
p_solitary_mean_longer = vcat(repeat([p_solitary_mean[1]], averaging_points-1), p_solitary_mean)
Plots.plot(time_span_all, p_solitary_mean_longer, xlabel=L"t", ylabel=L"p_{s}", label=false, size=size_pt, fontsize=fontsize, linecolor=node_colors[one_solitary_idx])










## make animation of phases on circle
### max size determined by distance to solitary node
graph = pg.graph
distances = map(idx -> length(Graphs.a_star(graph, one_solitary_idx, idx) ), plot_idxs)
distances[one_solitary_idx]
maximum(distances)

n_points_shown = 20 #2 #96
seriesalpha = range(0, 1, length = n_points_shown) #1 #range(0, 1, length = n_points_shown)
seriesalpha = repeat(seriesalpha, 1, length(plot_idxs))

# linewidth = 10*scaler #range(0, 10*scaler, length = n_points_shown)
linewidth = [(17 - d) *scaler *(n-1)/(n_points_shown-1) for n in 1:n_points_shown, d in distances]

# to try things out
#####
index = 10000
value = time_span_all[index]
time_span_plot = @view time_span_all[1:index]
    x_trajectories_plot = @view x_trajectories[:, index:index+n_points_shown-1]
    y_trajectories_plot = @view y_trajectories[:, index:index+n_points_shown-1]
Plots.plot(cos.(ϕ),sin.(ϕ) ,xlims=(-1.1,1.2), ylims=(-1.1,1.2),
        seriesalpha=0.5, linewidth= scaler, color=:gray,
        xlabel=L"\cos\varphi_i", ylabel=L"\sin\varphi_i",
        aspect_ratio=1, label=false, labelfontsize=fontsize, legendfontsize=fontsize,
        tickfontsize=fontsize, size=size_pt);
Plots.plot!(x_trajectories_plot', y_trajectories_plot', color=node_colors',
    seriesalpha=seriesalpha, linewidth=linewidth, label=false)
Plots.annotate!(0,0,Plots.text(raw"$t = " * string(round(value, digits=1)) * raw"$", Int(1.5*fontsize) ))
Plots.annotate!(0,-0.5,Plots.text(L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$", Int(1.5*fontsize), color=RGBA(0,0,0,1)))
#####

# the event displays:
show_time = 8 #seconds at 24 fps, or at 12 if every 2nd is shown
show_frame_width = time_ratio * show_time
ϕ = 0 : 2*pi/1000 : 2*pi

anim_circle_smoother = @animate for (index, value) in enumerate(time_span_all[1:end-n_points_shown])
    time_span_plot = @view time_span_all[1:index]
    x_trajectories_plot = @view x_trajectories[:, index:index+n_points_shown-1]
    y_trajectories_plot = @view y_trajectories[:, index:index+n_points_shown-1]
    
    # make gray ring in background
    Plots.plot(cos.(ϕ),sin.(ϕ) ,xlims=(-1.1,1.2), ylims=(-1.1,1.2),
        seriesalpha=0.5, linewidth= scaler, color=:gray,
        xlabel=L"\cos\varphi_i", ylabel=L"\sin\varphi_i",
        aspect_ratio=1, label=false, labelfontsize=fontsize, legendfontsize=fontsize,
        tickfontsize=fontsize, size=size_pt)

    Plots.plot!(x_trajectories_plot', y_trajectories_plot', color=node_colors',
    seriesalpha=seriesalpha, linewidth=linewidth, label=false)

    Plots.annotate!(0,0,Plots.text(raw"$t = " * string(round(value, digits=1)) * raw"$", Int(1.5*fontsize) ))
    
    if t_pert_reso  <= value < t_pert_reso + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_reso)/show_frame_width
        Plots.annotate!(0,0.5,Plots.text(L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$", Int(1.5*fontsize), color=RGBA(0,0,0,annotate_alpha)))
    end
    if t_pert_natu  <= value < t_pert_natu + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_natu)/show_frame_width
        Plots.annotate!(0,-0.5,Plots.text(L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$", Int(1.5*fontsize), color=RGBA(0,0,0,annotate_alpha)))
    end
print("\r"*string(value))
end every (resolution_factor)

mp4(anim_circle_smoother, "animation_grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*"_circle_125t_v2_20.mp4", fps = 24)


#



##### make omega and power flow plot

### make final frame as plot
xlims = (time_span_all[1], time_span_all[end])
ylims_ω = (min(-0.5, ω_trajectories...), max(ω_trajectories...))
ylims_p = (max(min(p_solitary_mean...),-0.3), max(p_solitary_mean...))
index = length(time_span_all)
value = time_span_all[end]
time_span_plot = @view time_span_all[1:index]
ω_trajectories_plot = @view ω_trajectories[:, 1:index]
p_solitary_mean_longer_plot = @view p_solitary_mean_longer[1:index]

plt_ω =  Plots.plot(time_span_plot, ω_trajectories_plot', color=node_colors',
    xticks=false, xlims=xlims, ylims=ylims_ω, #xlabel=L"t", 
    ylabel=L"\omega_i", label=false, legend=false);
Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
plt_p_solitary_mean = Plots.plot(time_span_plot, p_solitary_mean_longer_plot, color=node_colors[one_solitary_idx],
    xlims=xlims, ylims=ylims_p,
    yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]), label=false,
    xlabel=L"t", ylabel=L" p_s", legend=:right); #corrected legend position from :topright here to :bottomright in upper plot (ω₁)
Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
Plots.annotate!(-10,1.2,("sync",fontsize,:green));
if value > t_pert_reso
    Plots.annotate!(30,1.2,("resonance",fontsize, :orange));
end;
if value > t_pert_natu
    Plots.annotate!(75,1.2,("natural",fontsize), :red);
end;
# add power flow
# p_solitary_plt = Plots.plot(time_span_all, p_solitary_mean, xlims=xlims, xlabel=L"t", ylabel=L"p_s", label="full");
# Plots.plot!(time_span_all, p_solitary_trajectory, linestyle=:dash, label="linearized");
# Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=false);
# Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=false);

# _solitary_plt
# resize
# size_pt_last_frame = (2*970,3*970) # did not use
# plt_ω_p_solitary_mean_and_time = Plots.plot(plt_ω, plt_p_solitary_mean, p_solitary_plt, layout=(3,1), size=size_pt,
#     labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize, rightmargin=7Plots.mm)

# save("two_node_toy_omega_mean_and_time_power_flow_trajectories.pdf",plt_ω_p_solitary_mean_and_time) # 500 kB
# save("two_node_toy_omega_mean_and_time_power_flow_trajectories.png",plt_ω_p_solitary_mean_and_time) #250 kB

plt_ω
plt_p_solitary_mean
plt_ω_p_solitary_mean = Plots.plot(plt_ω, plt_p_solitary_mean, layout=(2,1), size=size_pt,
    labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize, rightmargin=7Plots.mm, leftmargin=4Plots.mm)
FileIO.save(joinpath(plot_path, "omega_p_solitary_mean_grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*".pdf"), plt_ω_p_solitary_mean)
FileIO.save(joinpath(plot_path, "omega_p_solitary_mean_grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*".png"), plt_ω_p_solitary_mean)






## remake for paper with larger labels and shaded areas
# v3: black instead purple (color=4) vlines for perturbation times
# v4: flatter figure, new axis labels, annotation for solitary curve
golden_ratio = (1 + sqrt(5))/2
size_pt_new = (970, 970/golden_ratio)

xlims = (time_span_all[1], time_span_all[end])
ylims_ω = (min(-0.5, ω_trajectories...), max(ω_trajectories...))
ylims_p = (max(min(p_solitary_mean...),-0.3), max(p_solitary_mean...))
index = length(time_span_all)
value = time_span_all[end]
time_span_plot = @view time_span_all[1:index]
ω_trajectories_plot = @view ω_trajectories[:, 1:index]
p_solitary_mean_longer_plot = @view p_solitary_mean_longer[1:index]

plt_ω = Plots.vline([t_pert_reso], color=:black, linewidth=scaler*2, linestyle=:dot, label=false);
Plots.vline!([], color=:black, linewidth=scaler, linestyle=:dot, label=L"$ \Delta\dot{\varphi}_{75} = 6.0$");
Plots.vline!([t_pert_natu], color=:black, linewidth=scaler*2, linestyle=:dot, label=false);
Plots.vspan!([xlims[1], t_pert_reso], color = :green, alpha=0.1, labels = false);
Plots.vspan!([t_pert_reso, t_pert_natu], color = :yellow, alpha=0.1, labels = false);
Plots.vspan!([t_pert_natu, xlims[end]], color = :red, alpha=0.1, labels = false);
Plots.plot!(time_span_plot, ω_trajectories_plot', color=node_colors',
    linewidth=scaler,
    xticks=false, xlims=xlims, ylims=ylims_ω, #xlabel=L"t",
    yticks= ([-2,2,6,10],["-2","2","6","10"]),
    ylabel=L"$\dot{\varphi}_i$", label=false, legend=:right);
plt_ω
#Plots.annotate!(85, 6, ("solitary node (75)", fontsize, :black));

plt_p_solitary_mean = Plots.vline([t_pert_reso], color=:black, linewidth=scaler*2, linestyle=:dot, label=false);
Plots.vline!([t_pert_natu], color=:black, linewidth=scaler*2, linestyle=:dot, label=false);
Plots.vspan!([xlims[1], t_pert_reso], color = :green, alpha=0.1, labels = "sync");
Plots.vspan!([t_pert_reso, t_pert_natu], color = :yellow, alpha=0.1, labels = "resonance");
Plots.vspan!([t_pert_natu, xlims[end]], color = :red, alpha=0.1, labels = "natural");
Plots.plot!(time_span_plot, p_solitary_mean_longer_plot, color=node_colors[one_solitary_idx],
    linewidth=scaler,
    xlims=xlims, ylims=ylims_p,
    yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]), label=false,
    xlabel=L"t", ylabel=L"$p_s$", legend=:right); #corrected legend position from :topright here to :bottomright in upper plot (ω₁)

#Plots.annotate!(-10,1.2,("sync",fontsize,:black));
#if value > t_pert_reso
#    Plots.annotate!(30,1.2,("resonance",fontsize, :black));
#end;
#if value > t_pert_natu
#    Plots.annotate!(75,1.2,("natural",fontsize), :black);
#end;

# plt_ω
# plt_p_solitary_mean
plt_ω_p_solitary_mean = Plots.plot(plt_ω, plt_p_solitary_mean, layout=(2,1), size=size_pt_new,
    labelfontsize=trunc(Int,fontsize*1.2), legendfontsize=trunc(Int,fontsize), tickfontsize=fontsize, right_margin=7Plots.mm, left_margin=4Plots.mm, bottom_margin=8Plots.mm)


# FileIO.save(joinpath(plot_path, "omega_p_solitary_mean_grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*"_v3"*".pdf"), plt_ω_p_solitary_mean)
FileIO.save(joinpath(plot_path, "omega_p_solitary_mean_grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*"_v3"*".png"), plt_ω_p_solitary_mean)










### make animation






# to try things out
index = 30000
value = time_span_all[index]

time_span_plot = @view time_span_all[1:index]
    ω_trajectories_plot = @view ω_trajectories[:, 1:index]
    p_solitary_mean_longer_plot = @view p_solitary_mean_longer[1:index]

    plt_ω =  Plots.plot(time_span_plot, ω_trajectories_plot', color=node_colors',
        xticks=false, xlims=xlims, ylims=ylims_ω, #xlabel=L"t", 
        ylabel=L"\omega", label=false, legend=false);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
    plt_p_solitary_mean = Plots.plot(time_span_plot, p_solitary_mean_longer_plot, color=node_colors[one_solitary_idx],
        xlims=xlims, ylims=ylims_p,
        yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]), label=false,
        xlabel=L"t", ylabel=L"p_s", legend=:right);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
    Plots.annotate!(-10,1.2,("sync",fontsize,:green));
    if value > t_pert_reso
        Plots.annotate!(30,1.2,("resonance",fontsize, :orange));
    end
    if value > t_pert_natu
        Plots.annotate!(75,1.2,("natural",fontsize), :red);
    end
    plt_ω_p_solitary = Plots.plot(plt_ω, plt_p_solitary_mean, layout=(2,1), size=size_pt,
        labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize, rightmargin=7Plots.mm)
    

#######

"""
xlims = (time_span_all[1], time_span_all[end])
ylims_ω = (min(-0.5, ω_trajectories...), max(ω_trajectories...))
ylims_p = (max(min(p_solitary_mean...),-0.3), max(p_solitary_mean...))
anim = @animate for (index, value) in enumerate(time_span_all)
    time_span_plot = @view time_span_all[1:index]
    ω_trajectories_plot = @view ω_trajectories[:, 1:index]
    p_solitary_mean_longer_plot = @view p_solitary_mean_longer[1:index]

    plt_ω =  Plots.plot(time_span_plot, ω_trajectories_plot', color=node_colors',
        xticks=false, xlims=xlims, ylims=ylims_ω, #xlabel=L"t", 
        ylabel=L"\omega", label=false, legend=false);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
    plt_p_solitary_mean = Plots.plot(time_span_plot, p_solitary_mean_longer_plot, color=node_colors[one_solitary_idx],
        xlims=xlims, ylims=ylims_p,
        yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]), label=false,
        xlabel=L"t", ylabel=L"p_s", legend=:topright);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
    Plots.annotate!(-15,1.3,("sync",fontsize,:green));
    if value > t_pert_reso
        Plots.annotate!(30,1.3,("resonance",fontsize, :orange));
    end
    if value > t_pert_natu
        Plots.annotate!(75,1.3,("natural",fontsize), :red);
    end
    plt_ω_p_solitary = Plots.plot(plt_ω, plt_p_solitary_mean, layout=(2,1), size=size_pt,
        labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize, rightmargin=7Plots.mm)
    
    print("\r"*string(value))
end every (resolution_factor)

mp4(anim, "animation_minimal_toy_t125_omega_1_p_solitary.mp4", fps = 24)
"""




##### circle plot together with line plots




# to try things out
index = 30000-n_points_shown
value = time_span_all[index]
time_span_plot = @view time_span_all[1:index]
ω_trajectories_plot = @view ω_trajectories[:, 1:index]
p_solitary_mean_longer_plot = @view p_solitary_mean_longer[1:index]
plt_ω =  Plots.plot(time_span_plot, ω_trajectories_plot', color=node_colors',
    xticks=false, xlims=xlims, ylims=ylims_ω, #xlabel=L"t", 
    ylabel=L"\omega_i", label=false, legend=false);
Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
plt_p_solitary_mean = Plots.plot(time_span_plot, p_solitary_mean_longer_plot, color=node_colors[one_solitary_idx],
    xlims=xlims, ylims=ylims_p,
    xticks=false, yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]), label=false,
    ylabel=L"p_s", legend=:right); #corrected legend position from :topright here to :bottomright in upper plot (ω₁)
Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
Plots.annotate!(-10,1.2,("sync",fontsize,:green));
if value > t_pert_reso
    Plots.annotate!(30,1.2,("resonance",fontsize, :orange));
end;
if value > t_pert_natu
    Plots.annotate!(75,1.2,("natural",fontsize), :red);
end;

### circle
time_span_plot = @view time_span_all[1:index]
x_trajectories_plot = @view x_trajectories[:, index:index+n_points_shown-1]
y_trajectories_plot = @view y_trajectories[:, index:index+n_points_shown-1]

# make gray ring in background
plt_circ = Plots.plot(cos.(ϕ),sin.(ϕ) ,xlims=(-1.1,1.2), ylims=(-1.1,1.2),
    seriesalpha=0.5, linewidth= scaler, color=:gray,
    xlabel=L"\cos\varphi_i", ylabel=L"\sin\varphi_i",
    aspect_ratio=1, label=false, labelfontsize=fontsize, legendfontsize=fontsize,
    tickfontsize=fontsize, size=size_pt);

Plots.plot!(x_trajectories_plot', y_trajectories_plot', color=node_colors',
seriesalpha=seriesalpha, linewidth=linewidth, label=false);

Plots.annotate!(0,0,Plots.text(raw"$t = " * string(round(value, digits=1)) * raw"$", Int(1.5*fontsize) ));

if t_pert_reso  <= value < t_pert_reso + show_frame_width
    annotate_alpha = 1 - abs(value - t_pert_reso)/show_frame_width
    Plots.annotate!(0,0.5,Plots.text(L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$", Int(1.5*fontsize), color=RGBA(0,0,0,annotate_alpha)))
end
if t_pert_natu  <= value < t_pert_natu + show_frame_width
    annotate_alpha = 1 - abs(value - t_pert_natu)/show_frame_width
    Plots.annotate!(0,-0.5,Plots.text(L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$", Int(1.5*fontsize), color=RGBA(0,0,0,annotate_alpha)))
end

plt_all = Plots.plot(plt_ω, plt_p_solitary_mean, plt_circ, layout=l, size=size_pt_all,
labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize,
leftmargin=[10Plots.mm 10Plots.mm 30Plots.mm],
topmargin=[0Plots.mm 0Plots.mm 0Plots.mm])
##########




l = @layout [
    [a{0.5h}; b{0.4h}] c
]

# size_inches_all = scaler .* (2*2*(3+3/8),  2*(3+3/8))
# size_pt_all = Int.(72 .* size_inches_all)
# size_pt_all = size_pt_all .- mod.(size_pt_all,2) .+ (1,0)
size_pt_all = (1942,970)
resolution_factor # as above
# prerequisits for line plots
    # have to rename xlims for one of the plots:
    # annotate "sync" "resonance" "natural"  at higher point because margin is higher in composite plot of all plots
xlims_ω_p_solitary = (time_span_all[1], time_span_all[end])
ylims_ω = (min(-0.5, ω_trajectories...), max(ω_trajectories...))
ylims_p = (max(min(p_solitary_mean...),-0.3), max(p_solitary_mean...))

# prerequisits for circle plot

graph = pg.graph
distances = map(idx -> length(Graphs.a_star(graph, one_solitary_idx, idx) ), plot_idxs)
distances[one_solitary_idx]
maximum(distances)

n_points_shown = 20 #2 #96
seriesalpha = range(0, 1, length = n_points_shown) #1 #range(0, 1, length = n_points_shown)
seriesalpha = repeat(seriesalpha, 1, length(plot_idxs))

linewidth = [(17 - d) *scaler *(n-1)/(n_points_shown-1) for n in 1:n_points_shown, d in distances]

# the event displays:
show_time = 8 #seconds at 24 fps, or at 12 if every 2nd is shown
show_frame_width = time_ratio * show_time
ϕ = 0 : 2*pi/1000 : 2*pi

# animation
anim_all = @animate for (index, value) in enumerate(time_span_all[1:end-n_points_shown])
    time_span_plot = @view time_span_all[1:index]
    ω_trajectories_plot = @view ω_trajectories[:, 1:index]
    p_solitary_mean_longer_plot = @view p_solitary_mean_longer[1:index]
    plt_ω =  Plots.plot(time_span_plot, ω_trajectories_plot', color=node_colors',
        xticks=false, xlims=xlims, ylims=ylims_ω, #xlabel=L"t", 
        ylabel=L"\omega_i", label=false, legend=false);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
    plt_p_solitary_mean = Plots.plot(time_span_plot, p_solitary_mean_longer_plot, color=node_colors[one_solitary_idx],
        xlims=xlims, ylims=ylims_p,
        xticks=false, yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]), label=false,
        ylabel=L"p_s", legend=:right); #corrected legend position from :topright here to :bottomright in upper plot (ω₁)
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$");
    Plots.annotate!(-10,1.2,("sync",fontsize,:green));
    if value > t_pert_reso
        Plots.annotate!(30,1.2,("resonance",fontsize, :orange));
    end;
    if value > t_pert_natu
        Plots.annotate!(75,1.2,("natural",fontsize), :red);
    end;

    ### circle
    time_span_plot = @view time_span_all[1:index]
    x_trajectories_plot = @view x_trajectories[:, index:index+n_points_shown-1]
    y_trajectories_plot = @view y_trajectories[:, index:index+n_points_shown-1]

    # make gray ring in background
    plt_circ = Plots.plot(cos.(ϕ),sin.(ϕ) ,xlims=(-1.1,1.2), ylims=(-1.1,1.2),
        seriesalpha=0.5, linewidth= scaler, color=:gray,
        xlabel=L"\cos\varphi_i", ylabel=L"\sin\varphi_i",
        aspect_ratio=1, label=false, labelfontsize=fontsize, legendfontsize=fontsize,
        tickfontsize=fontsize, size=size_pt);

    Plots.plot!(x_trajectories_plot', y_trajectories_plot', color=node_colors',
    seriesalpha=seriesalpha, linewidth=linewidth, label=false);

    Plots.annotate!(0,0,Plots.text(raw"$t = " * string(round(value, digits=1)) * raw"$", Int(1.5*fontsize) ));

    if t_pert_reso  <= value < t_pert_reso + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_reso)/show_frame_width
        Plots.annotate!(0,0.5,Plots.text(L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$", Int(1.5*fontsize), color=RGBA(0,0,0,annotate_alpha)))
    end
    if t_pert_natu  <= value < t_pert_natu + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_natu)/show_frame_width
        Plots.annotate!(0,-0.5,Plots.text(L"set $\omega_{75} \!+\!\!\!\!\!= 6.0$", Int(1.5*fontsize), color=RGBA(0,0,0,annotate_alpha)))
    end

    plt_all = Plots.plot(plt_ω, plt_p_solitary_mean, plt_circ, layout=l, size=size_pt_all,
    labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize,
    leftmargin=[10Plots.mm 10Plots.mm 30Plots.mm],
    topmargin=[0Plots.mm 0Plots.mm 0Plots.mm])

    print("\r"*string(value))
end every (resolution_factor)

mp4(anim_all, "animation_grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*"_all_125t_v2_20.mp4", fps = 24)
