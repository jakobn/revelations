

include(joinpath(@__DIR__, "../src/network_generation.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")
ensemble_path_local = joinpath(ensemble_path,"random_pgs/")
data_path = joinpath(@__DIR__,"../data/")
data_path_local = joinpath(data_path, "random_pgs")
plot_path = joinpath(@__DIR__,"../plots/")


topology_data = FileIO.load(joinpath(ensemble_path_local,"topology_data.jld"))
generation_parameters = FileIO.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

grid_idx = 1


pg, RHS, op = find_power_grid_path(ensemble_path_local, grid_idx)

where_dense_sprouts = topology_data["where_dense_sprouts"]
# node 74

# change power from -1. to -5.
node_idx = where_dense_sprouts[grid_idx][1]
node = pg.nodes[node_idx]
symbol = :P
P_new = -5.
P_old = getproperty(node, symbol)
ΔP = P_new - P_old

NPC = NodeParameterChange(node_idx, P_new, (0.,0.), symbol)
pg_new = NPC(pg)
pg_new.nodes[node_idx]

# compensate for power deviation
for idx in 1:N
    if idx != node_idx
        pg_new = NodeParameterChange(idx, getproperty(pg_new.nodes[idx], symbol) - ΔP/(N-1), (0.,0.), symbol)(pg_new)
    end
end

pg_new.nodes[node_idx]
pg_new.nodes[1]
sum(x -> getproperty(x, symbol), pg_new.nodes)
sum(x -> getproperty(x, symbol), pg.nodes)

op_new = find_operationpoint(pg_new, sol_method=:nlsolve)

write_powergrid(pg_new, joinpath( ensemble_path, "random_pgs", "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*".json"), Json)
# use test stability and acceptance of op - functionality of find_power_grid_path(9)
pg_load, RHS_load, op_load = find_power_grid_path(ensemble_path_local, "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new))

pg_load.nodes[node_idx]





# change power from -1. to -3.
node_idx = where_dense_sprouts[grid_idx][1]
node = pg.nodes[node_idx]
symbol = :P
P_new = -3.
P_old = getproperty(node, symbol)
ΔP = P_new - P_old

NPC = NodeParameterChange(node_idx, P_new, (0.,0.), symbol)
pg_new = NPC(pg)
pg_new.nodes[node_idx]

# compensate for power deviation
for idx in 1:N
    if idx != node_idx
        pg_new = NodeParameterChange(idx, getproperty(pg_new.nodes[idx], symbol) - ΔP/(N-1), (0.,0.), symbol)(pg_new)
    end
end

pg_new.nodes[node_idx]
pg_new.nodes[1]
sum(x -> getproperty(x, symbol), pg_new.nodes)
sum(x -> getproperty(x, symbol), pg.nodes)

op_new = find_operationpoint(pg_new, sol_method=:nlsolve)

write_powergrid(pg_new, joinpath( ensemble_path, "random_pgs", "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*".json"), Json)
# use test stability and acceptance of op - functionality of find_power_grid_path(9)
pg_load, RHS_load, op_load = find_power_grid_path(ensemble_path_local, "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new))

pg_load.nodes[node_idx]







# change power from -1. to -2.1 and α from 0.1 to 0.18
node_idx = where_dense_sprouts[grid_idx][1]
node = pg.nodes[node_idx]
symbol = :P
P_new = -2.1
P_old = getproperty(node, symbol)
ΔP = P_new - P_old

α_new = 0.18

NPC = NodeParameterChange(node_idx, P_new, (0.,0.), symbol)
pg_new = NPC(pg)
pg_new = NodeParameterChange(node_idx, α_new, (0.,0.), :D)(pg_new)
pg_new.nodes[node_idx]

# compensate for power deviation
for idx in 1:N
    if idx != node_idx
        pg_new = NodeParameterChange(idx, getproperty(pg_new.nodes[idx], symbol) - ΔP/(N-1), (0.,0.), symbol)(pg_new)
    end
end

pg_new.nodes[node_idx]
pg_new.nodes[1]
sum(x -> getproperty(x, symbol), pg_new.nodes)
sum(x -> getproperty(x, symbol), pg.nodes)

op_new = find_operationpoint(pg_new, sol_method=:nlsolve)

write_powergrid(pg_new, joinpath( ensemble_path, "random_pgs", "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*"_D_"*string(α_new)*".json"), Json)
# use test stability and acceptance of op - functionality of find_power_grid_path(9)
pg_load, RHS_load, op_load = find_power_grid_path(ensemble_path_local, "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*"_D_"*string(α_new))

pg_load.nodes[node_idx]