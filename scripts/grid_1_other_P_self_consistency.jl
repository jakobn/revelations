using CairoMakie
using JLD
using FileIO
using OrderedCollections
using Roots
using FixedPointNumbers # to load in the node_colors from topology_data

include(joinpath(@__DIR__, "../src/self_consistency.jl"))
include(joinpath(@__DIR__,"../src/laplacian.jl"))
include(joinpath(@__DIR__, "../src/network_generation.jl"))
include(joinpath(@__DIR__, "../src/plotting_functions.jl")) # for full_node_classification()
include(joinpath(@__DIR__, "../src/helper_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")
ensemble_path_local = joinpath(ensemble_path,"random_pgs/")
data_path = joinpath(@__DIR__,"../data/")
data_path_local = joinpath(data_path, "random_pgs")
# plot_path = joinpath(@__DIR__,"../plots/")
plot_path = joinpath(@__DIR__,"../plots/prl_v4")

size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
scaler = 1
size_pt = scaler .* 72 .* size_inches

# alpha = 0.02
marker1 = :xcross
color1 = palette(:tab10)[1] # (palette(:tab10)[1], alpha);
size1 = 12 * scaler


topology_data = FileIO.load(joinpath(ensemble_path_local,"topology_data.jld"))
generation_parameters = FileIO.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]


#as examples
grid_idx = 1
node_colors =  topology_data["node_colors"][grid_idx];
node_idx = 74
# P_74 = -5
P_new = -5.
symbol = :P
pg, RHS, op = find_power_grid_path(ensemble_path_local, "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new))
N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))
pg.nodes[node_idx]

# numerics
# sparse
# name = "_200t_20s_only_dw"
# tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*name*".jld"))
# dense
name = "_200t_50s_10dphi_120dw"
tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*name*".jld"))
mean_ωs = tail_statistics["mean_ωs"][one_solitary_idx:one_solitary_idx,:,:,:]

min_ω = minimum(mean_ωs)
max_ω = maximum(mean_ωs)
n_bins = 200
edges = [min_ω : (max_ω - min_ω)/n_bins : max_ω ...]
centers = edges[1:end-1] .+ (max_ω - min_ω)/n_bins

# filter for 1-solitaries
threshold = 1. # count as nonzero mean frequency
desynchronized_ωs_bool = abs.(mean_ωs) .> threshold
how_many_desynchronized_ωs = sum(desynchronized_ωs_bool, dims=4)
how_many_desynchronized_ωs_nonzero = how_many_desynchronized_ωs[findall(x -> (x > 0), how_many_desynchronized_ωs)]
where_desynchronized = findall(i -> (i>0), desynchronized_ωs_bool)
where_one_solitaries = findall(i -> isequal(1, i), how_many_desynchronized_ωs)
where_one_solitaries_with_node_i_idx = vcat(conditional_cartesian_idx_vector(where_desynchronized, where_one_solitaries, 1:3)... )
one_solitaries_mean_ωs = vcat(conditional_nodes_indexer(where_desynchronized, mean_ωs, where_one_solitaries, 1:3)...)
condition_cartesian_idx_vector = map(x -> CartesianIndex(0,0,0,x), 1:N)
# sort by node idx
one_solitaries_mean_ωs_by_node_i_idx = conditional_nodes_indexer(where_one_solitaries_with_node_i_idx, mean_ωs, condition_cartesian_idx_vector, 4)

solitary_node = pg.nodes[one_solitary_idx]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])
amplitude_factors = all_amplitude_factors[1]
node_parameters_dict = generation_parameters["node_parameters"]
α = node_parameters_dict[:D]
m = 2* node_parameters_dict[:H]
α_solitary = solitary_node.D
m_solitary = 2 * solitary_node.H
P_solitary = solitary_node.P
ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

step = 0.001
# omit zero due to divergence
ω_range_neg = ω_min:step:-step
ω_range_pos = step:step:ω_max
ω_range = vcat(ω_range_neg, ω_range_pos)
p_range_neg = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range_neg)
p_range_pos = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range_pos)

reduced_eigenvalues_weights = amplitude_factors ./ maximum(amplitude_factors)
sqrt.(abs.(reduced_eigenvalues)) # abs because the 0 eigenvalue can be ~ -eps
shifted_signed_modes = sign(P_solitary) .* static_frequency.(sqrt.(abs.(reduced_eigenvalues)), α, α_solitary, N, N_slacks)


# evaluation of solutions and stability
# have set no_pts to over ~10 to get all solutions in the wide interval [ω_min, ω_max]
one_solitary_solutions = find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, P_solitary, α, α_solitary, N, N_slacks), ω_min, ω_max, no_pts = 50)
one_solitary_stabilities = map(ω -> self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), one_solitary_solutions)
stable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities)]
stable_one_solitary_solutions_physical = stable_one_solitary_solutions[abs.(stable_one_solitary_solutions) .> 10*eps(Float64)]
unstable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities .== 0)]


#make the plot
Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega_s$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P_{%$one_solitary_idx} - \alpha_{%$one_solitary_idx} \, \omega_s")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range_neg, p_range_neg)
Makie.lines!(ax, ω_range_pos, p_range_pos, label=L"$p_s$", color=Makie.wong_colors(1)[2])
for l in 1:length(reduced_eigenvalues)
    Makie.vlines!(shifted_signed_modes[l], color=(:black, reduced_eigenvalues_weights[l]), linewidth=1, linestyle=:dot)
end
Makie.vlines!([],color=:black, linewidth=1, linestyle=:dot, label=L"$$eigenmodes")

Makie.xlims!(ax, [-11, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
leg = axislegend(position = (0.6, 0.3))

unstablescatter = Makie.scatter!(unstable_one_solitary_solutions, P_solitary .- α_solitary .* unstable_one_solitary_solutions, marker='u', markersize=16, color=(:red, 0.7), label=L"$$unstable solution")

mean_ωs_hist = normalize(fit(StatsBase.Histogram, one_solitaries_mean_ωs_by_node_i_idx[one_solitary_idx], edges), mode=:probability)
# here: normalize by largest weight to increase visibility
color=map(x -> (node_colors[one_solitary_idx], 1*x), mean_ωs_hist.weights./maximum(mean_ωs_hist.weights))
numerics_scatter = Makie.scatter!(centers, P_solitary .- α_solitary .* centers, color=color, markersize=size1*4/3, marker = marker1)
numerics_scatter_for_leg2 = Makie.scatter!([NaN], [NaN], color=node_colors[one_solitary_idx], markersize=size1*4/3, marker = marker1)

stablescatter = Makie.scatter!(stable_one_solitary_solutions_physical, P_solitary .- α_solitary .* stable_one_solitary_solutions_physical, marker='s', markersize=16, color=:green, label=L"$$stable solution")

leg2 = Legend(Fig[1,1], [stablescatter, unstablescatter, numerics_scatter_for_leg2], [L"$$stable solution",L"$$unstable solution",L"$$simulation"],
    halign=0.01, valign=0.50, tellwidth=false, tellheight=false)
Fig



# FileIO.save(joinpath(plot_path, "grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*"_"*string(symbol)*"_"*string(P_new)*"_self_consistency_intersection_modes.pdf"), Fig)

Plots.plot(centers, mean_ωs_hist.weights)
# shows that only natural frequency is found here
"""
Makie.ylims!(-1.1,0)
Fig

Makie.xlims!(ax, [-11., ω_max])
Makie.ylims!(ax, sort(-1. .- α_solitary.* [-11., ω_max]))
Fig
"""





where_dense_sprouts = topology_data["where_dense_sprouts"]
# node 74
one_solitary_idx = where_dense_sprouts[grid_idx][1]
# P_74 = -3

P_new = -3.
symbol = :P
pg, RHS, op = find_power_grid_path(ensemble_path_local, "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new))
N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))
pg.nodes[node_idx]

# numerics
# not there (yet)

# sparse
# name = "_200t_20s_only_dw"
# tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*name*".jld"))
# dense
# careful, array contains many random undef values!
name = "_200t_50s_10dphi_120dw"
tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*name*".jld"))
mean_ωs = tail_statistics["mean_ωs"][one_solitary_idx:one_solitary_idx,:,:,:]


min_ω = minimum(mean_ωs)
max_ω = maximum(mean_ωs)
n_bins = 200
edges = [min_ω : (max_ω - min_ω)/n_bins : max_ω ...]
centers = edges[1:end-1] .+ (max_ω - min_ω)/n_bins

# filter for 1-solitaries
threshold = 1. # count as nonzero mean frequency
desynchronized_ωs_bool = abs.(mean_ωs) .> threshold
how_many_desynchronized_ωs = sum(desynchronized_ωs_bool, dims=4)
how_many_desynchronized_ωs_nonzero = how_many_desynchronized_ωs[findall(x -> (x > 0), how_many_desynchronized_ωs)]
where_desynchronized = findall(i -> (i>0), desynchronized_ωs_bool)
where_one_solitaries = findall(i -> isequal(1, i), how_many_desynchronized_ωs)
where_one_solitaries_with_node_i_idx = vcat(conditional_cartesian_idx_vector(where_desynchronized, where_one_solitaries, 1:3)... )
one_solitaries_mean_ωs = vcat(conditional_nodes_indexer(where_desynchronized, mean_ωs, where_one_solitaries, 1:3)...)
condition_cartesian_idx_vector = map(x -> CartesianIndex(0,0,0,x), 1:N)
# sort by node idx
one_solitaries_mean_ωs_by_node_i_idx = conditional_nodes_indexer(where_one_solitaries_with_node_i_idx, mean_ωs, condition_cartesian_idx_vector, 4)

solitary_node = pg.nodes[one_solitary_idx]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])
amplitude_factors = all_amplitude_factors[1]
node_parameters_dict = generation_parameters["node_parameters"]
α = node_parameters_dict[:D]
m = 2* node_parameters_dict[:H]
α_solitary = solitary_node.D
m_solitary = 2 * solitary_node.H
P_solitary = solitary_node.P
ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

step = 0.001
# omit zero due to divergence
ω_range_neg = ω_min:step:-step
ω_range_pos = step:step:ω_max
ω_range = vcat(ω_range_neg, ω_range_pos)
p_range_neg = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range_neg)
p_range_pos = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range_pos)

reduced_eigenvalues_weights = amplitude_factors ./ maximum(amplitude_factors)
sqrt.(abs.(reduced_eigenvalues)) # abs because the 0 eigenvalue can be ~ -eps
shifted_signed_modes = sign(P_solitary) .* static_frequency.(sqrt.(abs.(reduced_eigenvalues)), α, α_solitary, N, N_slacks)


# evaluation of solutions and stability
# have set no_pts to over ~10 to get all solutions in the wide interval [ω_min, ω_max]
one_solitary_solutions = find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, P_solitary, α, α_solitary, N, N_slacks), ω_min, ω_max, no_pts = 50)
one_solitary_stabilities = map(ω -> self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), one_solitary_solutions)
stable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities)]
stable_one_solitary_solutions_physical = stable_one_solitary_solutions[abs.(stable_one_solitary_solutions) .> 10*eps(Float64)]
unstable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities .== 0)]


#make the plot
Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega_s$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P_{%$one_solitary_idx} - \alpha_{%$one_solitary_idx} \, \omega_s")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range_neg, p_range_neg)
Makie.lines!(ax, ω_range_pos, p_range_pos, label=L"$p_s$", color=Makie.wong_colors(1)[2])
for l in 1:length(reduced_eigenvalues)
    Makie.vlines!(shifted_signed_modes[l], color=(:black, reduced_eigenvalues_weights[l]), linewidth=1, linestyle=:dot)
end
Makie.vlines!([],color=:black, linewidth=1, linestyle=:dot, label=L"$$eigenmodes")

Makie.xlims!(ax, [-11, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
leg = axislegend(position = (0.6, 0.3))

unstablescatter = Makie.scatter!(unstable_one_solitary_solutions, P_solitary .- α_solitary .* unstable_one_solitary_solutions, marker='u', markersize=16, color=(:red, 0.7), label=L"$$unstable solution")

mean_ωs_hist = normalize(fit(StatsBase.Histogram, one_solitaries_mean_ωs_by_node_i_idx[one_solitary_idx], edges), mode=:probability)
# here: normalize by largest weight to increase visibility
color=map(x -> (node_colors[one_solitary_idx], 1*x), mean_ωs_hist.weights./maximum(mean_ωs_hist.weights))
numerics_scatter = Makie.scatter!(centers, P_solitary .- α_solitary .* centers, color=color, markersize=size1*4/3, marker = marker1)
numerics_scatter_for_leg2 = Makie.scatter!([NaN], [NaN], color=node_colors[one_solitary_idx], markersize=size1*4/3, marker = marker1)

stablescatter = Makie.scatter!(stable_one_solitary_solutions_physical, P_solitary .- α_solitary .* stable_one_solitary_solutions_physical, marker='s', markersize=16, color=:green, label=L"$$stable solution")

leg2 = Legend(Fig[1,1], [stablescatter, unstablescatter, numerics_scatter_for_leg2], [L"$$stable solution",L"$$unstable solution",L"$$simulation"],
    halign=0.01, valign=0.50, tellwidth=false, tellheight=false)
Fig

Plots.plot(centers, mean_ωs_hist.weights)




# try which P and α are good
P_try = -2.1
α_try = 0.18

P_try/α_try

Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega_s$")

Makie.lines!(ax, ω_range, P_try .- α_try .* ω_range, label=L"$P_{%$one_solitary_idx} - \alpha_{%$one_solitary_idx} \, \omega_s")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range_neg, p_range_neg)
Makie.lines!(ax, ω_range_pos, p_range_pos, label=L"$p_s$", color=Makie.wong_colors(1)[2])
Makie.xlims!(ax, [-11, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
Fig







# P_74 = -2.1
where_dense_sprouts = topology_data["where_dense_sprouts"]
# node 74
one_solitary_idx = where_dense_sprouts[grid_idx][1]

P_new = -2.1
α_new = 0.18
symbol = :P
pg, RHS, op = find_power_grid_path(ensemble_path_local, "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*"_D_"*string(α_new))
N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))
pg.nodes[node_idx]

# numerics
# not there (yet)

# sparse
# name = "_200t_20s_only_dw"
# tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*name*".jld"))
# dense
# careful, array contains many random undef values!
name = "_200t_50s_10dphi_120dw"
tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*"_D_"*string(α_new)*name*".jld"))
mean_ωs = tail_statistics["mean_ωs"][one_solitary_idx:one_solitary_idx,:,:,:]

min_ω = minimum(mean_ωs)
max_ω = maximum(mean_ωs)
n_bins = 200
edges = [min_ω : (max_ω - min_ω)/n_bins : max_ω ...]
centers = edges[1:end-1] .+ (max_ω - min_ω)/n_bins

# filter for 1-solitaries
threshold = 1. # count as nonzero mean frequency
desynchronized_ωs_bool = abs.(mean_ωs) .> threshold
how_many_desynchronized_ωs = sum(desynchronized_ωs_bool, dims=4)
how_many_desynchronized_ωs_nonzero = how_many_desynchronized_ωs[findall(x -> (x > 0), how_many_desynchronized_ωs)]
where_desynchronized = findall(i -> (i>0), desynchronized_ωs_bool)
where_one_solitaries = findall(i -> isequal(1, i), how_many_desynchronized_ωs)
where_one_solitaries_with_node_i_idx = vcat(conditional_cartesian_idx_vector(where_desynchronized, where_one_solitaries, 1:3)... )
one_solitaries_mean_ωs = vcat(conditional_nodes_indexer(where_desynchronized, mean_ωs, where_one_solitaries, 1:3)...)
condition_cartesian_idx_vector = map(x -> CartesianIndex(0,0,0,x), 1:N)
# sort by node idx
one_solitaries_mean_ωs_by_node_i_idx = conditional_nodes_indexer(where_one_solitaries_with_node_i_idx, mean_ωs, condition_cartesian_idx_vector, 4)

solitary_node = pg.nodes[one_solitary_idx]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])
amplitude_factors = all_amplitude_factors[1]
node_parameters_dict = generation_parameters["node_parameters"]
α = node_parameters_dict[:D]
m = 2* node_parameters_dict[:H]
α_solitary = solitary_node.D
m_solitary = 2 * solitary_node.H
P_solitary = solitary_node.P
ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

step = 0.001
# omit zero due to divergence
ω_range_neg = ω_min:step:-step
ω_range_pos = step:step:ω_max
ω_range = vcat(ω_range_neg, ω_range_pos)
p_range_neg = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range_neg)
p_range_pos = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range_pos)

reduced_eigenvalues_weights = amplitude_factors ./ maximum(amplitude_factors)
sqrt.(abs.(reduced_eigenvalues)) # abs because the 0 eigenvalue can be ~ -eps
shifted_signed_modes = sign(P_solitary) .* static_frequency.(sqrt.(abs.(reduced_eigenvalues)), α, α_solitary, N, N_slacks)


# evaluation of solutions and stability
# have set no_pts to over ~10 to get all solutions in the wide interval [ω_min, ω_max]
one_solitary_solutions = find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, P_solitary, α, α_solitary, N, N_slacks), ω_min, ω_max, no_pts = 50)
one_solitary_stabilities = map(ω -> self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), one_solitary_solutions)
stable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities)]
stable_one_solitary_solutions_physical = stable_one_solitary_solutions[abs.(stable_one_solitary_solutions) .> 10*eps(Float64)]
unstable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities .== 0)]


#make the plot
Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*15);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega_s$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P_{%$one_solitary_idx} - \alpha_{%$one_solitary_idx} \, \omega_s")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range_neg, p_range_neg)
Makie.lines!(ax, ω_range_pos, p_range_pos, label=L"$p_s$", color=Makie.wong_colors(1)[2])
for l in 1:length(reduced_eigenvalues)
    Makie.vlines!(shifted_signed_modes[l], color=(:black, reduced_eigenvalues_weights[l]), linewidth=1, linestyle=:dot)
end
Makie.vlines!([],color=:black, linewidth=1, linestyle=:dot, label=L"$$eigenmodes")

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
leg = axislegend(position = (0.98, 0.95))

unstablescatter = Makie.scatter!(unstable_one_solitary_solutions, P_solitary .- α_solitary .* unstable_one_solitary_solutions, marker='u', markersize=16, color=(:red, 0.7), label=L"$$unstable solution")

mean_ωs_hist = normalize(fit(StatsBase.Histogram, one_solitaries_mean_ωs_by_node_i_idx[one_solitary_idx], edges), mode=:probability)
# here: normalize by largest weight to increase visibility

### also x*6 for higher visibility
color=map(x -> (node_colors[one_solitary_idx], 6*x), mean_ωs_hist.weights./maximum(mean_ωs_hist.weights))
numerics_scatter = Makie.scatter!(centers, P_solitary .- α_solitary .* centers, color=color, markersize=size1*4/3, marker = marker1)
numerics_scatter_for_leg2 = Makie.scatter!([NaN], [NaN], color=node_colors[one_solitary_idx], markersize=size1*4/3, marker = marker1)

stablescatter = Makie.scatter!(stable_one_solitary_solutions_physical, P_solitary .- α_solitary .* stable_one_solitary_solutions_physical, marker='s', markersize=16, color=:green, label=L"$$stable solution")

leg2 = Legend(Fig[1,1], [stablescatter, unstablescatter, numerics_scatter_for_leg2], [L"$$stable solution",L"$$unstable solution",L"$$simulation"],
    halign=0.01, valign=0.03, tellwidth=false, tellheight=false)
Fig

FileIO.save(joinpath(plot_path, "grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*"_"*string(symbol)*"_"*string(P_new)*"_D_"*string(α_new)*"_self_consistency_intersection_modes_numerics_hist_only_solitaries_alpha_maxnorm_x6"*name*"single_column.pdf"), Fig)

Plots.plot(centers, mean_ωs_hist.weights)