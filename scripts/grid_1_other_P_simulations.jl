"""
Make simulations for the nodes with altered power injection (only same node perturbed)
"""
#
using FileIO
using JLD
using Distributed
# using OrdinaryDiffEq
# using PowerDynamics

include(joinpath(@__DIR__,"../src/network_generation.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))
include(joinpath(@__DIR__,"../src/simulation_functions.jl"))
include(joinpath(@__DIR__, "../src/plotting_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")
ensemble_path_local = joinpath(ensemble_path,"random_pgs/")
data_path = joinpath(@__DIR__,"../data/")
data_path_local = joinpath(data_path, "random_pgs/")
plot_path = joinpath(@__DIR__,"../plots/")

# now with 10 dϕ and 120 dω (and Tsit5)
t_sim = (0.,200.) #nomenclature 200t (transient), 20s (statistics)
t_stat = 50. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/10000 : t_sim[2]
abstol = 1e-3
reltol = 1e-3


topology_data = JLD.load(joinpath(ensemble_path_local,"topology_data.jld"))
generation_parameters = JLD.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

n_initial_ϕ = 10
n_initial_ω = 120
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -5.
ω_max_incl = 6.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω


where_dense_sprouts = topology_data["where_dense_sprouts"]
where_dense_sprouts[grid_idx]

# for grid 1, node 74, P=-5.
node_idx_range = 74
symbol = :P
P_new = -5.

@time for grid_idx in 1:1 # 1:n_grids_later

    pg, RHS, op =  find_power_grid_path(ensemble_path_local, "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new))
    

    # dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic
    mean_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))
    std_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))

    # plot_idxs = collect(1:N)

    #@distributed
    for (dϕ_idx,  dω_idx, node_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
        dϕ = perturbation_range_ϕ[dϕ_idx]
        dω = perturbation_range_ω[dω_idx] + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
        solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
        # pg_solution = PowerGridSolution(solution, pg)
        # myplot = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)
        # display(myplot)
        mean_ωs[node_idx, dϕ_idx, dω_idx, :], std_ωs[node_idx, dϕ_idx, dω_idx, :] = calculate_statistics_ω(solution, t_stat_iter)
        println(node_idx)
    end

    FileIO.save(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*"_200t_50s_10dphi_120dw.jld"), "mean_ωs", mean_ωs, "std_ωs", std_ωs)

    FileIO.save(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*"_200t_50s_10dphi_120dw.jld"),
        "perturbation_range_ϕ", perturbation_range_ϕ, "perturbation_range_ω", perturbation_range_ω,
        "t_sim", t_sim, "t_stat_iter", t_stat_iter, "node_idx_range", node_idx_range,
        "abstol", abstol, "reltol", reltol)

end

# 27234 s with Tsit5()





# for grid 1, node 74, P=-3.
node_idx_range = 74
symbol = :P
P_new = -3.

@time for grid_idx in 1:1 # 1:n_grids_later

    pg, RHS, op =  find_power_grid_path(ensemble_path_local, "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new))
    

    # dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic
    mean_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))
    std_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))

    # plot_idxs = collect(1:N)

    #@distributed
    for (dϕ_idx,  dω_idx, node_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
        dϕ = perturbation_range_ϕ[dϕ_idx]
        dω = perturbation_range_ω[dω_idx] + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
        solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
        # pg_solution = PowerGridSolution(solution, pg)
        # myplot = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)
        # display(myplot)
        mean_ωs[node_idx, dϕ_idx, dω_idx, :], std_ωs[node_idx, dϕ_idx, dω_idx, :] = calculate_statistics_ω(solution, t_stat_iter)
        println("dϕ_idx = $dϕ_idx, dω_idx = $dω_idx")
    end

    FileIO.save(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*"_200t_50s_10dphi_120dw.jld"), "mean_ωs", mean_ωs, "std_ωs", std_ωs)

    FileIO.save(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*"_200t_50s_10dphi_120dw.jld"),
        "perturbation_range_ϕ", perturbation_range_ϕ, "perturbation_range_ω", perturbation_range_ω,
        "t_sim", t_sim, "t_stat_iter", t_stat_iter, "node_idx_range", node_idx_range,
        "abstol", abstol, "reltol", reltol)

end

# 683 s with Tsit5()







# for grid 1, node 74, P=-3.
node_idx_range = 74
symbol = :P
P_new = -2.1
α_new = 0.18

pg, RHS, op =  find_power_grid_path(ensemble_path_local, "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*"_D_"*string(α_new))


dϕ = 0.
dω = -7.
solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
t_step = 0.01
t_sim_iter = t_sim[1] : t_step : t_sim[end] 

sol_t = solution.t
pg_sol = PowerGridSolution(solution, pg)
# sol_ωs = solution(solution.t ,idxs=3:3:3*N).u # this is a vector of vectors. better is:
# sum.(sol_ωs)
pg_sol_ωs = pg_sol(sol_t, :, :ω)
sum_ωs = sum(pg_sol_ωs, dims=1 )
sum_ω_plt = Plots.plot(sol_t, sum_ωs', label=L"\sum_i \omega_i" ,xlabel=L"t", ylabel=L"\omega")
Plots.plot!(sol_t, (α - α_new)/α .* pg_sol_ωs[node_idx,:], label=L"\left(1 - \frac{\alpha_{new}}{\alpha} \right) \omega_{74}")
FileIO.save(joinpath(plot_path, "sum_frequencies_heterogeneous_alpha.pdf"), sum_ω_plt)


frequencies_plt = Plots.plot(sol_t, pg_sol_ωs', legend=false, xlabel=L"t", ylabel=L"\omega")
FileIO.save(joinpath(plot_path, "all_frequencies_heterogeneous_alpha.pdf"), frequencies_plt)



@time for grid_idx in 1:1 # 1:n_grids_later

    pg, RHS, op =  find_power_grid_path(ensemble_path_local, "grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*"_D_"*string(α_new))
    

    # dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic
    mean_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))
    std_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))

    # plot_idxs = collect(1:N)

    #@distributed
    for (dϕ_idx,  dω_idx, node_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
        dϕ = perturbation_range_ϕ[dϕ_idx]
        dω = perturbation_range_ω[dω_idx] + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
        solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
        # pg_solution = PowerGridSolution(solution, pg)
        # myplot = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)
        # display(myplot)
        mean_ωs[node_idx, dϕ_idx, dω_idx, :], std_ωs[node_idx, dϕ_idx, dω_idx, :] = calculate_statistics_ω(solution, t_stat_iter)
        println("dϕ_idx = $dϕ_idx, dω_idx = $dω_idx")
    end

    FileIO.save(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*"_D_"*string(α_new)*"_200t_50s_10dphi_120dw.jld"), "mean_ωs", mean_ωs, "std_ωs", std_ωs)

    FileIO.save(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*"_node_"*string(node_idx)*"_"*string(symbol)*"_"*string(P_new)*"_D_"*string(α_new)*"_200t_50s_10dphi_120dw.jld"),
        "perturbation_range_ϕ", perturbation_range_ϕ, "perturbation_range_ω", perturbation_range_ω,
        "t_sim", t_sim, "t_stat_iter", t_stat_iter, "node_idx_range", node_idx_range,
        "abstol", abstol, "reltol", reltol)

end

# 282 s with Tsit5()