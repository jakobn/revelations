using CairoMakie
using JLD
using FileIO
using OrderedCollections
using Roots
using FixedPointNumbers # to load in the node_colors from topology_data

include(joinpath(@__DIR__, "../src/self_consistency.jl"))
include(joinpath(@__DIR__,"../src/laplacian.jl"))
include(joinpath(@__DIR__, "../src/network_generation.jl"))
include(joinpath(@__DIR__, "../src/plotting_functions.jl")) # for full_node_classification()
include(joinpath(@__DIR__, "../src/helper_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")
ensemble_path_local = joinpath(ensemble_path,"random_pgs/")
data_path = joinpath(@__DIR__,"../data/")
data_path_local = joinpath(data_path, "random_pgs")
plot_path = joinpath(@__DIR__,"../plots/")


size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
scaler = 1
size_pt = scaler .* 72 .* size_inches

# alpha = 0.02
marker1 = :xcross
color1 = palette(:tab10)[1] # (palette(:tab10)[1], alpha);
size1 = 12 * scaler


topology_data = FileIO.load(joinpath(ensemble_path_local,"topology_data.jld"))
generation_parameters = FileIO.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]


#as examples
grid_idx = 1
node_colors =  topology_data["node_colors"][grid_idx];

pg, RHS, op = find_power_grid_path(ensemble_path_local, grid_idx)
N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))

# numerics
# sparse
# name = "_200t_20s_only_dw"
# tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*name*".jld"))
# dense
name = "_200t_50s_10dphi_120dw"
tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*name*".jld"))
mean_ωs = tail_statistics["mean_ωs"]

min_ω = minimum(mean_ωs)
max_ω = maximum(mean_ωs)
n_bins = 200
edges = [min_ω : (max_ω - min_ω)/n_bins : max_ω ...]
centers = edges[1:end-1] .+ (max_ω - min_ω)/n_bins

# filter for 1-solitaries
solitary_frequency_data = FileIO.load(joinpath(data_path_local, "solitary_frequency_data_grid_"*string(grid_idx)*name*".jld"))
where_one_solitaries_with_node_i_idx = solitary_frequency_data["idxs"]
one_solitaries_mean_ωs = solitary_frequency_data["ωs"]

condition_cartesian_idx_vector = map(x -> CartesianIndex(0,0,0,x), 1:N)
# sort by node idx
one_solitaries_mean_ωs_by_node_i_idx = conditional_nodes_indexer(where_one_solitaries_with_node_i_idx, mean_ωs, condition_cartesian_idx_vector, 4)


where_dense_sprouts = topology_data["where_dense_sprouts"]
# node 74 <dont filter)
one_solitary_idx = where_dense_sprouts[grid_idx][1]
Plots.scatter(one_solitaries_mean_ωs_by_node_i_idx[one_solitary_idx])

solitary_node = pg.nodes[one_solitary_idx]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])
amplitude_factors = all_amplitude_factors[1]
node_parameters_dict = generation_parameters["node_parameters"]
α = node_parameters_dict[:D]
m = 2* node_parameters_dict[:H]
α_solitary = solitary_node.D
m_solitary = 2 * solitary_node.H
P_solitary = solitary_node.P
ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

step = 0.001
# omit zero due to divergence
ω_range_neg = ω_min:step:-step
ω_range_pos = step:step:ω_max
ω_range = vcat(ω_range_neg, ω_range_pos)
p_range_neg = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range_neg)
p_range_pos = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range_pos)

reduced_eigenvalues_weights = amplitude_factors ./ maximum(amplitude_factors)
sqrt.(abs.(reduced_eigenvalues)) # abs because the 0 eigenvalue can be ~ -eps
shifted_signed_modes = sign(P_solitary) .* static_frequency.(sqrt.(abs.(reduced_eigenvalues)), α, α_solitary, N, N_slacks)

# evaluation of solutions and stability
one_solitary_solutions = find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, P_solitary, α, α_solitary, N, N_slacks), ω_min, ω_max)
one_solitary_stabilities = map(ω -> self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), one_solitary_solutions)
stable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities)]
stable_one_solitary_solutions_physical = stable_one_solitary_solutions[abs.(stable_one_solitary_solutions) .> 10*eps(Float64)]
unstable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities .== 0)]
# expected behaviour.
# However, you need to cut the unphysical quasi-zero "solution" (10^-30) due to numerical inaccuracy

#make the plot
Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega_s$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P_{%$one_solitary_idx} - \alpha \omega_s")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range_neg, p_range_neg)
Makie.lines!(ax, ω_range_pos, p_range_pos, label=L"$p_s$", color=Makie.wong_colors(1)[2])
for l in 1:length(reduced_eigenvalues)
    Makie.vlines!(shifted_signed_modes[l], color=(:black, reduced_eigenvalues_weights[l]), linewidth=1, linestyle=:dot)
end
Makie.vlines!([],color=:black, linewidth=1, linestyle=:dot, label=L"$$eigenmodes")

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
leg = axislegend(position = :rt)

unstablescatter = Makie.scatter!(unstable_one_solitary_solutions, P_solitary .- α_solitary .* unstable_one_solitary_solutions, marker='u', markersize=16, color=(:red, 0.7), label=L"$$unstable solution")

mean_ωs_hist = normalize(fit(StatsBase.Histogram, [mean_ωs[:, :, :, one_solitary_idx]...], edges), mode=:probability)
color=map(x -> (node_colors[one_solitary_idx], 200*x), mean_ωs_hist.weights)
numerics_scatter = Makie.scatter!(centers, P_solitary .- α_solitary .* centers, color=color, markersize=size1*4/3, marker = marker1)
numerics_scatter_for_leg2 = Makie.scatter!([NaN], [NaN], color=node_colors[one_solitary_idx], markersize=size1*4/3, marker = marker1)

stablescatter = Makie.scatter!(stable_one_solitary_solutions_physical, P_solitary .- α_solitary .* stable_one_solitary_solutions_physical, marker='s', markersize=16, color=:green, label=L"$$stable solution")

leg2 = Legend(Fig[1,1], [stablescatter, unstablescatter, numerics_scatter_for_leg2], [L"$$stable solution",L"$$unstable solution",L"$$simulation"],
    halign=0.03, valign=0.10, tellwidth=false, tellheight=false)
Fig
FileIO.save(joinpath(plot_path, "grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*"_self_consistency_intersection_modes_numerics_hist_alphax200"*name*".pdf"), Fig)

Makie.ylims!(-4,0)
Fig






# filter for 1-solitaries (is cut pasted to top of document)

#make the plot
Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega_s$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P_{%$one_solitary_idx} - \alpha \omega_s")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range_neg, p_range_neg)
Makie.lines!(ax, ω_range_pos, p_range_pos, label=L"$p_s$", color=Makie.wong_colors(1)[2])
for l in 1:length(reduced_eigenvalues)
    Makie.vlines!(shifted_signed_modes[l], color=(:black, reduced_eigenvalues_weights[l]), linewidth=1, linestyle=:dot)
end
Makie.vlines!([],color=:black, linewidth=1, linestyle=:dot, label=L"$$eigenmodes")

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
leg = axislegend(position = :rt)

unstablescatter = Makie.scatter!(unstable_one_solitary_solutions, P_solitary .- α_solitary .* unstable_one_solitary_solutions, marker='u', markersize=16, color=(:red, 0.7), label=L"$$unstable solution")

mean_ωs_hist = normalize(fit(StatsBase.Histogram, one_solitaries_mean_ωs_by_node_i_idx[one_solitary_idx], edges), mode=:probability)
# here: normalize by largest weight to increase visibility
color=map(x -> (node_colors[one_solitary_idx], 1*x), mean_ωs_hist.weights./maximum(mean_ωs_hist.weights))
numerics_scatter = Makie.scatter!(centers, P_solitary .- α_solitary .* centers, color=color, markersize=size1*4/3, marker = marker1)
numerics_scatter_for_leg2 = Makie.scatter!([NaN], [NaN], color=node_colors[one_solitary_idx], markersize=size1*4/3, marker = marker1)

stablescatter = Makie.scatter!(stable_one_solitary_solutions_physical, P_solitary .- α_solitary .* stable_one_solitary_solutions_physical, marker='s', markersize=16, color=:green, label=L"$$stable solution")

leg2 = Legend(Fig[1,1], [stablescatter, unstablescatter, numerics_scatter_for_leg2], [L"$$stable solution",L"$$unstable solution",L"$$simulation"],
    halign=0.03, valign=0.10, tellwidth=false, tellheight=false)
Fig
FileIO.save(joinpath(plot_path, "grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*"_self_consistency_intersection_modes_numerics_hist_only_solitaries"*name*".pdf"), Fig)

Makie.ylims!(-4,0)
Fig





# for v4: half page width (this is with standard params! See other script grid_1_other_P_self_consistency)
scaler = 1
fontsize_new = 15

Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*fontsize_new);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega_s$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P_{%$one_solitary_idx} - \alpha \omega_s")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range_neg, p_range_neg)
Makie.lines!(ax, ω_range_pos, p_range_pos, label=L"$p_s$", color=Makie.wong_colors(1)[2])
for l in 1:length(reduced_eigenvalues)
    Makie.vlines!(shifted_signed_modes[l], color=(:black, reduced_eigenvalues_weights[l]), linewidth=1, linestyle=:dot)
end
Makie.vlines!([],color=:black, linewidth=1, linestyle=:dot, label=L"$$eigenmodes")

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
leg = axislegend(position = :rt)

unstablescatter = Makie.scatter!(unstable_one_solitary_solutions, P_solitary .- α_solitary .* unstable_one_solitary_solutions, marker='u', markersize=16, color=(:red, 0.7), label=L"$$unstable solution")

mean_ωs_hist = normalize(fit(StatsBase.Histogram, one_solitaries_mean_ωs_by_node_i_idx[one_solitary_idx], edges), mode=:probability)
# here: normalize by largest weight to increase visibility
color=map(x -> (node_colors[one_solitary_idx], 1*x), mean_ωs_hist.weights./maximum(mean_ωs_hist.weights))
numerics_scatter = Makie.scatter!(centers, P_solitary .- α_solitary .* centers, color=color, markersize=size1*4/3, marker = marker1)
numerics_scatter_for_leg2 = Makie.scatter!([NaN], [NaN], color=node_colors[one_solitary_idx], markersize=size1*4/3, marker = marker1)

stablescatter = Makie.scatter!(stable_one_solitary_solutions_physical, P_solitary .- α_solitary .* stable_one_solitary_solutions_physical, marker='s', markersize=16, color=:green, label=L"$$stable solution")

leg2 = Legend(Fig[1,1], [stablescatter, unstablescatter, numerics_scatter_for_leg2], [L"$$stable solution",L"$$unstable solution",L"$$simulation"],
    halign=0.03, valign=0.10, tellwidth=false, tellheight=false)
Fig
#FileIO.save(joinpath(plot_path, "grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*"_self_consistency_intersection_modes_numerics_hist_only_solitaries"*name*"_single_column.pdf"), Fig)








# node 75
one_solitary_idx = where_dense_sprouts[grid_idx][2]

solitary_node = pg.nodes[one_solitary_idx]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])
amplitude_factors = all_amplitude_factors[1]
node_parameters_dict = generation_parameters["node_parameters"]
α = node_parameters_dict[:D]
m = 2* node_parameters_dict[:H]
α_solitary = solitary_node.D
m_solitary = 2 * solitary_node.H
P_solitary = solitary_node.P
ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

step = 0.001
# omit zero due to divergence
ω_range_neg = ω_min:step:-step
ω_range_pos = step:step:ω_max
ω_range = vcat(ω_range_neg, ω_range_pos)
p_range_neg = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range_neg)
p_range_pos = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range_pos)

reduced_eigenvalues_weights = amplitude_factors ./ maximum(amplitude_factors)
sqrt.(abs.(reduced_eigenvalues)) # abs because the 0 eigenvalue can be ~ -eps
shifted_signed_modes = sign(P_solitary) .* static_frequency.(sqrt.(abs.(reduced_eigenvalues)), α, α_solitary, N, N_slacks)

# evaluation of solutions and stability
one_solitary_solutions = find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, P_solitary, α, α_solitary, N, N_slacks), ω_min, ω_max)
one_solitary_stabilities = map(ω -> self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), one_solitary_solutions)
stable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities)]
stable_one_solitary_solutions_physical = stable_one_solitary_solutions[abs.(stable_one_solitary_solutions) .> 10*eps(Float64)]
unstable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities .== 0)]
# expected behaviour.
# However, you need to cut the unphysical quasi-zero "solution" (10^-30) due to numerical inaccuracy

#make the plot
Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega_s$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P_{%$one_solitary_idx} - \alpha \omega_s")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range_neg, p_range_neg)
Makie.lines!(ax, ω_range_pos, p_range_pos, label=L"$p_s$", color=Makie.wong_colors(1)[2])
for l in 1:length(reduced_eigenvalues)
    Makie.vlines!(shifted_signed_modes[l], color=(:black, reduced_eigenvalues_weights[l]), linewidth=1, linestyle=:dot)
end
Makie.vlines!([],color=:black, linewidth=1, linestyle=:dot, label=L"$$eigenmodes")

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
leg = axislegend(position = :rt)

unstablescatter = Makie.scatter!(unstable_one_solitary_solutions, P_solitary .- α_solitary .* unstable_one_solitary_solutions, marker='u', markersize=16, color=(:red, 0.7), label=L"$$unstable solution")

mean_ωs_hist = normalize(fit(StatsBase.Histogram, one_solitaries_mean_ωs_by_node_i_idx[one_solitary_idx], edges), mode=:probability)
# here: normalize by largest weight to increase visibility
color=map(x -> (node_colors[one_solitary_idx], 1*x), mean_ωs_hist.weights./maximum(mean_ωs_hist.weights))
numerics_scatter = Makie.scatter!(centers, P_solitary .- α_solitary .* centers, color=color, markersize=size1*4/3, marker = marker1)
numerics_scatter_for_leg2 = Makie.scatter!([NaN], [NaN], color=node_colors[one_solitary_idx], markersize=size1*4/3, marker = marker1)

stablescatter = Makie.scatter!(stable_one_solitary_solutions_physical, P_solitary .- α_solitary .* stable_one_solitary_solutions_physical, marker='s', markersize=16, color=:green, label=L"$$stable solution")

leg2 = Legend(Fig[1,1], [stablescatter, unstablescatter, numerics_scatter_for_leg2], [L"$$stable solution",L"$$unstable solution",L"$$simulation"],
    halign=0.03, valign=0.10, tellwidth=false, tellheight=false)
Fig
FileIO.save(joinpath(plot_path, "grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*"_self_consistency_intersection_modes_numerics_hist_only_solitaries"*name*".pdf"), Fig)

Makie.ylims!(-4,0)
Fig







# node 84
one_solitary_idx = where_dense_sprouts[grid_idx][3]

solitary_node = pg.nodes[one_solitary_idx]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])
amplitude_factors = all_amplitude_factors[1]
node_parameters_dict = generation_parameters["node_parameters"]
α = node_parameters_dict[:D]
m = 2* node_parameters_dict[:H]
α_solitary = solitary_node.D
m_solitary = 2 * solitary_node.H
P_solitary = solitary_node.P
ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

step = 0.001
# omit zero due to divergence
ω_range_neg = ω_min:step:-step
ω_range_pos = step:step:ω_max
ω_range = vcat(ω_range_neg, ω_range_pos)
p_range_neg = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range_neg)
p_range_pos = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range_pos)

reduced_eigenvalues_weights = amplitude_factors ./ maximum(amplitude_factors)
sqrt.(abs.(reduced_eigenvalues)) # abs because the 0 eigenvalue can be ~ -eps
shifted_signed_modes = sign(P_solitary) .* static_frequency.(sqrt.(abs.(reduced_eigenvalues)), α, α_solitary, N, N_slacks)

# evaluation of solutions and stability
one_solitary_solutions = find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, P_solitary, α, α_solitary, N, N_slacks), ω_min, ω_max)
one_solitary_stabilities = map(ω -> self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), one_solitary_solutions)
stable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities)]
stable_one_solitary_solutions_physical = stable_one_solitary_solutions[abs.(stable_one_solitary_solutions) .> 10*eps(Float64)]
unstable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities .== 0)]
# expected behaviour.
# However, you need to cut the unphysical quasi-zero "solution" (10^-30) due to numerical inaccuracy

#make the plot
Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega_s$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P_{%$one_solitary_idx} - \alpha \omega_s")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range_neg, p_range_neg)
Makie.lines!(ax, ω_range_pos, p_range_pos, label=L"$p_s$", color=Makie.wong_colors(1)[2])
for l in 1:length(reduced_eigenvalues)
    Makie.vlines!(shifted_signed_modes[l], color=(:black, reduced_eigenvalues_weights[l]), linewidth=1, linestyle=:dot)
end
Makie.vlines!([],color=:black, linewidth=1, linestyle=:dot, label=L"$$eigenmodes")

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
leg = axislegend(position = :rt)

unstablescatter = Makie.scatter!(unstable_one_solitary_solutions, P_solitary .- α_solitary .* unstable_one_solitary_solutions, marker='u', markersize=16, color=(:red, 0.7), label=L"$$unstable solution")

mean_ωs_hist = normalize(fit(StatsBase.Histogram, one_solitaries_mean_ωs_by_node_i_idx[one_solitary_idx], edges), mode=:probability)
# here: normalize by largest weight to increase visibility
color=map(x -> (node_colors[one_solitary_idx], 1*x), mean_ωs_hist.weights./maximum(mean_ωs_hist.weights))
numerics_scatter = Makie.scatter!(centers, P_solitary .- α_solitary .* centers, color=color, markersize=size1*4/3, marker = marker1)
numerics_scatter_for_leg2 = Makie.scatter!([NaN], [NaN], color=node_colors[one_solitary_idx], markersize=size1*4/3, marker = marker1)

stablescatter = Makie.scatter!(stable_one_solitary_solutions_physical, P_solitary .- α_solitary .* stable_one_solitary_solutions_physical, marker='s', markersize=16, color=:green, label=L"$$stable solution")

leg2 = Legend(Fig[1,1], [stablescatter, unstablescatter, numerics_scatter_for_leg2], [L"$$stable solution",L"$$unstable solution",L"$$simulation"],
    halign=0.03, valign=0.10, tellwidth=false, tellheight=false)
Fig
FileIO.save(joinpath(plot_path, "grid_"*string(grid_idx)*"_node_"*string(one_solitary_idx)*"_self_consistency_intersection_modes_numerics_hist_only_solitaries"*name*".pdf"), Fig)








# old stuff
# make plots of all asymptotic frequencies in the network
simulation_parameters = FileIO.load(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*"_200t_20s_only_dw.jld"))
node_idx_range = simulation_parameters["node_idx_range"]
dϕ_idx_range = 1:length(simulation_parameters["perturbation_range_ϕ"])
dω_idx_range = 1:length(simulation_parameters["perturbation_range_ω"])
node_colors =  topology_data["node_colors"][grid_idx]
node_markers =  topology_data["node_markers"][grid_idx]

classes = topology_data["classes"]
print(classes[grid_idx])
findall(col -> col == colorant"red", node_colors)
classes[grid_idx][findall(col -> col == colorant"red", node_colors)]

fontsize = 12
size_inches = (2* (3+3/8), 2/3 * 2 * (3+3/8))
size_pt = 72 .* size_inches
marker_size = 4

mean_ωs_scatter_i = Makie.Figure(resolution=size_pt, fontsize=12);
ax = Makie.Axis(mean_ωs_scatter_i[1, 1], xlabel = L"node location $i$", ylabel = "⟨ωᵢ⟩",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
for (dϕ_idx, dω_idx, node_k_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
    Makie.scatter!(node_idx_range, mean_ωs[node_k_idx, dϕ_idx, dω_idx, :], color=node_colors, markersize=marker_size, marker = node_markers)
end
mean_ωs_scatter_i

# save("mean_omegas_scatter_pert_index_i.png", mean_ωs_scatter_i, px_per_unit=4)
