"""
plotting of trajectories and statistics
"""
#
using FileIO
using JLD
using Distributed
using LaTeXStrings
using Random, Distributions
# using OrdinaryDiffEq
# using PowerDynamics

include(joinpath(@__DIR__,"../src/network_generation.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))
include(joinpath(@__DIR__,"../src/simulation_functions.jl"))


ensemble_path = joinpath(@__DIR__,"../ensembles/")
ensemble_path_local = joinpath(ensemble_path,"random_pgs/")
data_path = joinpath(@__DIR__,"../data/")
data_path_local = joinpath(data_path, "../random_pgs")
plot_path = joinpath(@__DIR__,"../plots/")

grid_idx = 1

# only dω
# 200 time units simulation time

t_sim = (0.,200.) #nomenclature 200t (transient), 20s (statistics)
t_stat = 20. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/997 : t_sim[2]
abstol = 1e-3
reltol = 1e-3


topology_data = JLD.load(joinpath(ensemble_path_local,"topology_data.jld"))
generation_parameters = JLD.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]
where_dense_sprouts = topology_data["where_dense_sprouts"]
where_dense_sprouts[1]

n_initial_ϕ = 1
n_initial_ω = 10
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

node_idx_range = 1:N

pg, RHS, op = find_power_grid_path(ensemble_path_local, grid_idx)

# dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic
mean_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))
std_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))

plot_idxs = collect(1:N)

#@distributed
node_idx = where_dense_sprouts[grid_idx][1]
dϕ_idx = 1
dω_idx = 2
dϕ = perturbation_range_ϕ[dϕ_idx]
dω = perturbation_range_ω[dω_idx] + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
pg_solution = PowerGridSolution(solution, pg)
myplot_ϕ = Plots.plot(pg_solution, plot_idxs, :φ, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false, xlabel=L"t", ylabel=L"\varphi")
myplot_ω = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false, xlabel=L"t", ylabel=L"\omega")

Plots.plot(pg_solution, plot_idxs, :φ, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)


FileIO.save(plot_path*"grid_1_node_74_dw_-8_dphi_0_trajectory_phi.png", myplot_ϕ)
FileIO.save(plot_path*"grid_1_node_74_dw_-8_dphi_0_trajectory_w.png", myplot_ω)

# shows that 200 time units is sufficient

node_idx = where_dense_sprouts[grid_idx][1]
dϕ_idx = 1
dω_idx = 9
dϕ = perturbation_range_ϕ[dϕ_idx]
dω = perturbation_range_ω[dω_idx] + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
pg_solution = PowerGridSolution(solution, pg)
myplot_ω = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false);
Plots.xlabel!(L"t"); #L"$t$ [0.25 s]"
Plots.ylabel!(L"\omega")

myplot_ϕ = Plots.plot(pg_solution, plot_idxs, :φ, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false, xlabel=L"t", ylabel=L"\varphi")

FileIO.save(plot_path*"grid_1_node_74_dw_-1_dphi_0_trajectory_phi.png", myplot_ϕ)
FileIO.save(plot_path*"grid_1_node_74_dw_-1_dphi_0_trajectory_w.png", myplot_ω)

# also for scenario of fallback to steady state



# try some more initial conditions and the new parameters
grid_idx = 1
pg, RHS, op = find_power_grid_path(ensemble_path_local, grid_idx)

t_sim = (0.,200.) #nomenclature 200t (transient), 20s (statistics)
t_stat = 50. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/10000 : t_sim[2]
abstol = 1e-3
reltol = 1e-3

topology_data = JLD.load(joinpath(ensemble_path_local,"topology_data.jld"))
generation_parameters = JLD.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

n_initial_ϕ = 1
n_initial_ω = 1
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -5.
ω_max_incl = 6.
ϕ_dist = Uniform(ϕ_min, ϕ_max_excl)
ω_dist = Uniform(ω_min, ω_max_incl)

node_idx_range = 1:N
plot_idxs = collect(1:N)

for node_idx in node_idx_range
    dϕ = Random.rand(ϕ_dist)
    dω = Random.rand(ω_dist) + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
    solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
    pg_solution = PowerGridSolution(solution, pg)
    myplot = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω, sigdigits=3)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)
    display(myplot)
end
# most are fine, some erratic trajectories, maybe from switching between attractors with similar solitary freqs?
# one in a hundred is a bit unlucky...
