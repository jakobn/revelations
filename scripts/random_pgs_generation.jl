"""
Generation of an ensemble of networks using ../src/network_generation.jl
Plots of ensemble properties
"""
#
using Random
using TreeNodeClassification
using FileIO
using JLD
using StatsBase
using Plots
using DataStructures
using Images
using FixedPointNumbers

include(joinpath(@__DIR__,"../src/network_generation.jl"))
include(joinpath(@__DIR__,"../src/plotting_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")
data_path = joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")

N = 100
node_type = SwingEqLVS # this is fixed in random_PD_grid
H = .5
P = 1.
D = .1
Ω = 1.
Γ = 1.
V = 1.
node_parameters_dict = Dict(:H=>H, :P=>P, :D=>D, :Ω=>Ω, :Γ=>Γ, :V=>V)
K = 6.

### generate n_grids = M (50 in Nitzbon paper) power grids and store them
n_grids = 100

generation_parameters = Dict(
    :"n_grids"              => n_grids,
    :"N"                    => N,
    :"node_type"            => node_type,
    :"node_parameters"      => node_parameters_dict,
    :"K"                    => K
    )
# maybe add a unique name for an ensemble later 
FileIO.save(joinpath(ensemble_path, "random_pgs", "generation_parameters.jld"), "generation_parameters", generation_parameters)

classes = Array{Any}(undef, (n_grids, N))
n_dense_sprouts = Vector{Int}(undef, n_grids)
where_dense_sprouts = Vector{Vector{Int}}(undef, n_grids)
where_dense_roots = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees_unique = Vector{Vector{Int}}(undef, n_grids)
mean_degrees = Vector{Float64}(undef,n_grids)
node_colors = Vector{Any}(undef, n_grids) #fill(fill(colorant"red", N), n_grids)
node_markers = Vector{Any}(undef, n_grids) 
@time for grid_idx in 1:n_grids
    # this makes the grid generation reproducable (tested it!)
    Random.seed!(grid_idx)
    pg, op = random_PD_grid(N, node_parameters_dict, K; )
    write_powergrid(pg, joinpath( ensemble_path, "random_pgs", "grid_"*string(grid_idx)*".json"), Json)
    # pg_graph = graph_convert(pg.graph)
    pg_graph = pg.graph
    classes[grid_idx] = full_node_classification(pg_graph, N, 5) # second argument: maxiter, third argument: treshold (above a sprout is dense)
    where_dense_sprouts[grid_idx] = findall(i->i=="Dense Sprout",classes[grid_idx])
    length_dense_sprouts = length(where_dense_sprouts[grid_idx])
    n_dense_sprouts[grid_idx] = length_dense_sprouts
    where_dense_roots[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
    dense_root_degrees[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
    degrees = Graphs.degree(pg_graph)
    mean_degrees[grid_idx] = mean(degrees)
    node_colors[grid_idx] = color_nodes(pg)
    node_markers[grid_idx] = power_shape_nodes(pg)

    # dense root degrees, counting for each dense sprout
    position = 1
    for node_idx in where_dense_sprouts[grid_idx]
        root = Graphs.neighbors(pg_graph, node_idx)[1]
        where_dense_roots[grid_idx][position] = root
        #root_neighbors = Graphs.neighbors(pg_graph, root)
        dense_root_degrees[grid_idx][position] = Graphs.degree(pg_graph, root) # length(root_neighbors)
        position += 1
    end
    
    # counting each dense root only once
    where_dense_roots_tmp = where_dense_roots[grid_idx]
    where_dense_roots_unique_tmp = OrderedSet(where_dense_roots_tmp)
    dense_root_degrees_unique[grid_idx] = Vector{Int}(undef, length(where_dense_roots_unique_tmp))
    position = 1
    for node_idx in where_dense_roots_unique_tmp
        dense_root_degrees_unique[grid_idx][position] = degrees[node_idx]
        position += 1
    end

    # gplot = my_graph_plot(pg,[])
    # FileIO.save(plot_path*"grid_"*string(grid_idx)*".pdf", gplot)
end


FileIO.save(joinpath(ensemble_path, "random_pgs", "topology_data.jld"),
    "classes", classes,
    "n_dense_sprouts", n_dense_sprouts,
    "where_dense_sprouts", where_dense_sprouts,
    "where_dense_roots", where_dense_roots,
    "where_dense_roots_unique", OrderedSet.(where_dense_roots),
    "dense_root_degrees", dense_root_degrees,
    "dense_root_degrees_unique", dense_root_degrees_unique,
    "mean_degrees", mean_degrees,
    "node_colors", node_colors,
    "node_markers", node_markers)




### make histograms of the ensembles topology data

## histogram of dense root degrees
# now with corrected counting if several dense sprouts at one root

topology_data = FileIO.load(joinpath(ensemble_path, "random_pgs", "topology_data.jld"))
dense_root_degrees = topology_data["dense_root_degrees"]
# manually:
# bins = minimum(vcat(dense_root_degrees...))-0.5 : maximum(vcat(dense_root_degrees...))+0.5
# Plots.histogram(vcat(dense_root_degrees...), bins = bins)

# built-in (but uses manual bins from above): 
# hist_dense_root_degrees = StatsBase.fit(StatsBase.Histogram, vcat(dense_root_degrees...), bins)
# hist.edges
# hist.weights
# Plots.histogram(vcat(dense_root_degrees...), bins = hist_dense_root_degrees.edges)

fontsize=12

dense_root_degrees = topology_data["dense_root_degrees"]
bins = minimum(vcat(dense_root_degrees...))-0.5 : maximum(vcat(dense_root_degrees...))+0.5
plt_hist_root_degree = Plots.histogram(vcat(dense_root_degrees...),
    bins = bins, legend = false, size=(350,250),
    labelfontsize=fontsize, tickfontsize=fontsize);
Plots.xlabel!("root degree");
Plots.ylabel!("frequency")

## counting dense roots only once
dense_root_degrees_unique = topology_data["dense_root_degrees_unique"]
bins = minimum(vcat(dense_root_degrees_unique...))-0.5 : maximum(vcat(dense_root_degrees_unique...))+0.5
plt_hist_root_degree_unique = Plots.histogram(vcat(dense_root_degrees_unique...),
    bins = bins, legend = false, size=(350,250),
    labelfontsize=fontsize, tickfontsize=fontsize);
Plots.xlabel!("root degree (unique)");
Plots.ylabel!("frequency")


## histogram of number of dense sprouts
n_dense_sprouts = topology_data["n_dense_sprouts"]
sum(n_dense_sprouts)
bins = minimum(n_dense_sprouts)-0.5:maximum(n_dense_sprouts)+0.5 # careful with doubled name bins
hist_dense_sprouts = StatsBase.fit(StatsBase.Histogram, n_dense_sprouts, bins)
# Plots.histogram(hist_dense_sprouts, bins = hist_dense_sprouts.edges, ) #what is this?!
plt_hist_dense_sprouts = Plots.bar(hist_dense_sprouts.edges, hist_dense_sprouts.weights,
    x_ticks = 0:10, legend=false, size=(350,250),
    labelfontsize=fontsize, tickfontsize=fontsize);
Plots.xlabel!("number of dense sprouts");
Plots.ylabel!("frequency")


# if you want both in one figure. You have to bugfix the margins though.
plt_both = Plots.plot(plt_hist_dense_sprouts, plt_hist_root_degree,
    layout=(1,2), size=(700,250))

save(joinpath(plot_path,"ensemble_hist_number_dense_sprouts.pdf"),plt_hist_dense_sprouts)
save(joinpath(plot_path,"ensemble_hist_roots_degree.pdf"), plt_hist_root_degree)
save(joinpath(plot_path,"ensemble_hist_roots_degree_unique.pdf"), plt_hist_root_degree_unique)