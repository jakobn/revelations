"""
Simulation of dynamics on synthetic networks generated with random_pgs_generation.jl
grid_1
trying out different simulation times, impact of dϕ

plotting asymptotic frequencies
"""
#
using FileIO
using JLD
using Distributed
# using OrdinaryDiffEq
# using PowerDynamics

include(joinpath(@__DIR__,"../src/network_generation.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))
include(joinpath(@__DIR__,"../src/simulation_functions.jl"))
include(joinpath(@__DIR__, "../src/plotting_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")
ensemble_path_local = joinpath(ensemble_path,"random_pgs/")
data_path = joinpath(@__DIR__,"../data/")
data_path_local = joinpath(data_path, "random_pgs/")
plot_path = joinpath(@__DIR__,"../plots/")

# only dω
# 200 time units simulation time sufficient (see plots of trajectories)
# try different t_stat

t_sim = (0.,200.) #nomenclature 200t (transient), 20s (statistics)
t_stat = 20. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/997 : t_sim[2]
abstol = 1e-3
reltol = 1e-3


topology_data = JLD.load(joinpath(ensemble_path_local,"topology_data.jld"))
generation_parameters = JLD.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

n_initial_ϕ = 1
n_initial_ω = 10
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

node_idx_range = 1:N

@time for grid_idx in 1:1 # 1:n_grids_later

    pg, RHS, op = find_power_grid_path(ensemble_path_local, grid_idx)

    # dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic
    mean_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))
    std_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))

    # plot_idxs = collect(1:N)

    #@distributed
    for (dϕ_idx,  dω_idx, node_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
        dϕ = perturbation_range_ϕ[dϕ_idx]
        dω = perturbation_range_ω[dω_idx] + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
        solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
        # pg_solution = PowerGridSolution(solution, pg)
        # myplot = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)
        # display(myplot)
        mean_ωs[node_idx, dϕ_idx, dω_idx, :], std_ωs[node_idx, dϕ_idx, dω_idx, :] = calculate_statistics_ω(solution, t_stat_iter)
        println(dω_idx, node_idx)
    end

    FileIO.save(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*"_200t_20s_only_dw.jld"), "mean_ωs", mean_ωs, "std_ωs", std_ωs)

    FileIO.save(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*"_200t_20s_only_dw.jld"),
        "perturbation_range_ϕ", perturbation_range_ϕ, "perturbation_range_ω", perturbation_range_ω,
        "t_sim", t_sim, "t_stat_iter", t_stat_iter, "node_idx_range", node_idx_range,
        "abstol", abstol, "reltol", reltol)

end

# 5000 to 6000 s for one grid, all nodes, 1 ϕ, 10 ω
# is only 270 s with Tsit5()
# first data generated with Rodas4(), though




# now with 10 dϕ and 120 dω (and Tsit5)
t_sim = (0.,200.) #nomenclature 200t (transient), 20s (statistics)
t_stat = 50. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/10000 : t_sim[2]
abstol = 1e-3
reltol = 1e-3


topology_data = JLD.load(joinpath(ensemble_path_local,"topology_data.jld"))
generation_parameters = JLD.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

n_initial_ϕ = 10
n_initial_ω = 120
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -5.
ω_max_incl = 6.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

node_idx_range = 1:N

@time for grid_idx in 1:1 # 1:n_grids_later

    pg, RHS, op = find_power_grid_path(ensemble_path_local, grid_idx)

    # dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic
    mean_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))
    std_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))

    # plot_idxs = collect(1:N)

    #@distributed
    for (dϕ_idx,  dω_idx, node_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
        dϕ = perturbation_range_ϕ[dϕ_idx]
        dω = perturbation_range_ω[dω_idx] + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
        solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
        # pg_solution = PowerGridSolution(solution, pg)
        # myplot = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)
        # display(myplot)
        mean_ωs[node_idx, dϕ_idx, dω_idx, :], std_ωs[node_idx, dϕ_idx, dω_idx, :] = calculate_statistics_ω(solution, t_stat_iter)
        println(node_idx)
    end

    FileIO.save(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*"_200t_50s_10dphi_120dw.jld"), "mean_ωs", mean_ωs, "std_ωs", std_ωs)

    FileIO.save(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*"_200t_50s_10dphi_120dw.jld"),
        "perturbation_range_ϕ", perturbation_range_ϕ, "perturbation_range_ω", perturbation_range_ω,
        "t_sim", t_sim, "t_stat_iter", t_stat_iter, "node_idx_range", node_idx_range,
        "abstol", abstol, "reltol", reltol)

end

# 27234 s with Tsit5()





# simulate only node 74 with plenty of initial conditions to make a phase portrait

t_sim = (0.,200.) #nomenclature 200t (transient), 20s (statistics)
t_stat = 50. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/10000 : t_sim[2]
abstol = 1e-3
reltol = 1e-3


topology_data = JLD.load(joinpath(ensemble_path_local,"topology_data.jld"))
generation_parameters = JLD.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

n_initial_ϕ = 300
n_initial_ω = 300
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -5.
ω_max_incl = 6.

# careful, rounding error
perturbation_range_ϕ = ϕ_min : (ϕ_max_excl - ϕ_min) /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

node_idx_range = 74

@time for grid_idx in 1:1 # 1:n_grids_later

    pg, RHS, op = find_power_grid_path(ensemble_path_local, grid_idx)

    # dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic
    mean_ωs = Array{Float64}(undef, (length(node_idx_range), n_initial_ϕ, n_initial_ω, N))
    std_ωs = Array{Float64}(undef, (length(node_idx_range), n_initial_ϕ, n_initial_ω, N))

    # plot_idxs = collect(1:N)

    # @distributed
    for (dϕ_idx, dω_idx, node_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
        # new generation of perturbation values to avoid rounding errors
        dϕ = ϕ_min + (ϕ_max_excl - ϕ_min) * (dϕ_idx - 1) /(n_initial_ϕ)
        dω = ω_min + (ω_max_incl - ω_min) * (dω_idx - 1) /(n_initial_ω -1) + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
        solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
        # pg_solution = PowerGridSolution(solution, pg)
        # myplot = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)
        # display(myplot)

        # first index had to be changed for memory, not generalized to several perturbed nodes yet!
        mean_ωs[1, dϕ_idx, dω_idx, :], std_ωs[1, dϕ_idx, dω_idx, :] = calculate_statistics_ω(solution, t_stat_iter)
        println("dϕ_idx = $dϕ_idx, dω_idx = $dω_idx")
    end

    FileIO.save(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*"_node74_200t_50s_400dphi_400dw.jld"), "mean_ωs", mean_ωs, "std_ωs", std_ωs)

    FileIO.save(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*"_node74_200t_50s_10dphi_120dw.jld"),
        "perturbation_range_ϕ", perturbation_range_ϕ, "perturbation_range_ω", perturbation_range_ω,
        "t_sim", t_sim, "t_stat_iter", t_stat_iter, "node_idx_range", node_idx_range,
        "abstol", abstol, "reltol", reltol)

end

# 37794 s

# Make phase portrait
fontsize = 12
size_inches = (2* (3+3/8), 2/3 * 2 * (3+3/8))
size_pt = 72 .* size_inches
marker_size = 4

grid_idx = 1
# for ranges without rounding errors
perturbation_range_ϕ  = ϕ_min .+ (0:(n_initial_ϕ - 1)) .* (ϕ_max_excl - ϕ_min) / n_initial_ϕ 
perturbation_range_ω = ω_min .+ (0:(n_initial_ω - 1)) .* (ω_max_incl - ω_min) /(n_initial_ω -1)
tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*"_node74_200t_50s_400dphi_400dw.jld"))
mean_ωs = tail_statistics["mean_ωs"]
node_idx = 74
pg, RHS, op = find_power_grid_path(ensemble_path_local, grid_idx)
xs = perturbation_range_ϕ
ys = perturbation_range_ω .+ 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D
zs = mean_ωs[1,:,:,node_idx]

phase_portrait_grid_1_node_74 = Makie.Figure(resolution=size_pt, fontsize=12);
ax = Makie.Axis(phase_portrait_grid_1_node_74[1, 1], xlabel = L"$ \Delta\varphi$", ylabel = L"$ \Delta \omega$")
hm = Makie.heatmap!(xs, ys, zs)
Colorbar(phase_portrait_grid_1_node_74[:, end+1], hm)
phase_portrait_grid_1_node_74

save(joinpath(plot_path, "phase_portrait_grid_1_node_74.pdf"), phase_portrait_grid_1_node_74)





# Make plots and compare
generation_parameters = JLD.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

fontsize = 12
size_inches = (2* (3+3/8), 2/3 * 2 * (3+3/8))
size_pt = 72 .* size_inches
marker_size = 4

grid_idx = 1

## less data points
name = "_200t_20s_only_dw"

simulation_parameters = FileIO.load(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*name*".jld"))
node_idx_range = simulation_parameters["node_idx_range"]
dϕ_idx_range = 1:length(simulation_parameters["perturbation_range_ϕ"])
dω_idx_range = 1:length(simulation_parameters["perturbation_range_ω"])

topology_data = FileIO.load(joinpath(ensemble_path_local, "topology_data.jld"))
node_colors =  topology_data["node_colors"][grid_idx]
node_markers =  topology_data["node_markers"][grid_idx]

tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*name*".jld"))
mean_ωs = tail_statistics["mean_ωs"]

mean_ωs_scatter_i = Makie.Figure(resolution=size_pt, fontsize=12);
ax = Makie.Axis(mean_ωs_scatter_i[1, 1], xlabel = L"node location $i$", ylabel = L"$\omega_s$",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
for (dϕ_idx, dω_idx, node_k_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
    Makie.scatter!(node_idx_range, mean_ωs[node_k_idx, dϕ_idx, dω_idx, :], color=node_colors, markersize=marker_size, marker = node_markers)
end
mean_ωs_scatter_i
save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i"*name*".pdf"), mean_ωs_scatter_i)



## more data points
name = "_200t_50s_10dphi_120dw"

simulation_parameters = FileIO.load(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*name*".jld"))
node_idx_range = simulation_parameters["node_idx_range"]
dϕ_idx_range = 1:length(simulation_parameters["perturbation_range_ϕ"])
dω_idx_range = 1:length(simulation_parameters["perturbation_range_ω"])

topology_data = FileIO.load(joinpath(ensemble_path_local, "topology_data.jld"))
node_colors =  topology_data["node_colors"][grid_idx]
# node_markers =  topology_data["node_markers"][grid_idx]
# go back to circle shapes for these plots
node_markers = fill(:circle, N)


tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*name*".jld"))
mean_ωs = tail_statistics["mean_ωs"]

mean_ωs_scatter_i = Makie.Figure(resolution=size_pt, fontsize=12);
ax = Makie.Axis(mean_ωs_scatter_i[1, 1], xlabel = L"node location $i$", ylabel = L"$\omega_s$", #ylabel = "⟨ωᵢ⟩" does not work
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
for (dϕ_idx, dω_idx, node_k_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
    Makie.scatter!(node_idx_range, mean_ωs[node_k_idx, dϕ_idx, dω_idx, :], color=node_colors, markersize=marker_size, marker = node_markers)
end
mean_ωs_scatter_i
# takes forever save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i"*name*".pdf"), mean_ωs_scatter_i)
save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i"*name*".png"), mean_ωs_scatter_i, px_per_unit=4)


# separate plots for all the different dϕ
for dϕ_idx in dϕ_idx_range
    mean_ωs_scatter_i = Makie.Figure(resolution=size_pt, fontsize=12);
    ax = Makie.Axis(mean_ωs_scatter_i[1, 1], xlabel = L"node location $i$", ylabel = L"$\omega_s$",
        xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
    for (dω_idx, node_k_idx) in collect(Iterators.product(dω_idx_range, node_idx_range))
        Makie.scatter!(node_idx_range, mean_ωs[node_k_idx, dϕ_idx, dω_idx, :], color=node_colors, markersize=marker_size, marker = node_markers)
    end
    display(mean_ωs_scatter_i)
    save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i"*"_200t_50s_dphi"*string(36*(dϕ_idx-1))*"deg_120dw"*".png"), mean_ωs_scatter_i)
end


# plot only the asymptotic frequencies from the perturbation at the same node
simulation_parameters = FileIO.load(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*name*".jld"))
perturbation_range_ω = simulation_parameters["perturbation_range_ω"]
length(perturbation_range_ω)
n_initial_ω = 120

mean_ωs_scatter_i = Makie.Figure(resolution=size_pt, fontsize=12);

ax = Makie.Axis(mean_ωs_scatter_i[1, 1], xlabel = L"node location $i$", ylabel = L"$\omega_s$",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
for (dϕ_idx, node_k_idx) in collect(Iterators.product(dϕ_idx_range, node_idx_range))
    Makie.scatter!(node_k_idx * ones(n_initial_ω), mean_ωs[node_k_idx, dϕ_idx, :, node_k_idx], color=node_colors[node_k_idx], markersize=marker_size, marker = node_markers[node_k_idx])
end
mean_ωs_scatter_i
save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i"*name*"_only_same_node.pdf"), mean_ωs_scatter_i)


# scatter over node perturbed l index

mean_ωs_scatter_l = Makie.Figure(resolution=size_pt, fontsize=12);
ax = Makie.Axis(mean_ωs_scatter_l[1, 1], xlabel = L"node perturbed $l$", ylabel = L"$\omega_s$",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
for (dϕ_idx, dω_idx, node_i_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
    Makie.scatter!(node_idx_range, mean_ωs[:, dϕ_idx, dω_idx, node_i_idx], color=node_colors[node_i_idx], markersize=marker_size, marker = node_markers)
end
mean_ωs_scatter_l

save(joinpath(plot_path, "mean_omegas_scatter_pert_index_l"*name*".png"), mean_ωs_scatter_l, px_per_unit=4)


# histogram of degrees of solitary states
threshold = 1. # count as nonzero mean frequency
desynchronized_ωs_bool = abs.(mean_ωs) .> threshold
how_many_desynchronized_ωs = sum(desynchronized_ωs_bool, dims=4)
how_many_desynchronized_ωs_nonzero = how_many_desynchronized_ωs[findall(x -> (x > 0), how_many_desynchronized_ωs)]

h = fit(StatsBase.Histogram, how_many_desynchronized_ωs_nonzero, nbins=N)
hn = normalize(h, mode=:pdf)
# or mode = :density (does nothing here but set isdensity to one)
hn.closed
hn.isdensity
edges = hn.edges[1]
weights = hn.weights
h.weights
# centers = edges[1:end-1] .+ (edges[2] - edges[1])/2
positions = edges[1:end-1]

# which fraction of asymptotic states are desync? ~21.9%
length(how_many_desynchronized_ωs_nonzero) / length(how_many_desynchronized_ωs)
# which fraction of desync states are 1-solitaries?
weights[findall(x -> (x==1.), edges)]


min = minimum(log10.(weights[findall(x -> (x > 0), weights)]))
max = maximum(log10.(weights[findall(x -> (x > 0), weights)]))
fillto = min - 2

solitary_degree_hist = Makie.Figure(resolution=size_pt, fontsize=12);
ax = Makie.Axis(solitary_degree_hist[1,1], yscale = identity, xscale = identity,
    xlabel = "solitary degree", ylabel = L"$\log_{10}$(fraction)")
Makie.barplot!(positions, log10.(weights), fillto=fillto)
Makie.ylims!(min - 0.5, 0)
display(solitary_degree_hist)
save(joinpath(plot_path, "grid_one"*name*"solitary_degree_hist.pdf"), solitary_degree_hist)

# filter 1-solitary states
where_desynchronized = findall(i -> (i>0), desynchronized_ωs_bool)
where_one_solitaries = findall(i -> isequal(1, i), how_many_desynchronized_ωs)

where_one_solitaries_with_node_i_idx = vcat(conditional_cartesian_idx_vector(where_desynchronized, where_one_solitaries, 1:3)... )
one_solitaries_mean_ωs = vcat(conditional_nodes_indexer(where_desynchronized, mean_ωs, where_one_solitaries, 1:3)...)
FileIO.save(joinpath(data_path_local, "solitary_frequency_data_grid_"*string(grid_idx)*name*".jld"),
    "idxs", where_one_solitaries_with_node_i_idx, "ωs", one_solitaries_mean_ωs)

# test
mean_ωs[where_one_solitaries_with_node_i_idx] == one_solitaries_mean_ωs

one_solitaries_node_i_idxs = index_vectors(where_one_solitaries_with_node_i_idx)[4]


mean_ωs_scatter_i = Makie.Figure(resolution=size_pt, fontsize=12);

ax = Makie.Axis(mean_ωs_scatter_i[1, 1], xlabel = L"node location $i$", ylabel = L"$\omega_s$",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
Makie.scatter!(one_solitaries_node_i_idxs, one_solitaries_mean_ωs, color=broadcast(x -> node_colors[x], one_solitaries_node_i_idxs), markersize=marker_size, marker = :circle)
margin = 5
Makie.xlims!(1 - margin,N + margin)
mean_ωs_scatter_i
save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i"*name*"_only_one_solitaries.pdf"), mean_ωs_scatter_i)
save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i"*name*"_only_one_solitaries.png"), mean_ωs_scatter_i, px_per_unit=4)


# scatter all data as density plot to highlight importance
min_ω = minimum(mean_ωs)
max_ω = maximum(mean_ωs)
n_bins = 200
edges = [min_ω : (max_ω - min_ω)/n_bins : max_ω ...]
centers = edges[1:end-1] .+ (max_ω - min_ω)/n_bins


mean_ωs_hist = normalize(fit(StatsBase.Histogram, [mean_ωs[:, :, :, 1]...], edges), mode=:probability)
mean_ωs_hist.weights
print(mean_ωs_hist.weights)
sum(mean_ωs_hist.weights)

node_i_idx = 1
color=map(x -> (node_colors[node_i_idx], x), mean_ωs_hist.weights)
print(color)
maximum(mean_ωs_hist.weights)

"""
mean_ωs_scatter_i = Makie.Figure(resolution=size_pt, fontsize=12);
ax = Makie.Axis(mean_ωs_scatter_i[1, 1], xlabel = L"node location $i$", ylabel = L"$\omega_s$",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
Makie.scatter!(node_i_idx .* ones(n_bins), centers, color=color, markersize=marker_size, marker = node_markers[node_i_idx])
display(mean_ωs_scatter_i)
sort(mean_ωs_hist.weights)
print(sort(mean_ωs_hist.weights))
"""

mean_ωs_scatter_i = Makie.Figure(resolution=size_pt, fontsize=12);
ax = Makie.Axis(mean_ωs_scatter_i[1, 1], xlabel = L"node location $i$", ylabel = L"$\omega_s$",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
for node_i_idx in node_idx_range
    mean_ωs_hist = normalize(fit(StatsBase.Histogram, [mean_ωs[:, :, :, node_i_idx]...], edges), mode=:probability)
    color=map(x -> (node_colors[node_i_idx], 1000*x), mean_ωs_hist.weights)
    Makie.scatter!(node_i_idx .* ones(n_bins), centers, color=color, markersize=marker_size, marker = node_markers[node_i_idx])
end
mean_ωs_scatter_i
save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i_hist_alphax1000"*name*".png"), mean_ωs_scatter_i, px_per_unit=4)
save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i_hist_alphax1000"*name*".pdf"), mean_ωs_scatter_i)




# filter 1-solitaries and make Histogram - this is not a histogram, rather a density plot
solitary_frequency_data = FileIO.load(joinpath(data_path_local, "solitary_frequency_data_grid_"*string(grid_idx)*name*".jld"))
where_one_solitaries_with_node_i_idx = solitary_frequency_data["idxs"]
one_solitaries_mean_ωs = solitary_frequency_data["ωs"]

condition_cartesian_idx_vector = map(x -> CartesianIndex(0,0,0,x), 1:N)
# sort by node idx
one_solitaries_mean_ωs_by_node_i_idx = conditional_nodes_indexer(where_one_solitaries_with_node_i_idx, mean_ωs, condition_cartesian_idx_vector, 4)
length.(one_solitaries_mean_ωs_by_node_i_idx)
n_data_left_max = maximum(length.(one_solitaries_mean_ωs_by_node_i_idx))

minimum(vcat(one_solitaries_mean_ωs_by_node_i_idx...))
maximum(vcat(one_solitaries_mean_ωs_by_node_i_idx...))

min_ω = minimum(mean_ωs)
max_ω = maximum(mean_ωs)
n_bins = 200
edges = [min_ω : (max_ω - min_ω)/n_bins : max_ω ...]
centers = edges[1:end-1] .+ (max_ω - min_ω)/n_bins

# find largest weight (over all nodes)
all_mean_ωs_hists_weights = map(i -> normalize(fit(StatsBase.Histogram, one_solitaries_mean_ωs_by_node_i_idx[i], edges), mode=:probability).weights, node_idx_range)
maximum.(all_mean_ωs_hists_weights)
print(maximum.(all_mean_ωs_hists_weights))

all_mean_ωs_hists_weights_flattened = vcat(all_mean_ωs_hists_weights...)
# we should normalize to make comparable across nodes after filtering!
max_weight = maximum(all_mean_ωs_hists_weights_flattened[findall(x -> !(isnan(x)), all_mean_ωs_hists_weights_flattened)])

# normalize
all_mean_ωs_hists_weights_normalized = map(i -> all_mean_ωs_hists_weights[i] .* length(one_solitaries_mean_ωs_by_node_i_idx[i]) ./ n_data_left_max, node_idx_range)
all_mean_ωs_hists_weights_normalized_flattened = vcat(all_mean_ωs_hists_weights_normalized...)
max_weight_normalized = maximum(all_mean_ωs_hists_weights_normalized_flattened[findall(x -> !(isnan(x)), all_mean_ωs_hists_weights_normalized_flattened)])
# not needed here, do it in plot loop # all_mean_ωs_hists_weights_normalized_by_largest = all_mean_ωs_hists_weights_normalized ./ max_weight_normalized

mean_ωs_scatter_i = Makie.Figure(resolution=size_pt, fontsize=12);
ax = Makie.Axis(mean_ωs_scatter_i[1, 1], xlabel = L"node location $i$", ylabel = L"$\omega_s$",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
for node_i_idx in node_idx_range
    mean_ωs_hist = normalize(fit(StatsBase.Histogram, one_solitaries_mean_ωs_by_node_i_idx[node_i_idx], edges), mode=:probability)
    # normalize by number of data points after cutting interval around ω = 0, to make comparable
    # probably easier to not normalize as probability in the first place?
    n_data_left = length(one_solitaries_mean_ωs_by_node_i_idx[node_i_idx])
    if n_data_left > 0
        normalized_weights = mean_ωs_hist.weights .* n_data_left ./ n_data_left_max ./ max_weight_normalized
        color=map(x -> (node_colors[node_i_idx], 1*x), normalized_weights)
        Makie.scatter!(node_i_idx .* ones(n_bins), centers, color=color, markersize=marker_size, marker = node_markers[node_i_idx])
    end
end
margin = 5
Makie.xlims!(1 - margin,N + margin)
mean_ωs_scatter_i
save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i_hist_normalized_across_and_by_largest_"*name*"_only_one_solitaries.png"), mean_ωs_scatter_i, px_per_unit=4)
save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i_hist_normalized_across_and_by_largest_"*name*"_only_one_solitaries.pdf"), mean_ωs_scatter_i)


# normalize only by largest entry
# likely and unlikely solitary states might appear the same. Alpha more depends on the number of bins with nonzero entries

mean_ωs_scatter_i = Makie.Figure(resolution=size_pt, fontsize=12);
ax = Makie.Axis(mean_ωs_scatter_i[1, 1], xlabel = L"node location $i$", ylabel = L"$\omega_s$",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
for node_i_idx in node_idx_range
    mean_ωs_hist = normalize(fit(StatsBase.Histogram, one_solitaries_mean_ωs_by_node_i_idx[node_i_idx], edges), mode=:probability)
    # normalize by number of data points after cutting interval around ω = 0, to make comparable
    n_data_left = length(one_solitaries_mean_ωs_by_node_i_idx[node_i_idx])
    if n_data_left > 0
        normalized_weights = mean_ωs_hist.weights ./ max_weight # not max_weight_normalized, and not normalized by n_data_left/n_data_left_max
        color=map(x -> (node_colors[node_i_idx], 1*x), normalized_weights)
        Makie.scatter!(node_i_idx .* ones(n_bins), centers, color=color, markersize=marker_size, marker = node_markers[node_i_idx])
    end
end
margin = 5
Makie.xlims!(1 - margin,N + margin)
mean_ωs_scatter_i


save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i_hist_normalized_only_by_largest_"*name*"_only_one_solitaries.png"), mean_ωs_scatter_i, px_per_unit=4)
save(joinpath(plot_path, "mean_omegas_scatter_pert_index_i_hist_normalized_only_by_largest_"*name*"_only_one_solitaries.pdf"), mean_ωs_scatter_i)





# make Histogram of asymptotic frequencies instead of scatter plot over node index
