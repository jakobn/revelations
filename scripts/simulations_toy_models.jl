"""
simulation of toy models
"""
#
using FileIO
using JLD
using Distributed
# using OrdinaryDiffEq
# using PowerDynamics

include(joinpath(@__DIR__,"../src/network_generation.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))
include(joinpath(@__DIR__,"../src/simulation_functions.jl"))
include(joinpath(@__DIR__, "../src/plotting_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")
data_path = joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")

t_sim = (0.,200.) #nomenclature 200t (transient), 20s (statistics)
t_stat = 50. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/10000 : t_sim[2]
abstol = 1e-3
reltol = 1e-3

ensemble_name = "two_node_toy_models"
ensemble_path_local = joinpath(ensemble_path, ensemble_name*"/")
data_path_local = joinpath(data_path, ensemble_name*"/")

n_initial_ϕ = 10
n_initial_ω = 12
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -5.
ω_max_incl = 6.

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

grid_idx_range = 0:30

@time for grid_idx in grid_idx_range
    println(grid_idx)
    pg, RHS, op = find_power_grid_path(ensemble_path_local, ensemble_name[1:end-1]*"_n_"*string(grid_idx))
    n = grid_idx
    last_idx = 2 # not counting slack node here
    node_idx_range = 1:2

    statistics_idxs = 3:3:last_idx*3
    # dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic
    mean_ωs = Array{Float64}(undef, (length(node_idx_range), n_initial_ϕ, n_initial_ω, last_idx))
    std_ωs = Array{Float64}(undef, (length(node_idx_range), n_initial_ϕ, n_initial_ω, last_idx))

    for (dϕ_idx, dω_idx, node_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
        # new generation of perturbation values to avoid rounding errors
        dϕ = ϕ_min + (ϕ_max_excl - ϕ_min) * (dϕ_idx - 1) /(n_initial_ϕ)
        dω = ω_min + (ω_max_incl - ω_min) * (dω_idx - 1) /(n_initial_ω -1) + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
        pert_state =  single_node_perturbation_ϕ_ω(op, [dϕ, dω], node_idx)
        problem = ODEProblem(rhs(pg), pert_state.vec, t_sim)
        solution = solve(problem)
        # pg_solution = PowerGridSolution(solution, pg)
        # myplot = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)
        # display(myplot)

        mean_ωs[node_idx, dϕ_idx, dω_idx, :], std_ωs[node_idx, dϕ_idx, dω_idx, :] = calculate_statistics_ω_idxs(solution, t_stat_iter, statistics_idxs)
        # println("dϕ_idx = $dϕ_idx, dω_idx = $dω_idx")
    end

    FileIO.save(joinpath(data_path_local, "tail_statistics_grid_two_node_toy_model_n_"*string(grid_idx)*"_200t_50s_10dphi_10dw.jld"), "mean_ωs", mean_ωs, "std_ωs", std_ωs)

    # FileIO.save(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*"_node74_200t_50s_10dphi_120dw.jld"),
    #    "perturbation_range_ϕ", perturbation_range_ϕ, "perturbation_range_ω", perturbation_range_ω,
    #    "t_sim", t_sim, "t_stat_iter", t_stat_iter, "node_idx_range", node_idx_range,
    #    "abstol", abstol, "reltol", reltol)

end

# solver Tsit5() can not use mass matrices: must be the slack node
# apparently no explicit method can?
# solved with default method (just omit the algorithm argument)
# 494 s


# star_graph_toy_models
ensemble_name = "star_graph_toy_models"
ensemble_path_local = joinpath(ensemble_path, ensemble_name*"/")
data_path_local = joinpath(data_path, ensemble_name*"/")

n_initial_ϕ = 10
n_initial_ω = 12
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -5.
ω_max_incl = 6.

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

grid_idx_range = 0:30


@time for grid_idx in grid_idx_range
    println(grid_idx)
    pg, RHS, op = find_power_grid_path(ensemble_path_local, ensemble_name[1:end-1]*"_n_"*string(grid_idx))
    n = grid_idx
    N = n +2
    last_idx = N
    node_idx_range = 1: minimum([3,N]) # perturbing one of the n identical nodes is sufficient!

    statistics_idxs = 3:3:last_idx*3
    # dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic
    mean_ωs = Array{Float64}(undef, (length(node_idx_range), n_initial_ϕ, n_initial_ω, last_idx))
    std_ωs = Array{Float64}(undef, (length(node_idx_range), n_initial_ϕ, n_initial_ω, last_idx))

    for (dϕ_idx, dω_idx, node_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
        # new generation of perturbation values to avoid rounding errors
        dϕ = ϕ_min + (ϕ_max_excl - ϕ_min) * (dϕ_idx - 1) /(n_initial_ϕ)
        dω = ω_min + (ω_max_incl - ω_min) * (dω_idx - 1) /(n_initial_ω -1) + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
        solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
        # pg_solution = PowerGridSolution(solution, pg)
        # myplot = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)
        # display(myplot)

        mean_ωs[node_idx, dϕ_idx, dω_idx, :], std_ωs[node_idx, dϕ_idx, dω_idx, :] = calculate_statistics_ω_idxs(solution, t_stat_iter, statistics_idxs)
        # println("dϕ_idx = $dϕ_idx, dω_idx = $dω_idx")
    end

    FileIO.save(joinpath(data_path_local, "tail_statistics_grid_two_node_toy_model_n_"*string(grid_idx)*"_200t_50s_10dphi_10dw.jld"), "mean_ωs", mean_ωs, "std_ωs", std_ωs)

    # FileIO.save(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*"_node74_200t_50s_10dphi_120dw.jld"),
    #    "perturbation_range_ϕ", perturbation_range_ϕ, "perturbation_range_ω", perturbation_range_ω,
    #    "t_sim", t_sim, "t_stat_iter", t_stat_iter, "node_idx_range", node_idx_range,
    #    "abstol", abstol, "reltol", reltol)

end
# 490 s






# all-to-all_toy_models
ensemble_name = "all_to_all_toy_models"
ensemble_path_local = joinpath(ensemble_path, ensemble_name*"/")
data_path_local = joinpath(data_path, ensemble_name*"/")

n_initial_ϕ = 10
n_initial_ω = 12
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -5.
ω_max_incl = 6.

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

grid_idx_range = 0:30

@time for grid_idx in grid_idx_range
    println(grid_idx)
    pg, RHS, op = find_power_grid_path(ensemble_path_local, ensemble_name[1:end-1]*"_n_"*string(grid_idx))
    n = grid_idx
    N = n +2
    last_idx = N
    node_idx_range = 1: minimum([3,N]) # perturbing one of the n identical nodes is sufficient!

    statistics_idxs = 3:3:last_idx*3
    # dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic
    mean_ωs = Array{Float64}(undef, (length(node_idx_range), n_initial_ϕ, n_initial_ω, last_idx))
    std_ωs = Array{Float64}(undef, (length(node_idx_range), n_initial_ϕ, n_initial_ω, last_idx))

    for (dϕ_idx, dω_idx, node_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
        # new generation of perturbation values to avoid rounding errors
        dϕ = ϕ_min + (ϕ_max_excl - ϕ_min) * (dϕ_idx - 1) /(n_initial_ϕ)
        dω = ω_min + (ω_max_incl - ω_min) * (dω_idx - 1) /(n_initial_ω -1) + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
        solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
        # pg_solution = PowerGridSolution(solution, pg)
        # myplot = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)
        # display(myplot)

        mean_ωs[node_idx, dϕ_idx, dω_idx, :], std_ωs[node_idx, dϕ_idx, dω_idx, :] = calculate_statistics_ω_idxs(solution, t_stat_iter, statistics_idxs)
        # println("dϕ_idx = $dϕ_idx, dω_idx = $dω_idx")
    end

    FileIO.save(joinpath(data_path_local, "tail_statistics_grid_two_node_toy_model_n_"*string(grid_idx)*"_200t_50s_10dphi_10dw.jld"), "mean_ωs", mean_ωs, "std_ωs", std_ωs)

    # FileIO.save(joinpath(data_path_local, "simulation_parameters_grid_"*string(grid_idx)*"_node74_200t_50s_10dphi_120dw.jld"),
    #    "perturbation_range_ϕ", perturbation_range_ϕ, "perturbation_range_ω", perturbation_range_ω,
    #    "t_sim", t_sim, "t_stat_iter", t_stat_iter, "node_idx_range", node_idx_range,
    #    "abstol", abstol, "reltol", reltol)

end
# 12693 s, could have been much faster, as you only need to perturb one of the all-to-all nodes (same with star-graph)
# slower because more links in network