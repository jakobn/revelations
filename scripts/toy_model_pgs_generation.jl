using PowerDynamics
include(joinpath(@__DIR__,"./network_generation.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")

H = .5; P = 1.; D = .1; Ω = 1.; Γ = 1.; V = 1.;
node_parameters_dict = Dict(:H=>H, :P=>P, :D=>D, :Ω=>Ω, :Γ=>Γ, :V=>V)
K = 6.

for n in 0:30
    pg, op = two_node_toy_model(n, node_parameters_dict, K)
    path = joinpath(ensemble_path,"two_node_toy_models",  "two_node_toy_model_n_"*string(n)*".json")
    write_powergrid(pg, path, Json)
end

for n in 0:30
    pg, op = star_graph_toy_model(n, node_parameters_dict, K)
    path = joinpath(ensemble_path, "star_graph_toy_models", "star_graph_toy_model_n_"*string(n)*".json")
    write_powergrid(pg, path, Json)
end

for n in 0:30
    pg, op = all_to_all_toy_model(n, node_parameters_dict, K)
    path = joinpath(ensemble_path, "all_to_all_toy_models",  "all_to_all_toy_model_n_"*string(n)*".json")
    write_powergrid(pg, path, Json)
end