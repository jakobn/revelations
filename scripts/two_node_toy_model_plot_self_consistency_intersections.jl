using CairoMakie
using JLD
using OrderedCollections
using Roots

include(joinpath(@__DIR__, "../src/self_consistency.jl"))
include(joinpath(@__DIR__,"../src/laplacian.jl"))
include(joinpath(@__DIR__, "../src/network_generation.jl"))


ensemble_path = joinpath(@__DIR__,"../ensembles/")
ensemble_path_local = joinpath(ensemble_path,"two_node_toy_models/")
data_path = joinpath(@__DIR__,"../data/")
data_path_local = joinpath(data_path, "../two_node_toy_models_pgs")
plot_path = joinpath(@__DIR__,"../plots/")

# new size: half page width
size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
scaler = 1
size_pt = scaler .* 72 .* size_inches
fontsize=scaler*12*1.5 # *1.5 is new in v2, for large enough font in half page column

H = .5; P = 1.; D = .1; Ω = 1.; Γ = 1.; V = 1.;
node_parameters_dict = Dict(:H=>H, :P=>P, :D=>D, :Ω=>Ω, :Γ=>Γ, :V=>V)
K = 6.

n = 6
N = n+2

one_solitary_idx = 1

pg, RHS, op = find_power_grid_path(ensemble_path_local, "two_node_toy_model_n_"*string(n))
N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))

solitary_node = pg.nodes[one_solitary_idx]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])
amplitude_factors = all_amplitude_factors[1]
α = node_parameters_dict[:D]
m = 2* node_parameters_dict[:H]
α_solitary = solitary_node.D
m_solitary = 2 * solitary_node.H
P_solitary = solitary_node.P
ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

ω_range = ω_min:0.001:ω_max
p_range = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range)

reduced_eigenvalues_weights = amplitude_factors ./ maximum(amplitude_factors)
sqrt.(reduced_eigenvalues)
shifted_signed_modes = sign(P_solitary) .* static_frequency.(sqrt.(reduced_eigenvalues), α, α_solitary, N, N_slacks)

amplitude_factors
# evaluation of solutions and stability
one_solitary_solutions = find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, P_solitary, α, α_solitary, N, N_slacks), ω_min, ω_max)
one_solitary_stabilities = map(ω -> self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), one_solitary_solutions)
stable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities)]
unstable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities .== 0)]
# expected behaviour.

#make figure
Fig = Makie.Figure(resolution=size_pt, fontsize=fontsize);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega_s$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P_1 - \alpha \omega_s")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range, p_range, label=L"$p_s(n=6)$")
for l in 1:length(reduced_eigenvalues)
    Makie.vlines!(shifted_signed_modes[l], color=(:black, reduced_eigenvalues_weights[l]), linewidth=1, linestyle=:dot)
end
Makie.vlines!([],color=:black, linewidth=1, linestyle=:dot, label=L"$$eigenmodes")

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
leg = axislegend(position = :rt)
# Fig

# # does it look like the analytical solution? yes.
# function analytic_mean_p_two_node_toy_model(K, solitary_frequency, m, α, N) 
#     n = N -2
#     p = 0.5 * solitary_frequency * α * K^2 / ( (K * n - m * solitary_frequency^2)^2 + α^2 * solitary_frequency^2)
# end

# analytic_p_range = map(ω -> analytic_mean_p_two_node_toy_model(K, ω, m, α, N), ω_range)
# Makie.lines!(ax, ω_range, analytic_p_range, label=L"analytic $\langle p \rangle$")
# Fig

stablescatter = Makie.scatter!(stable_one_solitary_solutions, P_solitary .- α_solitary .* stable_one_solitary_solutions, marker='s', markersize=16, color=:green, label=L"$$stable solution")
unstablescatter = Makie.scatter!(unstable_one_solitary_solutions, P_solitary .- α_solitary .* unstable_one_solitary_solutions, marker='u', markersize=16, color=(:red, 0.7), label=L"$$unstable solution")
leg2 = Legend(Fig[1,1], [stablescatter, unstablescatter], [L"$$stable solution",L"$$unstable solution"],
    halign=0.1, valign=0.3, tellwidth=false, tellheight=false)
Fig
# save for paper
FileIO.save(joinpath(plot_path, "intersection_plot_n_6_two_node_toy_model_v2.pdf"), Fig)