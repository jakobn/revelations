
"""
drop_index(cartesian_index_vector)

returns the cartesian_index_vector but with the last index dropped for each element
"""
# not used so far

function drop_index(cartesian_index_vector)
    len_vector = length(cartesian_index_vector)
    len_cartesian_index = length(cartesian_index_vector[1])
    new_vector = Vector{CartesianIndex{(len_cartesian_index-1)}}(undef, len_vector)
    for vector_idx in eachindex(cartesian_index_vector)
        cartesian_list = Vector{Int64}(undef, len_cartesian_index-1)
        for cartesian_idx in eachindex(cartesian_list)
            cartesian_list[cartesian_idx] = cartesian_index_vector[vector_idx][cartesian_idx]
        end
        new_vector[vector_idx] = CartesianIndex(cartesian_list...)
    end
    new_vector
end

"""
conditional_nodes_indexer(cartesian_idx_vector, stats_array, condition_cartesian_idx_vector, condition_range)

returns a vector where each element is a vector of elements of stats_array that fulfill:
the Cartesian coordinates specified by condition_range are the same for cartesian_idx_vector and condition_cartesian_idx_vector
e.g. find values of duett or solitary frequencies
"""
function conditional_nodes_indexer(cartesian_idx_vector, stats_array, condition_cartesian_idx_vector, condition_range)# or ...condition_vector, condition_fct)
    len_condition_cartesian_idx_vector = length(condition_cartesian_idx_vector)
    out_stats = Vector{Vector{Float64}}(undef, len_condition_cartesian_idx_vector)
    for condition_vector_idx in eachindex(condition_cartesian_idx_vector)
        condition_idx_temp = CartesianIndex(Tuple(condition_cartesian_idx_vector[condition_vector_idx])[condition_range])
        #println(condition_idx_temp)
        out_stats_temp = Vector{Float64}(undef, 0)
        for vector_idx in eachindex(cartesian_idx_vector)
            idx_temp = CartesianIndex(Tuple(cartesian_idx_vector[vector_idx])[condition_range])
            #println(idx_temp)
            if idx_temp == condition_idx_temp
                append!(out_stats_temp, stats_array[cartesian_idx_vector[vector_idx]])
            end
        end
        out_stats[condition_vector_idx] = out_stats_temp
    end
    out_stats
end

"""
conditional_cartesian_idx_vector(cartesian_idx_vector, condition_cartesian_idx_vector, condition_range)

returns a vector where each element is a CartesianIndex that fulfills:
the Cartesian coordinates specified by condition_range are the same for cartesian_idx_vector and condition_cartesian_idx_vector
e.g. find positions of duett or solitary frequencies
"""
function conditional_cartesian_idx_vector(cartesian_idx_vector, condition_cartesian_idx_vector, condition_range)# or ...condition_vector, condition_fct)
    len_condition_cartesian_idx_vector = length(condition_cartesian_idx_vector)
    len_cartesian_idx = length(cartesian_idx_vector[1])
    # out_stats = Vector{Vector{Float64}}(undef, len_condition_cartesian_idx_vector)
    out_idx = Vector{Vector{CartesianIndex{len_cartesian_idx}}}(undef, len_condition_cartesian_idx_vector)
    for condition_vector_idx in eachindex(condition_cartesian_idx_vector)
        condition_idx_temp = CartesianIndex(Tuple(condition_cartesian_idx_vector[condition_vector_idx])[condition_range])
        #println(condition_idx_temp)
        # out_stats_temp = Vector{Float64}(undef, 0)
        out_idx_temp = Vector{CartesianIndex{len_cartesian_idx}}(undef, 0)
        for vector_idx in eachindex(cartesian_idx_vector)
            idx_temp = CartesianIndex(Tuple(cartesian_idx_vector[vector_idx])[condition_range])
            if idx_temp == condition_idx_temp
                append!(out_idx_temp, [cartesian_idx_vector[vector_idx]])
                #println(idx_temp)
            end
        end
        out_idx[condition_vector_idx] = out_idx_temp
        # out_stats[condition_vector_idx] = out_stats_temp
    end
    out_idx
end

"""
index_vectors(cartesian_index_vector)
returns the "columns" of the cartesian_index_vector
"""

function index_vectors(cartesian_index_vector)
    len_vector = length(cartesian_index_vector)
    len_cartesian_index = length(cartesian_index_vector[1])
    new_vectors = Vector{Vector{Int64}}(undef, len_cartesian_index)
    for cartesian_idx in 1:len_cartesian_index
        new_vector = Vector{Int64}(undef, len_vector)
        for vector_idx in eachindex(cartesian_index_vector)
            new_vector[vector_idx] = cartesian_index_vector[vector_idx][cartesian_idx]
        end
        new_vectors[cartesian_idx] = new_vector
    end
    new_vectors
end

"""
all_nodes_indexer(cartesian_idx_vector, stats_array)
returns all values of stats_array where all but the last coordinate coincides with all but the last coordinates in cartesian_idx_vector
"""

function all_nodes_indexer(cartesian_idx_vector, stats_array)
    len_vector = length(cartesian_idx_vector)
    len_cartesian_idx = length(cartesian_idx_vector[1])
    out_stats = Array{Float64}(undef, (len_vector, size(stats_array)[end] ) )
    for vector_idx in eachindex(cartesian_idx_vector)
        idx_temp = Vector{Int64}(undef, len_cartesian_idx-1)
        for cartesian_idx in eachindex(idx_temp)
            idx_temp[cartesian_idx] = cartesian_idx_vector[vector_idx][cartesian_idx]
        end
        out_stats[vector_idx,:] = stats_array[idx_temp...,:]
    end
    out_stats
end

"""
conditional_matrix_replacer(matrix, condition_matrix, replacement)

returns a copy of matrix. Elements that meet the condition via condition_matrix = 1 at the same lcoation, are being replaced by replacement
"""
function conditional_matrix_replacer(matrix, condition_matrix, replacement)
    new_matrix = Matrix{Union{Missing, Float64},}(missing, size(matrix)) 
    for (i,j) in collect(Iterators.product(1:size(matrix)[1], 1:size(matrix)[2]))
        if condition_matrix[i,j] == 1
            new_matrix[i,j] = replacement
        else
            new_matrix[i,j] = matrix[i,j]
        end
    end
    new_matrix
end