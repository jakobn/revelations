
function coupling_matrix(pg)
    N = length(pg.nodes)
    K_matrix = zeros(N, N)
    for line in pg.lines
        from_idx = line.from
        to_idx = line.to
        K = -imag(line.Y)
        K_matrix[from_idx,to_idx] = K
        K_matrix[to_idx,from_idx] = K
    end
    K_matrix
end

function check_Laplacian_properties(Laplacian)
    @assert transpose(Laplacian) == Laplacian "Laplacian is not symmetric!"
    @assert all(isapprox.(sum(Laplacian; dims=2), 0, atol=1e-14)) "sum over row is not zero!"
end

function steady_state_Laplacian(pg, op)
    for node in pg.nodes
        @assert any(typeof(node) .== [SwingEqLVS, SwingEq, SlackAlgebraic])  "Not all nodes are SwingEq, SwingEqLVS or SlackAlgebraic."
    end
    phases = op[:,:φ]
    N = length(phases)
    Laplacian = zeros(N, N)
    for line in pg.lines
        from_idx = line.from
        to_idx = line.to
        K = -imag(line.Y)
        L = -K * cos(phases[from_idx] - phases[to_idx])
        Laplacian[from_idx,to_idx] = L
        Laplacian[to_idx,from_idx] = L
    end
    # diagonal
    for i in 1:N
        entry = 0
        for j in 1:N
            entry -= Laplacian[i,j]
        end
        Laplacian[i,i] = entry
    end
    check_Laplacian_properties(Laplacian)

    # delete rows and columns of SlackAlgebraic nodes, but leave diagonal entries of neighbors
    slack_idxs = findall(x -> (typeof(x) == SlackAlgebraic), pg.nodes)
    dynamic_idxs = setdiff(1:N, slack_idxs)
    dynamic_Laplacian = Laplacian[dynamic_idxs, dynamic_idxs]
end


function reduce_Laplacian(Laplacian, solitary_idxs)
    Laplacian_corrected_diagonal = Laplacian
    for solitary_idx in solitary_idxs
        Laplacian_corrected_diagonal += diagm((Laplacian[solitary_idx,:]))
    end
    
    reduced_idxs = setdiff(1:size(Laplacian,1), solitary_idxs)
    reduced_Laplacian = Laplacian_corrected_diagonal[reduced_idxs, reduced_idxs]
end

function reduce_K_matrix(K_matrix, solitary_idxs)
    reduced_idxs = setdiff(1:size(K_matrix,1), solitary_idxs)
    reduced_K_matrix = K_matrix[reduced_idxs, reduced_idxs]
end


