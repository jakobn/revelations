using JLD
using FileIO
using PowerDynamics
using SyntheticNetworks
using StatsBase
using Graphs
using LinearAlgebra
using ForwardDiff
using DifferentialEquations # needed to define ODEProblem
# or # using OrdinaryDiffEq (no difference)
using Colors
using TreeNodeClassification # for full_node_classification() for color_nodes()
using Plots # for ColorSchemes.jl: palette() for color_nodes()
using FixedPointNumbers
using OrderedCollections
"""
    find_power_grid_path(ensemble_path, grid_name)
Load the power grid "grid_name" from the location ensemble_path
"""
function find_power_grid_path(ensemble_path, grid_name)
    if typeof(grid_name) != String # make integer arguments possible
        grid_name = "grid_" * string(grid_name)
    end
    # load the power grid and all related stuff
    # filename = joinpath(@__DIR__,"../data/Ensemble/pg_method_Voltage_pg_idx_" * string(pg_idx))
    filename = joinpath(ensemble_path, grid_name * ".json")
    pg = read_powergrid(filename, Json)
    return pg, rhs(pg), find_operationpoint(pg, sol_method=:nlsolve)
end

"""
check_eigenvalues_MM(pg::PowerGrid, s::State)
check that real parts of all eigenvalues are close to zero or negative
"""
function check_eigenvalues_MM(pg::PowerGrid, s::State)
    rpg = rhs(pg)
    M = rpg.mass_matrix
    if M == true*I
        M = 1
    else M = Array(M)
    end
    f!(dx, x) = rpg(dx, x, nothing, 0.)
    j(x) = (dx = similar(x); ForwardDiff.jacobian(f!, dx, x))
    λ = eigvals(j(s.vec) * pinv(M) * M) .|> real |> extrema
    stable = isapprox(last(λ), 0, atol=1e-8)
    return λ, stable
end

"""
simulate_from_state(pg, state, tspan; abstol=1e-6, reltol=1e-3)
simulates the trajectories of the nodal variables starting from state and returns the solution object
"""
function simulate_from_state(pg, state, tspan; abstol = 1e-6, reltol = 1e-3)
    # abstol and reltol are defaults for odes according to https://diffeq.sciml.ai/dev/basics/common_solver_opts/
    # as opposed to Anna Büttner's code I set force_dtmin = false (default) because I want it exact and compatability
    problem = ODEProblem(rhs(pg), state, tspan)
    solution = solve(problem, Rodas4(), abstol=abstol, reltol=reltol)
    solution
end

"""
random_PD_grid(N::Int, node_parameters_dict, K

Generates a random power grid of size N with N SwingEqLVS nodes (w/o slack node) using SyntheticNetworks and then turns it into a PowerDynamics.PowerGrid type.

so far: homogeneous node_parameters (have to be except solitary node) except P, and homogeneous edge weights K
"""

function random_PD_grid(N::Int, node_parameters_dict, K; n_attempts=100, growth_parameters=[1, 1/5, 3/10, 1/3, 1/10, 0.0], active_power_distribution="random_half", balancing_method="None", balancing_parameters=[Nothing] )
    
    # decide node type and unpack parameters
    node_type = SwingEqLVS
    # H, P, D, Ω, Γ, V = node_parameters
    # node_parameters_dict = Dict(:H=>H, :P=>P, :D=>D, :Ω=>Ω, :Γ=>Γ, :V=>V)
        
    

    if active_power_distribution == "random_half"
        @assert (N%2)==0 "With balancing method random_half, N has to be even to ensure power balance with half of the nodes having power +(-)P." 
        P_vec = ones(N)
        turn_idx = sample(1:N, N÷2, replace=false)
        for n in turn_idx
            P_vec[n] = -1.
        end
        P_vec = P .* P_vec
        @assert sum(P_vec)==0 "power imbalance!"
    else
        @assert (false) "Active power distribution "* active_power_distribution * "not yet implemented."
    end

    n0, p, q, r, s, u = growth_parameters
    counter=1
    # n_attempts to generate a stable grid
    while counter <= n_attempts
        RPG = RandomPowerGrid(N, n0, p, q, r, s, u)  # last parameter u is currently not implemented
        pg_graph = generate_graph(RPG)      
        e = Graphs.edges(pg_graph)

        # collects all edges and turns them into StaticLines
        from_vec = Graphs.src.(e)
        to_vec = Graphs.dst.(e)

        lines = Array{Any}(undef, length(e))
        nodes = Array{Any}(undef, Graphs.nv(pg_graph))

        for l in 1:length(e)
            lines[l] = StaticLine(from = from_vec[l], to = to_vec[l], Y = -1im*K) 
        end

        
            
        if balancing_method == "None"
            for n in 1:Graphs.nv(pg_graph)
                nodes[n] = node_type(;node_parameters_dict...,P=P_vec[n])
            end
            newnodes = nodes
        else @assert (false) "Balancing method "* balancing_method * "not yet implemented."
            # newnodes[end] = SlackAlgebraic(U = complex(1.0))
        end

        pg = PowerGrid(newnodes, lines)
        # op = find_operationpoint(pg, sol_method=:rootfind) stopped working in newer vesion!
        try # necessary because nlsolve does not converge for grid 18 at first try in newer version 
                op = find_operationpoint(pg, sol_method=:nlsolve)

                # check if acceptable operation point and return if so
                dynamic_idxs = findall(idx -> (typeof(nodes[idx]) !== SlackAlgebraic), 1:N)
                if all(isapprox.(op[:, :v], 1.0, atol=0.01)) && all(isapprox.(op[dynamic_idxs, :ω], 0, atol=1e-9))
                    _, stable = check_eigenvalues_MM(pg, op)
                    if stable 
                        sol = simulate_from_state(pg, op.vec, (0.,25.))
                        if SciMLBase.successful_retcode(sol)
                            return pg, op
                        end
                    end
                end
        catch
        end
        counter += 1
    end
    print("unsuccessful!")
end 

function star_graph_toy_model(n, node_parameters_dict, K)
    node_type = SwingEqLVS
    lines = Array{Any}(undef, n+1)
    nodes = Array{Any}(undef, n+2)
    
    from_vec = append!([1], 2 .* ones(Int, n) )
    to_vec = Vector{Int64}(2:n+2) 
    
    for l in 1:n+1
        lines[l] = StaticLine(from = from_vec[l], to = to_vec[l], Y = -1im*K) 
    end
    
    P_vec = append!([1.,-1.], zeros(Float64, n))
    for i in 1:n+2
        nodes[i] = node_type(;node_parameters_dict...,P=P_vec[i])
    end
    
    pg = PowerGrid(nodes, lines)
    op = find_operationpoint(pg, sol_method=:nlsolve)
    
    # check if acceptable operation point and return if so
    if all(isapprox.(op[:, :v], 1.0, atol=0.01)) && all(isapprox.(op[:, :ω], 0, atol=1e-9))
        _, stable = check_eigenvalues_MM(pg, op)
        if stable 
            sol = simulate_from_state(pg, op.vec, (0.,25.))
            if SciMLBase.successful_retcode(sol)
                return pg, op
            else
                print("Simulation unsuccessful.")
            end
        end
    end
end
    
function all_to_all_toy_model(n, node_parameters_dict, K)
    node_type = SwingEqLVS
    N = n+2
    lines = Array{Any}(undef, Int((n+1)*(n)/2 + 1))
    nodes = Array{Any}(undef, n+2)
    lines[1] = StaticLine(from = 1, to = 2, Y = -1im*K)
    counter = 2
    for from_idx in 2:N-1
        for to_idx in from_idx+1:N
            lines[counter] = StaticLine(from = from_idx, to = to_idx, Y = -1im*K) 
            counter += 1
        end
    end

    P_vec = append!([1.,-1.], zeros(Float64, n))
    for i in 1:n+2
        nodes[i] = node_type(;node_parameters_dict...,P=P_vec[i])
    end

    pg = PowerGrid(nodes, lines)
    op = find_operationpoint(pg, sol_method=:nlsolve)

    # check if acceptable operation point and return if so
    if all(isapprox.(op[:, :v], 1.0, atol=0.01)) && all(isapprox.(op[:, :ω], 0, atol=1e-9))
        _, stable = check_eigenvalues_MM(pg, op)
        if stable 
            sol = simulate_from_state(pg, op.vec, (0.,25.))
            if SciMLBase.successful_retcode(sol)
                return pg, op
            else
                print("Simulation unsuccessful.")
            end
        end
    end
    
end


function two_node_toy_model(n, node_parameters_dict, K)
    nodes = [SwingEqLVS(;node_parameters_dict...,P=P),
            SwingEqLVS(;node_parameters_dict...,P=-P),
            SlackAlgebraic(;U=Complex(1))]
    lines = [StaticLine(from = 1, to = 2, Y = -1im*K),
            StaticLine(from = 2, to = 3, Y = -1im*K*n)]

    pg = PowerGrid(nodes, lines)
    op = find_operationpoint(pg, sol_method=:nlsolve)

    # check if acceptable operation point and return if so
    N = length(nodes)
    dynamic_idxs = findall(idx -> (typeof(nodes[idx]) !== SlackAlgebraic), 1:N)
    if all(isapprox.(op[:, :v], 1.0, atol=0.01)) && all(isapprox.(op[dynamic_idxs, :ω], 0, atol=1e-9))
        _, stable = check_eigenvalues_MM(pg, op)
        if stable 
            sol = simulate_from_state(pg, op.vec, (0.,25.))
            if SciMLBase.successful_retcode(sol)
                return pg, op
            else
                print("Simulation unsuccessful.")
            end
        end
    end
    
end


"""
classification_values(classes)
returns a numerical value for each class (to help ordering data by class)
it is not the same value as the index of the corresponding color in palette(:tab10), see function color_nodes(pg)
order is as in legend in figure with graph and node classes
"""
function classification_values(classes)
    class_values = zeros(N)
    for i in 1:length(classes)
        class = classes[i]
        if class == "Bulk"
            class_values[i] = 1
        elseif class == "Root"
            class_values[i] = 2
        elseif class == "Inner Tree Node"
            class_values[i] = 3
        elseif class == "Proper Leaf"
            class_values[i] = 6
        elseif class == "Sparse Sprout"
            class_values[i] = 4
        elseif class == "Dense Sprout"
            class_values[i] = 5
        end 
    end
    class_values
end


"""
sort_by_class(array, classes)
returns array, but sorted by classes in the order of classification_values(classes)
"""
function sort_by_class(array, classes)
    class_values = classification_values(classes)
    array_with_class_values = Pair.(array, class_values)
    array_with_class_values_sorted = sort(array_with_class_values, by=(x -> x.second))
    array_sorted_by_class = map(x -> x.first, array_with_class_values_sorted)
end


"""
color_nodes(pg::PowerGrid)
returns vector of colors according to the topological node classes
The classes are obtained with tree_node_classification
"""
function color_nodes(pg::PowerGrid)
    N = length(pg.nodes)
    pg_graph = pg.graph
    classes =  full_node_classification(pg_graph, 100, 5) # last argument is "<=" threshold for sparse sprouts
    pa = palette(:tab10)
    node_color = fill(colorant"red", N)
    for i in 1:N
        class = classes[i]
        if class == "Bulk"
            node_color[i] = pa[8]
        elseif class == "Root"
            node_color[i] = pa[6]
        elseif class == "Inner Tree Node"
            node_color[i] = pa[3]
        elseif class == "Proper Leaf"
            node_color[i] = pa[9]
        elseif class == "Sparse Sprout"
            node_color[i] = pa[10]
        elseif class == "Dense Sprout"
            node_color[i] = pa[1]
        end 
    end
    node_color
end

"""
power_shape_nodes(pg::PowerGrid)
returns vector of shapes according to the nodes being generators or consumers
"""
function power_shape_nodes(pg::PowerGrid)
    N = length(pg.nodes)
    node_markers = Vector{Any}(undef, N ) 
    nodes = pg.nodes
    for i in 1:N
        type = typeof(nodes[i])
        @assert type == SlackAlgebraic || type == SwingEqLVS || type == SwingEq "node type has to be SlackAlgebraic, SwingEq, or SwingEqLVS"
        node_markers[i] = '😄' # smile if something goes wrong :)
        if type == SlackAlgebraic
            node_markers[i] = :star8 # :star does not work
        elseif type == SwingEqLVS || type == SwingEq
            if pg.nodes[i].P < 0 # consumer
                node_markers[i] = :hline #:rect
            elseif pg.nodes[i].P > 0 # generator
                node_markers[i] = :cross #:circle
            elseif pg.nodes[i].P == 0
                node_markers[i] = :circle
            end
        end
    end
    node_markers
end