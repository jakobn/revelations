using Graphs
using GraphPlot # may not be needed but now it is here anyway
using GraphMakie
using CairoMakie
using TreeNodeClassification
using Plots # for palette()
using FixedPointNumbers # to load in the node_colors from topology_data

include(joinpath(@__DIR__, "network_generation.jl"))

"""
    function my_graph_plot(pg::PowerGrid, label_nodes = [])

Using GraphMakie to plot some nice powergrids. Markersize is with respect to the SNBS. 
"""
# function my_graph_plot(pg::PowerGrid, df::DataFrame, pg_idx::Int, label_nodes = [])
    # df_pg = df[df.PG_IDX .== pg_idx, :] # use only the subset of the data frame
function my_graph_plot(pg::PowerGrid, label_nodes = [])
    N = length(pg.nodes)
    node_color = color_nodes(pg)
    node_marker = power_shape_nodes(pg)

    if label_nodes != []
        node_label = fill("", N)
        if length(label_nodes) == 1
            node_label[label_nodes] = string(label_nodes)
        else    
            map(n -> node_label[n] = string(n), label_nodes)
        end
        f, ax, p = GraphMakie.graphplot(pg.graph, node_marker = node_marker, nlabels = node_label, node_color = node_color, node_size = 30)
        # f, ax, p = gplot(pg.graph, node_marker = node_marker, nlabels = node_label, node_color = node_color, node_size = 20)
    else
        f, ax, p = GraphMakie.graphplot(pg.graph, node_marker = node_marker, node_color = node_color, node_size = 30)
        # f, ax, p = gplot(pg.graph, node_marker = node_marker, node_color = node_color, node_size = 20)
    end
    #some error occured here suddenly on Dec 8 2021
    hidedecorations!(ax); hidespines!(ax)
    ax.aspect = DataAspect()
    return f
end 
