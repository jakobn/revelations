
using OrderedCollections
using LinearAlgebra
using PowerDynamics

include(joinpath(@__DIR__, "laplacian.jl"))
"""
Kronecker delta function: returns true, if i=j, false else
"""
function δ(i,j)
    ==(i,j)
end

"""
corotating_frequency(solitary_frequency, α, α_solitary, N, N_slacks::Int)

returns the solitary frequency in the corotating frame with the synchronized component,
i.e., solitary_frequency - synchronization_frequency.

The corotating frequency has to be the solitary frequency if there are slack nodes in the network.
"""
function corotating_frequency(solitary_frequency, α, α_solitary, N, N_slacks::Int)
    corotating_frequency = solitary_frequency * (1 + (N_slacks==0)* α_solitary/ (α * (N-1)) )
end

"""
static_frequency(corotating_frequency, α, α_solitary, N, N_slacks::Int)

returns the solitary frequency in the static frame,
i.e., solitary_frequency + synchronization_frequency.
Tranlates corotating frequency back to static frame.
Used to translate neetwork modes, with which the corotating frequency is in resonance.

The corotating frequency has to be thestatic frequency if there are slack nodes in the network.
"""
function static_frequency(corotating_frequency, α, α_solitary, N, N_slacks::Int)
    static_frequency = corotating_frequency / (1 + (N_slacks==0)* α_solitary/ (α * (N-1)) )
end

"""
corotating_frequency_derivative(α, α_solitary, N, N_slacks::Int)

derivative of corotating frequency with respect to solitary frequency
Is 1 if there are slack nodes in the system.
"""
function corotating_frequency_derivative(α, α_solitary, N, N_slacks::Int)
    derivative = 1 + (N_slacks==0)* α_solitary/ (α * (N-1))
end

"""
synchronization_frequency(solitary_frequencies, α, all_α_solitary, N, N_slacks::Int)

returns the synchronization frequency of the synchronized component.
It is usually different from its standard value zero
due to the power imbalance that the solitary states introduces in the synchronized component.

solitary_frequencies and all_alpha_solitary can be scalars or vectors, depending in the number of solitary nodes.

The synchronization frequency is zero if there are slack nodes in the network.
"""
function synchronization_frequency(solitary_frequencies, α, all_α_solitary, N, N_slacks::Int)
    N_solitaries = length(solitary_frequencies)
    @assert length(all_α_solitary) == N_solitaries "number of solitary frequencies and solitary α has to match."
    N_sync = N - N_solitaries
    synchronization_frequency = - sum(solitary_frequencies .* all_α_solitary)/(α * N_sync)
    if N_slacks > 0
        synchronization_frequency = 0
    end
    synchronization_frequency
end

"""
prepare_self_consistency_equation(pg::PowerGrid, op::State{PowerGrid, Float64}, original_solitary_idxs::Vector{Int})

Given a vector of solitary indices (or a single scalar index), this returns
1. the amplitude_factors: sum (over neighbors of solitary node) of squared eigenvector components times squared coupling strengths,
(per solitary node: one vector of length: number of modes),
2. eigenvalues of the reduced Laplacian,
that are needed to compute the self-consistency function
"""
function prepare_self_consistency_equation(pg::PowerGrid, op::State{PowerGrid, Float64}, original_solitary_idxs)
    slack_idxs = findall(x -> (typeof(x) == SlackAlgebraic), pg.nodes)
    N_slacks = length(slack_idxs)
    @assert all(all.(broadcast(idx -> (idx .!== slack_idxs), original_solitary_idxs) ) ) "Slack and solitary idxs can not match!"
    dynamic_Laplacian = steady_state_Laplacian(pg, op)
    if N_slacks == 0
        dynamic_solitary_idxs = original_solitary_idxs
    else
        dynamic_solitary_idxs = original_solitary_idxs .- broadcast(idx -> sum(idx .> slack_idxs), original_solitary_idxs)
    end
    reduced_Laplacian = reduce_Laplacian(dynamic_Laplacian, dynamic_solitary_idxs)
    K_matrix = coupling_matrix(pg)
    reduced_eigenvalues, reduced_eigenvectors = eigen(reduced_Laplacian)
    N = length(pg.nodes)
    N_solitaries = length(original_solitary_idxs)
    N_sync = N - N_solitaries
    N_sync_dynamic = N_sync - N_slacks
    all_amplitude_factors = Vector{Matrix{Float64}}(undef, N_solitaries)
    for idx in 1:N_solitaries
        original_solitary_idx = original_solitary_idxs[idx]
        # which idxs should be cut from the list of neighbors (because solitary or slack)
        cut_idxs = sort(vcat(original_solitary_idxs, slack_idxs))
        solitary_neighbor_idxs_original = setdiff( neighbors(pg.graph, original_solitary_idx), cut_idxs)
        # lower the index for every slack or solitary below it
        solitary_neighbor_idxs_reduced_dynamic = solitary_neighbor_idxs_original .- broadcast(idx -> sum(idx .> cut_idxs), solitary_neighbor_idxs_original)
        # need entries only for the remaining neighbors (with corrected index) and for all modes
        # dims: (number of solitary neighbors, number of modes)
        reduced_eigenvector_components_squared = reduced_eigenvectors[solitary_neighbor_idxs_reduced_dynamic, :].^2
        # vector of squared coupling strenghts with length: number of solitary neighbors
        coupling_strengths_squared = K_matrix[original_solitary_idx, solitary_neighbor_idxs_original].^2
        # in case of several neighbors: carry out sum over k (idxs of neighbors of solitary node)
        # the repeated object has dims (number of solitary neighbors, number of modes)
        all_amplitude_factors[idx] = sum(reduced_eigenvector_components_squared .* repeat(coupling_strengths_squared, 1, N_sync_dynamic), dims=1)
    end
        
    reduced_eigenvalues, all_amplitude_factors

end

"""
mean_power_flow_1_solitary(reduced_eigenvalues::Vector{Float64}, amplitude_factors::Vector{Matrix{Float64}}, solitary_frequency, m, α, α_solitary, N, N_slacks::Int)

Returns the mean power flow in case of one solitary node, from the solitary node to its neighbors.

In case of several neighbors, the flow to each of them can be computed by niserting only the respective amplitude factors.
Note that the sum over the neighbors is already carried out in prepare_self_consistency_equation().

"""    
function mean_power_flow_1_solitary(reduced_eigenvalues::Vector{Float64}, amplitude_factors::Matrix{Float64}, solitary_frequency, m, α, α_solitary, N, N_slacks::Int)
    ω_c = corotating_frequency(solitary_frequency, α, α_solitary, N, N_slacks)
    p = 0.5 * α * ω_c *
        dot(amplitude_factors, 1 ./ ( (reduced_eigenvalues .- m * ω_c^2).^2 .+ α^2 * ω_c^2) )
end

"""
self_consistency_equation_1_solitary(reduced_eigenvalues::Vector{Float64}, amplitude_factors::Matrix{Float64}, solitary_frequency, m, P_solitary, α, α_solitary, N, N_slacks::Int)

Returns the value of the self-consistency function (mean change in solitary frequency).
"""
function self_consistency_equation_1_solitary(reduced_eigenvalues::Vector{Float64}, amplitude_factors::Matrix{Float64}, solitary_frequency, m, P_solitary, α, α_solitary, N, N_slacks::Int)
    ω_c = corotating_frequency(solitary_frequency, α, α_solitary, N, N_slacks)
    Z = P_solitary -
        α_solitary *solitary_frequency -
        0.5 * α * ω_c *
        dot(amplitude_factors, 1 ./ ( (reduced_eigenvalues .- m * ω_c^2).^2 .+ α^2 * ω_c^2) )
end

"""
self_consistency_derivative_1_solitary(reduced_eigenvalues::Vector{Float64}, amplitude_factors::Matrix{Float64}, solitary_frequency, α, α_solitary, N, N_slacks::Int)

Returns the derivative of the self-consistency function for one solitary node with respect to the solitary frequency.
"""
function self_consistency_derivative_1_solitary(reduced_eigenvalues::Vector{Float64}, amplitude_factors::Matrix{Float64}, solitary_frequency, m, α, α_solitary, N, N_slacks::Int)
    ω_c = corotating_frequency(solitary_frequency, α, α_solitary, N, N_slacks)
    dissonance = (reduced_eigenvalues .- m * ω_c^2)
    derivative = - α_solitary - 0.5 * corotating_frequency_derivative(α, α_solitary, N, N_slacks) * α *
        dot(amplitude_factors, (dissonance.^2 .+ α^2 * ω_c^2 .+ 2 * ω_c^2 .* (2 * m .* dissonance .- α^2 * ω_c)) ./ (dissonance.^2 .+ α^2 * ω_c^2).^2)
end

"""
self_consistency_stability_1_solitary(reduced_eigenvalues::Vector{Float64}, amplitude_factors::Matrix{Float64}, solitary_frequency, m, α, α_solitary, N, N_slacks::Int)

Returns Boolean value for linear stability of self-consistency equation, if solitary_frequency is a solution.
"""
function self_consistency_stability_1_solitary(reduced_eigenvalues::Vector{Float64}, amplitude_factors::Matrix{Float64}, solitary_frequency, m, α, α_solitary, N, N_slacks::Int)
    0 > self_consistency_derivative_1_solitary(reduced_eigenvalues, amplitude_factors, solitary_frequency, m, α, α_solitary, N, N_slacks)
end

"""
self_consistency_equation_several_solitaries(reduced_eigenvalues::Vector{Float64}, all_amplitude_factors::Vector{Matrix{Float64}}, solitary_frequencies::Vector{Float64}, m, all_P_solitary::Vector{Float64}, α, all_α_solitary::Vector{Float64}, N, N_slacks::Int)

Experimental: theory is needed.
Evaluates self-consistency equations for several solitay nodes.
"""
function self_consistency_equation_several_solitaries(reduced_eigenvalues::Vector{Float64}, all_amplitude_factors::Vector{Matrix{Float64}}, solitary_frequencies::Vector{Float64}, m, all_P_solitary::Vector{Float64}, α, all_α_solitary::Vector{Float64}, N, N_slacks::Int)
    N_solitaries = length(solitary_frequencies)
    N_sync = N - N_solitaries
    @assert N_sync == length(reduced_eigenvalues) "number of modes and solitaries do not add up to system size."
    corotating_frequencies = solitary_frequencies .- synchronization_frequency(solitary_frequencies, α, all_α_solitary, N, N_slacks)
    all_frequency_matches = broadcast(ω -> (ω .== solitary_frequencies), solitary_frequencies)
    all_matched_amplitude_factors = broadcast(x -> sum(x, dims=1), all_frequency_matches .* all_amplitude_factors)

    #all_Z = all_P_solitary .-
    #    all_α_solitary .* solitary_frequencies .-
    #    0.5 * α * corotating_frequencies .*
    #    sum.( broadcast(x -> transpose(x) ./ (repeat(reduced_eigenvalues, 1, N_solitaries) .- m * repeat(transpose(corotating_frequencies.^2), N_sync, 1 ) ).^2 .+ α^2 * repeat(transpose(corotating_frequencies.^2), N_sync, 1 ), all_matched_amplitude_factors) ) .+
    #    α^2 * corotating_frequencies.^2
    all_Z = broadcast(idx -> all_P_solitary[idx] - 
        all_α_solitary[idx] * solitary_frequencies[idx] -
        0.5 * α * corotating_frequencies[idx] *
        dot( all_matched_amplitude_factors[idx], 1 ./ ( (reduced_eigenvalues .- m * corotating_frequencies[idx]^2 ).^2 .+ α^2 * corotating_frequencies[idx]^2) ),
        1:N_solitaries)

end

"""
aanalytic_mean_power_flow_two_node_toy_model(K, solitary_frequency, m, α, α_solitary, n)

Return value of analytic mean power flow for two-node toy-model.
"""
function analytic_mean_power_flow_two_node_toy_model(K, solitary_frequency, m, α, α_solitary, n)
    N_slacks = 1
    N = n + 2
    ω_c = corotating_frequency(solitary_frequency, α, α_solitary, N, N_slacks)
    p = 0.5 * K^2 * α * ω_c * (1 / ( (K * n - m * ω_c^2)^2 + α^2 * ω_c^2 ) )
end

"""
analytic_mean_power_flow_star_graph_toy_model(K, solitary_frequency, m, α, α_solitary, n)

Return value of analytic mean power flow for star-graph toy-model.
"""
function analytic_mean_power_flow_star_graph_toy_model(K, solitary_frequency, m, α, α_solitary, n)
    N_slacks = 0
    N = n + 2
    ω_c = corotating_frequency(solitary_frequency, α, α_solitary, N, N_slacks)
    p = 0.5 * K^2 * α * ω_c * (1/ (n+1) / ( (m * ω_c^2)^2 + α^2 * ω_c^2 ) + n / (n+1) / ( (K*(n+1) - m * ω_c^2)^2 + α^2 * ω_c^2 ) )
end

# analytic_mean_power_flow_all_to_all_toy_model
# same as star graph

"""
analytic_self_consistency_function(analytic_mean_power_flow_fct::Function, K, solitary_frequency, m, P_solitary, α, α_solitary, n)

given an analytic_mean_power_flow_fct, calculates the self-consistency function
"""
function analytic_self_consistency_function(analytic_mean_power_flow_fct::Function, K, solitary_frequency, m, P_solitary, α, α_solitary, n)
    Z = P_solitary - α_solitary * solitary_frequency - analytic_mean_power_flow_fct(K, solitary_frequency, m, α, α_solitary, n)
end

"""
analytic_derivative_mean_power_flow_two_node_toy_model(K, solitary_frequency, m, α, α_solitary, n)

analytic derivative of the mean power flow for the two-node toy model
"""
function analytic_derivative_mean_power_flow_two_node_toy_model(K, solitary_frequency, m, α, α_solitary, n)
    dissonance = K*n - m*solitary_frequency^2
    damping = α^2 * solitary_frequency^2
    dp = -α * K^2 * (dissonance^2 + damping + 2*solitary_frequency * (2* m * solitary_frequency * dissonance - damping)) / 2 / (dissonance^2 + damping)^2
end

"""
eigenvalues of the two-node toy model
"""
function analytic_eigenvalues_two_node_toy_model(K, n)
    reduced_eigenvalues = [K * n]
end

"""
eigenvalues of the contributing modes in the star-graph toy model
"""
function analytic_eigenvalues_star_graph_toy_model(K, n)
    reduced_eigenvalues = [0, K * (n+1)]
end

"""
eigenvectors in the two-node toy model
"""
function analytic_eigenvectors_two_node_toy_model(n)
    v = [1] 
end

"""
contributing eigenvectors of the star-graph toy model
"""
function analytic_eigenvectors_star_graph_toy_model(n)
    v_1 = normalize(ones(n+1))
    v_n_plus_1 = normalize( ones(n+1) - (n+1) * δ.(1, 1:(n+1)) ) 
end

"""
analytic amplitude factors of the two-node toy model
"""
function analytic_amplitude_factors_two_node_toy_model(K, n)
    amplitude_factors = Matrix{Float64}(undef, 1, 1)
    amplitude_factors[1,1] = K^2 * 1
    amplitude_factors
end

"""
analytic amplitude factors of the star-graph toy model
"""
function analytic_amplitude_factors_star_graph_toy_model(K, n)
    amplitude_factors = Matrix{Float64}(undef, 1, 2)
    amplitude_factors[1,:] = K^2 .* [1/(n+1), n/(n+1)]
    amplitude_factors
end

# was intended to serve for non integer n, however does not work because of vectors with length n+1 etc.
# untested
# to do: coupling strengths have to be provided in a vector with entry for each solitary
"""
function amplitude_factors(reduced_eigenvectors, coupling_strengths_squared, original_solitary_idxs, solitary_neighbor_idxs_original, slack_idxs, N)
    N_slacks = length(slack_idxs)
    if N_slacks == 0
        dynamic_solitary_idxs = original_solitary_idxs
    else
        dynamic_solitary_idxs = original_solitary_idxs .- broadcast(idx -> sum(idx .> slack_idxs), original_solitary_idxs)
    end
    N_solitaries = length(original_solitary_idxs)
    N_sync = N - N_solitaries
    N_sync_dynamic = N_sync - N_slacks
    all_amplitude_factors = Vector{Matrix{Float64}}(undef, N_solitaries)
    for idx in 1:N_solitaries
        original_solitary_idx = original_solitary_idxs[idx]
        # which idxs should be cut from the list of neighbors (because solitary or slack)
        cut_idxs = sort(vcat(original_solitary_idxs, slack_idxs))
        # lower the index for every slack or solitary below it
        solitary_neighbor_idxs_reduced_dynamic = solitary_neighbor_idxs_original .- broadcast(idx -> sum(idx .> cut_idxs), solitary_neighbor_idxs_original)
        # need entries only for the remaining neighbors (with corrected index) and for all modes
        # dims: (number of solitary neighbors, number of modes)
        reduced_eigenvector_components_squared = reduced_eigenvectors[solitary_neighbor_idxs_reduced_dynamic, :].^2
        # vector of squared coupling strenghts with length: number of solitary neighbors
        # in case of several neighbors: carry out sum over k (idxs of neighbors of solitary node)
        # the repeated object has dims (number of solitary neighbors, number of modes)
        all_amplitude_factors[idx] = sum(reduced_eigenvector_components_squared .* repeat(coupling_strengths_squared, 1, N_sync_dynamic), dims=1)
    end
    all_amplitude_factors
end
"""
#