using PowerDynamics
using DifferentialEquations

"""
simulate_from_state(pg, state, tspan; abstol=1e-6, reltol=1e-3)
simulates the trajectories of the nodal variables starting from state and returns the solution object
"""
function simulate_from_state(pg, state, tspan; abstol = 1e-6, reltol = 1e-3)
    # abstol and reltol are defaults for odes according to https://diffeq.sciml.ai/dev/basics/common_solver_opts/
    # as opposed to Anna Büttner's code I set force_dtmin = false (default) because I want it exact and compatability
    problem = ODEProblem(rhs(pg), state, tspan)
    solution = solve(problem, Rodas4(), abstol=abstol, reltol=reltol)
    solution
end

"""
single_node_perturbation(state, dvars, node_idx)

returns a perturbed state "newstate" with perturbed ω and ϕ at node_idx
"""
function single_node_perturbation_ϕ_ω(state, dvars, node_idx)
    dϕ, dω = dvars
    newstate_ϕ = ChangeInitialConditions(node = node_idx, var = :φ, f = Inc(dϕ))(state)
    newstate_ϕ_ω = ChangeInitialConditions(node = node_idx, var = :ω, f = Inc(dω))(newstate_ϕ)
    newstate_ϕ_ω
end

"""
simulate_from_perturbation_var(pg, state, dvar, var_perturber_function, node_idx, tspan; abstol=1e-6, reltol=1e-3)

simulates the trajectories of the nodal variables starting from state with perturbation dvar using var_perturber_function at node_idx and returns the solution object
"""
# function simulate_from_perturbation_var(pg, state, dvar, var_perturber_function, node_idx, tspan; abstol = 1e-6, reltol = 1e-3)
function simulate_from_perturbation_var(pg, state, dvar, var_perturber_function, node_idx, tspan; kwargs...)
    newstate = var_perturber_function(state, dvar, node_idx)
    # solution = simulate(DummyPerturbation(tspan), pg, newstate, tspan, abstol = abstol, reltol = reltol)
    # solution = simulate(DummyPerturbation(tspan), pg, newstate, tspan; kwargs...)
    problem = ODEProblem(rhs(pg), newstate.vec, tspan)
    # high accuracy:
    # solution = solve(problem, Rodas4(); kwargs... )
    # fast:
    solution = solve(problem, Tsit5(); kwargs... )
    solution
end


"""
calculate_statistics_ω(solution, t_stat_iter)
return mean ω and its std for network of SwingEqLVS or SwingEq (assuming u_r_1, u_i_1, ω_1, ..., u_r_N, u_i_N, ω_N as variables')
"""
function calculate_statistics_ω(solution, t_stat_iter)
    last_idx = length(solution.u[1])
    mean_ω =  mean(solution(t_stat_iter, idxs=3:3:last_idx).u)
    std_ω = std(solution(t_stat_iter, idxs=3:3:last_idx).u)
    mean_ω, std_ω
end


"""
calculate_statistics_ω(solution, t_stat_iter)
return mean ω and its std for network of SwingEqLVS or SwingEq (assuming u_r_1, u_i_1, ω_1, ..., u_r_N, u_i_N, ω_N as variables')
"""
function calculate_statistics_ω_idxs(solution, t_stat_iter, idxs)
    mean_ω =  mean(solution(t_stat_iter, idxs=idxs).u)
    std_ω = std(solution(t_stat_iter, idxs=idxs).u)
    mean_ω, std_ω
end