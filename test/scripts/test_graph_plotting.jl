include(joinpath(@__DIR__,"../../src/network_generation.jl"))
include(joinpath(@__DIR__,"../../src/plotting_functions.jl"))

ensemble_path = joinpath(@__DIR__, "../ensembles")
plot_path = joinpath(@__DIR__, "../plots/")

# synthetic grid
pg, RHS, op = find_power_grid_path(joinpath(ensemble_path, "random_pgs"), 1)
f = my_graph_plot(pg, [])
save(joinpath(plot_path, "grid_1_power_markers.pdf"), f)

f = my_graph_plot(pg, 1:100)
save(joinpath(plot_path, "grid_1_power_markers_all_labels.pdf"), f)

# toy models
n=7

## two_node_toy_model
pg, RHS, op = find_power_grid_path(joinpath(ensemble_path, "two_node_toy_models"), "two_node_toy_model_n_"*string(n))
power_shape_nodes(pg)
f = my_graph_plot(pg, [])

## all-to-all
pg, RHS, op = find_power_grid_path(joinpath(ensemble_path, "all_to_all_toy_models"), "all_to_all_toy_model_n_"*string(n))
f = my_graph_plot(pg, [])

## star graph
pg, RHS, op = find_power_grid_path(joinpath(ensemble_path, "star_graph_toy_models"), "star_graph_toy_model_n_"*string(n))
f = my_graph_plot(pg, [])

### with node labelfontsize
f = my_graph_plot(pg, [1,2,3,5,7,9])
f = my_graph_plot(pg, 1:9)
