using Random
using TreeNodeClassification
using FileIO
using JLD
using StatsBase
using Plots
using DataStructures
using Images
using FixedPointNumbers

include(joinpath(@__DIR__,"../../src/network_generation.jl"))
include(joinpath(@__DIR__,"../../src/plotting_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")
data_path = joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")

N = 100
node_type = SwingEqLVS # this is fixed in random_PD_grid
H = .5
P = 1.
D = .1
Ω = 1.
Γ = 1.
V = 1.
node_parameters_dict = Dict(:H=>H, :P=>P, :D=>D, :Ω=>Ω, :Γ=>Γ, :V=>V)
K = 6.

# test synthetic networks generation and saving
ensemble_path_local = joinpath(ensemble_path, "random_pgs")

### generate n_grids = M (50 in Nitzbon paper) power grids and store them
n_grids = 1 # 100 later

generation_parameters = Dict(
    :"n_grids"              => n_grids,
    :"N"                    => N,
    :"node_type"            => node_type,
    :"node_parameters"      => node_parameters_dict,
    :"K"                    => K
    )
# maybe add a unique name for an ensemble later 
FileIO.save(joinpath(ensemble_path, "random_pgs", "generation_parameters.jld"), "generation_parameters", generation_parameters)

classes = Array{Any}(undef, (n_grids, N))
n_dense_sprouts = Vector{Int}(undef, n_grids)
where_dense_sprouts = Vector{Vector{Int}}(undef, n_grids)
where_dense_roots = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees_unique = Vector{Vector{Int}}(undef, n_grids)
mean_degrees = Vector{Float64}(undef,n_grids)
node_colors = Vector{Any}(undef, n_grids) #fill(fill(colorant"red", N), n_grids)
node_markers = Vector{Any}(undef, n_grids) 
@time for grid_idx in 1:n_grids
    # this makes the grid generation reproducable (tested it!)
    Random.seed!(grid_idx)
    pg, op = random_PD_grid(N, node_parameters_dict, K; )
    write_powergrid(pg, joinpath( ensemble_path, "random_pgs", "grid_"*string(grid_idx)*".json"), Json)
    # pg_graph = graph_convert(pg.graph)
    pg_graph = pg.graph
    classes[grid_idx] = full_node_classification(pg_graph, N, 5) # second argument: maxiter, third argument: treshold (above a sprout is dense)
    where_dense_sprouts[grid_idx] = findall(i->i=="Dense Sprout",classes[grid_idx])
    length_dense_sprouts = length(where_dense_sprouts[grid_idx])
    n_dense_sprouts[grid_idx] = length_dense_sprouts
    where_dense_roots[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
    dense_root_degrees[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
    degrees = Graphs.degree(pg_graph)
    mean_degrees[grid_idx] = mean(degrees)
    node_colors[grid_idx] = color_nodes(pg)
    node_markers[grid_idx] = power_shape_nodes(pg)

    # dense root degrees, counting for each dense sprout
    position = 1
    for node_idx in where_dense_sprouts[grid_idx]
        root = Graphs.neighbors(pg_graph, node_idx)[1]
        where_dense_roots[grid_idx][position] = root
        #root_neighbors = Graphs.neighbors(pg_graph, root)
        dense_root_degrees[grid_idx][position] = Graphs.degree(pg_graph, root) # length(root_neighbors)
        position += 1
    end
    
    # counting each dense root only once
    where_dense_roots_tmp = where_dense_roots[grid_idx]
    where_dense_roots_unique_tmp = OrderedSet(where_dense_roots_tmp)
    dense_root_degrees_unique[grid_idx] = Vector{Int}(undef, length(where_dense_roots_unique_tmp))
    position = 1
    for node_idx in where_dense_roots_unique_tmp
        dense_root_degrees_unique[grid_idx][position] = degrees[node_idx]
        position += 1
    end

    # gplot = my_graph_plot(pg,[])
    # FileIO.save(plot_path*"grid_"*string(grid_idx)*".pdf", gplot)
end


FileIO.save(joinpath(ensemble_path, "random_pgs", "topology_data.jld"),
    "classes", classes,
    "n_dense_sprouts", n_dense_sprouts,
    "where_dense_sprouts", where_dense_sprouts,
    "where_dense_roots", where_dense_roots,
    "where_dense_roots_unique", OrderedSet.(where_dense_roots),
    "dense_root_degrees", dense_root_degrees,
    "dense_root_degrees_unique", dense_root_degrees_unique,
    "mean_degrees", mean_degrees,
    "node_colors", node_colors,
    "node_markers", node_markers)

# with FileIO the type of the colors is identified correctly when loading in again
topology_data = FileIO.load(joinpath(ensemble_path, "random_pgs", "topology_data.jld"))
topology_data["node_colors"]



# toy models
n = 7
N = n+2
n_grids = 1
grid_idx = 1

## star graph
ensemble_path_local = joinpath(ensemble_path, "star_graph_toy_models")

generation_parameters = Dict(
    :"n_grids"              => n_grids,
    :"N"                    => N,
    :"node_type"            => node_type,
    :"node_parameters"      => node_parameters_dict,
    :"K"                    => K
    )
FileIO.save(joinpath(ensemble_path_local, "generation_parameters.jld"), "generation_parameters", generation_parameters)

classes = Array{Any}(undef, (n_grids, N))
n_dense_sprouts = Vector{Int}(undef, n_grids)
where_dense_sprouts = Vector{Vector{Int}}(undef, n_grids)
where_dense_roots = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees_unique = Vector{Vector{Int}}(undef, n_grids)
mean_degrees = Vector{Float64}(undef,n_grids)
node_colors = Vector{Any}(undef, n_grids) #fill(fill(colorant"red", N), n_grids)
node_markers = Vector{Any}(undef, n_grids) 

pg, op = star_graph_toy_model(n, node_parameters_dict, K; )
write_powergrid(pg, joinpath( ensemble_path_local, "star_graph_toy_model_n_"*string(n)*".json"), Json)
# pg_graph = graph_convert(pg.graph)
pg_graph = pg.graph
classes[grid_idx] = full_node_classification(pg_graph, N, 5) # second argument: maxiter, third argument: treshold (above a sprout is dense)
where_dense_sprouts[grid_idx] = findall(i->i=="Dense Sprout",classes[grid_idx])
length_dense_sprouts = length(where_dense_sprouts[grid_idx])
n_dense_sprouts[grid_idx] = length_dense_sprouts
where_dense_roots[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
dense_root_degrees[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
degrees = Graphs.degree(pg_graph)
mean_degrees[grid_idx] = mean(degrees)
node_colors[grid_idx] = color_nodes(pg)
node_markers[grid_idx] = power_shape_nodes(pg)

# dense root degrees, counting for each dense sprout
position = 1
for node_idx in where_dense_sprouts[grid_idx]
    root = Graphs.neighbors(pg_graph, node_idx)[1]
    where_dense_roots[grid_idx][position] = root
    #root_neighbors = Graphs.neighbors(pg_graph, root)
    dense_root_degrees[grid_idx][position] = Graphs.degree(pg_graph, root) # length(root_neighbors)
    position += 1
end

# counting each dense root only once
where_dense_roots_tmp = where_dense_roots[grid_idx]
where_dense_roots_unique_tmp = OrderedSet(where_dense_roots_tmp)
dense_root_degrees_unique[grid_idx] = Vector{Int}(undef, length(where_dense_roots_unique_tmp))
position = 1
for node_idx in where_dense_roots_unique_tmp
    dense_root_degrees_unique[grid_idx][position] = degrees[node_idx]
    position += 1
end

# gplot = my_graph_plot(pg,[])
# FileIO.save(plot_path*"grid_"*string(grid_idx)*".pdf", gplot)



FileIO.save(joinpath(ensemble_path_local, "topology_data.jld"),
    "classes", classes,
    "n_dense_sprouts", n_dense_sprouts,
    "where_dense_sprouts", where_dense_sprouts,
    "where_dense_roots", where_dense_roots,
    "where_dense_roots_unique", OrderedSet.(where_dense_roots),
    "dense_root_degrees", dense_root_degrees,
    "dense_root_degrees_unique", dense_root_degrees_unique,
    "mean_degrees", mean_degrees,
    "node_colors", node_colors,
    "node_markers", node_markers)




## all-to-all
ensemble_path_local = joinpath(ensemble_path, "all_to_all_toy_models")

generation_parameters = Dict(
    :"n_grids"              => n_grids,
    :"N"                    => N,
    :"node_type"            => node_type,
    :"node_parameters"      => node_parameters_dict,
    :"K"                    => K
    )
FileIO.save(joinpath(ensemble_path_local, "generation_parameters.jld"), "generation_parameters", generation_parameters)

classes = Array{Any}(undef, (n_grids, N))
n_dense_sprouts = Vector{Int}(undef, n_grids)
where_dense_sprouts = Vector{Vector{Int}}(undef, n_grids)
where_dense_roots = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees_unique = Vector{Vector{Int}}(undef, n_grids)
mean_degrees = Vector{Float64}(undef,n_grids)
node_colors = Vector{Any}(undef, n_grids) #fill(fill(colorant"red", N), n_grids)
node_markers = Vector{Any}(undef, n_grids) 

pg, op = all_to_all_toy_model(n, node_parameters_dict, K; )
write_powergrid(pg, joinpath( ensemble_path_local, "all_to_all_toy_model_n_"*string(n)*".json"), Json)
# pg_graph = graph_convert(pg.graph)
pg_graph = pg.graph
classes[grid_idx] = full_node_classification(pg_graph, N, 5) # second argument: maxiter, third argument: treshold (above a sprout is dense)
where_dense_sprouts[grid_idx] = findall(i->i=="Dense Sprout",classes[grid_idx])
length_dense_sprouts = length(where_dense_sprouts[grid_idx])
n_dense_sprouts[grid_idx] = length_dense_sprouts
where_dense_roots[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
dense_root_degrees[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
degrees = Graphs.degree(pg_graph)
mean_degrees[grid_idx] = mean(degrees)
node_colors[grid_idx] = color_nodes(pg)
node_markers[grid_idx] = power_shape_nodes(pg)

# dense root degrees, counting for each dense sprout
position = 1
for node_idx in where_dense_sprouts[grid_idx]
    root = Graphs.neighbors(pg_graph, node_idx)[1]
    where_dense_roots[grid_idx][position] = root
    #root_neighbors = Graphs.neighbors(pg_graph, root)
    dense_root_degrees[grid_idx][position] = Graphs.degree(pg_graph, root) # length(root_neighbors)
    position += 1
end

# counting each dense root only once
where_dense_roots_tmp = where_dense_roots[grid_idx]
where_dense_roots_unique_tmp = OrderedSet(where_dense_roots_tmp)
dense_root_degrees_unique[grid_idx] = Vector{Int}(undef, length(where_dense_roots_unique_tmp))
position = 1
for node_idx in where_dense_roots_unique_tmp
    dense_root_degrees_unique[grid_idx][position] = degrees[node_idx]
    position += 1
end

# gplot = my_graph_plot(pg,[])
# FileIO.save(plot_path*"grid_"*string(grid_idx)*".pdf", gplot)



FileIO.save(joinpath(ensemble_path_local, "topology_data.jld"),
    "classes", classes,
    "n_dense_sprouts", n_dense_sprouts,
    "where_dense_sprouts", where_dense_sprouts,
    "where_dense_roots", where_dense_roots,
    "where_dense_roots_unique", OrderedSet.(where_dense_roots),
    "dense_root_degrees", dense_root_degrees,
    "dense_root_degrees_unique", dense_root_degrees_unique,
    "mean_degrees", mean_degrees,
    "node_colors", node_colors,
    "node_markers", node_markers)




## two-node toy model
ensemble_path_local = joinpath(ensemble_path, "two_node_toy_models")

generation_parameters = Dict(
    :"n_grids"              => n_grids,
    :"N"                    => N,
    :"node_type"            => node_type,
    :"node_parameters"      => node_parameters_dict,
    :"K"                    => K
    )
FileIO.save(joinpath(ensemble_path_local, "generation_parameters.jld"), "generation_parameters", generation_parameters)

classes = Array{Any}(undef, (n_grids, N))
n_dense_sprouts = Vector{Int}(undef, n_grids)
where_dense_sprouts = Vector{Vector{Int}}(undef, n_grids)
where_dense_roots = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees_unique = Vector{Vector{Int}}(undef, n_grids)
mean_degrees = Vector{Float64}(undef,n_grids)
node_colors = Vector{Any}(undef, n_grids) #fill(fill(colorant"red", N), n_grids)
node_markers = Vector{Any}(undef, n_grids) 

pg, op = two_node_toy_model(n, node_parameters_dict, K; )
write_powergrid(pg, joinpath( ensemble_path_local, "star_graph_toy_model_n_"*string(n)*".json"), Json)
# pg_graph = graph_convert(pg.graph)
pg_graph = pg.graph
classes[grid_idx] = full_node_classification(pg_graph, N, 5) # second argument: maxiter, third argument: treshold (above a sprout is dense)
where_dense_sprouts[grid_idx] = findall(i->i=="Dense Sprout",classes[grid_idx])
length_dense_sprouts = length(where_dense_sprouts[grid_idx])
n_dense_sprouts[grid_idx] = length_dense_sprouts
where_dense_roots[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
dense_root_degrees[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
degrees = Graphs.degree(pg_graph)
mean_degrees[grid_idx] = mean(degrees)
node_colors[grid_idx] = color_nodes(pg)
node_markers[grid_idx] = power_shape_nodes(pg)

# dense root degrees, counting for each dense sprout
position = 1
for node_idx in where_dense_sprouts[grid_idx]
    root = Graphs.neighbors(pg_graph, node_idx)[1]
    where_dense_roots[grid_idx][position] = root
    #root_neighbors = Graphs.neighbors(pg_graph, root)
    dense_root_degrees[grid_idx][position] = Graphs.degree(pg_graph, root) # length(root_neighbors)
    position += 1
end

# counting each dense root only once
where_dense_roots_tmp = where_dense_roots[grid_idx]
where_dense_roots_unique_tmp = OrderedSet(where_dense_roots_tmp)
dense_root_degrees_unique[grid_idx] = Vector{Int}(undef, length(where_dense_roots_unique_tmp))
position = 1
for node_idx in where_dense_roots_unique_tmp
    dense_root_degrees_unique[grid_idx][position] = degrees[node_idx]
    position += 1
end

# gplot = my_graph_plot(pg,[])
# FileIO.save(plot_path*"grid_"*string(grid_idx)*".pdf", gplot)



FileIO.save(joinpath(ensemble_path_local, "topology_data.jld"),
    "classes", classes,
    "n_dense_sprouts", n_dense_sprouts,
    "where_dense_sprouts", where_dense_sprouts,
    "where_dense_roots", where_dense_roots,
    "where_dense_roots_unique", OrderedSet.(where_dense_roots),
    "dense_root_degrees", dense_root_degrees,
    "dense_root_degrees_unique", dense_root_degrees_unique,
    "mean_degrees", mean_degrees,
    "node_colors", node_colors,
    "node_markers", node_markers)


# what happens for n=0?
n = 0

## star graph
pg, op = star_graph_toy_model(n, node_parameters_dict, K)

## all-to-all
pg, op = all_to_all_toy_model(n, node_parameters_dict, K)

## two-node toy model
pg, op = two_node_toy_model(n, node_parameters_dict, K)

