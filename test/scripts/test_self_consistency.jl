using CairoMakie
using JLD
using OrderedCollections
using Roots
using FileIO

include(joinpath(@__DIR__, "../../src/self_consistency.jl"))
include(joinpath(@__DIR__,"../../src/laplacian.jl"))
include(joinpath(@__DIR__, "../../src/network_generation.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles/")
data_path = joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")

size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
scaler = 1
size_pt = scaler .* 72 .* size_inches


# toy models n = 7
one_solitary_idx = 1



## star graph

ensemble_path_local = joinpath(ensemble_path,"star_graph_toy_models")

generation_parameters = FileIO.load(joinpath(ensemble_path_local, "generation_parameters.jld"))["generation_parameters"]
node_parameters_dict = generation_parameters["node_parameters"]
N = generation_parameters["N"]
n = N - 2

pg, RHS, op = find_power_grid_path(ensemble_path_local, "star_graph_toy_model_n_"*string(n))

N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))

K =generation_parameters["K"]
α = node_parameters_dict[:D]
m = 2*node_parameters_dict[:H]
α_solitary = pg.nodes[one_solitary_idx].D
m_solitary = 2 * pg.nodes[one_solitary_idx].H
P_solitary = pg.nodes[one_solitary_idx].P

# Eq. 5.4.4 in thesis
Laplacian = steady_state_Laplacian(pg, op)
# Eq. 5.4.5 in thesis
reduced_Laplacian = reduce_Laplacian(Laplacian, [one_solitary_idx])

# corotating_frequency
example_solitary_frequency = 6.1
corotating_frequency(example_solitary_frequency, α, α_solitary, N, N_slacks)
synchronization_frequency(example_solitary_frequency, α, α_solitary, N, N_slacks)

solitary_node = pg.nodes[one_solitary_idx]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])

reduced_eigenvalues
# they match the expected analytical eigenvalues
K .* [0, 1, 1, 1, 1, 1, 1, n+1]

amplitude_factors = all_amplitude_factors[1]
# they match the expected analytical amplitude factors:
analytical_amplitude_factors = K^2 .* [1/(n+1), 0, 0, 0, 0, 0, 0, n^2/(n^2+n)]

ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

ω_range = ω_min:0.001:ω_max
p_range = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range)

Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P - α ω$")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range, p_range, label = L"numerical $p$")

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))

# does it look like the analytical solution? yes.
analytic_p_range = map(ω -> analytic_mean_power_flow_star_graph_toy_model(K, ω, m, α, N), ω_range)
Makie.lines!(ax, ω_range, analytic_p_range, label=L"analytical $ p $", linestyle = :dot)
leg = axislegend(position = :rt)
Fig
save(joinpath(plot_path, "star_graph_n_7_self_consistency_intersection.pdf"), Fig)

# evaluation of solutions and stability
one_solitary_solutions = find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, P_solitary, α, α_solitary, N, N_slacks), ω_min, ω_max)
one_solitary_stabilities = map(ω -> self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), one_solitary_solutions)
stable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities)]
stable_one_solitary_solutions_physical = stable_one_solitary_solutions[abs.(stable_one_solitary_solutions) .> 10*eps(Float64)]

# technically, 0 should not be a solution, because the analytic function diverges,
# but it is correct, since the synchronous state is a degenerated solitary state.





## all-to-all
ensemble_path_local = joinpath(ensemble_path,"all_to_all_toy_models")

generation_parameters = FileIO.load(joinpath(ensemble_path_local, "generation_parameters.jld"))["generation_parameters"]
node_parameters_dict = generation_parameters["node_parameters"]
N = generation_parameters["N"]
n = N - 2

pg, RHS, op = find_power_grid_path(ensemble_path_local, "all_to_all_toy_model_n_"*string(n))

N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))

K =generation_parameters["K"]

α = node_parameters_dict[:D]
m = 2*node_parameters_dict[:H]
α_solitary = pg.nodes[one_solitary_idx].D
m_solitary = 2 * pg.nodes[one_solitary_idx].H
P_solitary = pg.nodes[one_solitary_idx].P

# Eq. 5.4.12 in thesis
Laplacian = steady_state_Laplacian(pg, op)
# Eq. 5.4.13 in thesis
reduced_Laplacian = reduce_Laplacian(Laplacian, [one_solitary_idx])

# corotating_frequency
example_solitary_frequency = 6.1
corotating_frequency(example_solitary_frequency, α, α_solitary, N, N_slacks)
synchronization_frequency(example_solitary_frequency, α, α_solitary, N, N_slacks)



solitary_node = pg.nodes[one_solitary_idx]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])

reduced_eigenvalues
# they match the expected analytical eigenvalues
K * (n+1) .* [0, 1, 1, 1, 1, 1, 1, 1]

amplitude_factors = all_amplitude_factors[1]
eigenvectors = eigen(reduced_Laplacian).vectors

# eigen gives us orthogonal eigenvectors for identical eigenvalues
for i in 2:n+1
    for j in i+1:n+1
        println(dot(eigenvectors[:,2], eigenvectors[:,3]))
    end
end

# actually, for all eigenvectors!
for i in 1:n+1
    for j in i+1:n+1
        println(dot(eigenvectors[:,2], eigenvectors[:,3]))
    end
end

# they do not match the analytical amplitude factors, because in the thesis they were wrong (not orthogonal)
analytical_amplitude_factors = K^2 .* [1/(n+1), 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5]
sum(analytical_amplitude_factors)

ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

ω_range = ω_min:0.001:ω_max
p_range = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range)

Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P - α ω$")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range, p_range, label = L"numerical $p$")

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))

# can't compare to analytical solution at the moment, however, the analytical solution of the star graph
# matches the numerics,
# which is not surprising as the other links should get redundant

analytic_p_range = map(ω -> analytic_mean_power_flow_star_graph_toy_model(K, ω, m, α, N), ω_range)
Makie.lines!(ax, ω_range, analytic_p_range, label=L"analytical $ p $", linestyle = :dot)

leg = axislegend(position = :rt)
Fig
save(joinpath(plot_path, "all_to_all_n_7_self_consistency_intersection.pdf"), Fig)

# evaluation of solutions and stability
one_solitary_solutions = find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, P_solitary, α, α_solitary, N, N_slacks), ω_min, ω_max)
one_solitary_stabilities = map(ω -> self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), one_solitary_solutions)
stable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities)]
stable_one_solitary_solutions_physical = stable_one_solitary_solutions[abs.(stable_one_solitary_solutions) .> 10*eps(Float64)]

# technically, 0 should not be a solution, because the analytic function diverges,
# but it is correct, since the synchronous state is a degenerated solitary state.





## two-node toy model
ensemble_path_local = joinpath(ensemble_path,"two_node_toy_models")

generation_parameters = FileIO.load(joinpath(ensemble_path_local, "generation_parameters.jld"))["generation_parameters"]
node_parameters_dict = generation_parameters["node_parameters"]
N = generation_parameters["N"]
n = N - 2

pg, RHS, op = find_power_grid_path(ensemble_path_local, "two_node_toy_model_n_"*string(n))

N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))

K =generation_parameters["K"]
α = node_parameters_dict[:D]
m = 2*node_parameters_dict[:H]
α_solitary = pg.nodes[one_solitary_idx].D
m_solitary = 2 * pg.nodes[one_solitary_idx].H
P_solitary = pg.nodes[one_solitary_idx].P

# cf. chapter 4 in thesis
Laplacian = steady_state_Laplacian(pg, op)
# Eq. 5.4.2 in thesis, and chapter 4
reduced_Laplacian = reduce_Laplacian(Laplacian, [one_solitary_idx])

# corotating_frequency
example_solitary_frequency = 6.1
corotating_frequency(example_solitary_frequency, α, α_solitary, N, N_slacks)
synchronization_frequency(example_solitary_frequency, α, α_solitary, N, N_slacks)

solitary_node = pg.nodes[one_solitary_idx]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])

reduced_eigenvalues
# they match the expected analytical eigenvalues
K * n

amplitude_factors = all_amplitude_factors[1]
# they match the expected analytical amplitude factors:
analytical_amplitude_factors = K^2 * 1

ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

ω_range = ω_min:0.001:ω_max
p_range = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range)

Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P - α ω$")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range, p_range, label = L"numerical $p$")

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))

# does it look like the analytical solution? yes.
analytic_p_range = map(ω -> analytic_mean_power_flow_two_node_toy_model(K, ω, m, α, N), ω_range)
Makie.lines!(ax, ω_range, analytic_p_range, label=L"analytical $ p $", linestyle = :dot)
leg = axislegend(position = :rt)
Fig
save(joinpath(plot_path, "two_node_toy_model_n_7_self_consistency_intersection.pdf"), Fig)


# evaluation of solutions and stability
one_solitary_solutions = find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, P_solitary, α, α_solitary, N, N_slacks), ω_min, ω_max)
one_solitary_stabilities = map(ω -> self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), one_solitary_solutions)
stable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities)]
stable_one_solitary_solutions_physical = stable_one_solitary_solutions[abs.(stable_one_solitary_solutions) .> 10*eps(Float64)]

# here, without a bulk mode, "0" is not a solution that is found





# what happens for n=0?
n = 0
N = n+2
one_solitary_idx = 1

H = .5
P = 1.
D = .1
Ω = 1.
Γ = 1.
V = 1.
node_parameters_dict = Dict(:H=>H, :P=>P, :D=>D, :Ω=>Ω, :Γ=>Γ, :V=>V)
K = 6.

## two node toy model
pg, op = two_node_toy_model(n, node_parameters_dict, K)
N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))

α = node_parameters_dict[:D]
m = 2*node_parameters_dict[:H]
α_solitary = pg.nodes[one_solitary_idx].D
m_solitary = 2 * pg.nodes[one_solitary_idx].H
P_solitary = pg.nodes[one_solitary_idx].P

Laplacian = steady_state_Laplacian(pg, op)
# Eq. 5.4.2 in thesis, and chapter 4
reduced_Laplacian = reduce_Laplacian(Laplacian, [one_solitary_idx])

reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])

reduced_eigenvalues
# they match the expected analytical eigenvalues
K * n

amplitude_factors = all_amplitude_factors[1]
# they match the expected analytical amplitude factors:
analytical_amplitude_factors = K^2 * 1

ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

ω_range = ω_min:0.001:ω_max
p_range = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range)

Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P - α ω$")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range, p_range, label = L"numerical $p$")

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))

# does it look like the analytical solution? yes.
analytic_p_range = map(ω -> analytic_mean_power_flow_two_node_toy_model(K, ω, m, α, N), ω_range)
Makie.lines!(ax, ω_range, analytic_p_range, label=L"analytical $ p $", linestyle = :dot)
leg = axislegend(position = :rt)
Fig

# as expected, only one stable solution at the natural frequency





# synthetic grids
grid_idx = 1
ensemble_path_local = joinpath(ensemble_path,"random_pgs")
data_path_local = joinpath(data_path,"random_pgs")

generation_parameters = FileIO.load(joinpath(ensemble_path_local, "generation_parameters.jld"))["generation_parameters"]
node_parameters_dict = generation_parameters["node_parameters"]
N = generation_parameters["N"]

pg, RHS, op = find_power_grid_path(ensemble_path_local, grid_idx)

N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))

topology_data = FileIO.load(joinpath(ensemble_path_local, "topology_data.jld"))
where_dense_sprouts = topology_data["where_dense_sprouts"]
where_dense_sprouts[grid_idx]

one_solitary_idx = 74

K =generation_parameters["K"]
α = node_parameters_dict[:D]
m = 2*node_parameters_dict[:H]
α_solitary = pg.nodes[one_solitary_idx].D
m_solitary = 2 * pg.nodes[one_solitary_idx].H # not needed
P_solitary = pg.nodes[one_solitary_idx].P

Laplacian = steady_state_Laplacian(pg, op)
reduced_Laplacian = reduce_Laplacian(Laplacian, [one_solitary_idx])

# corotating_frequency
example_solitary_frequency = 6.1
corotating_frequency(example_solitary_frequency, α, α_solitary, N, N_slacks)
synchronization_frequency(example_solitary_frequency, α, α_solitary, N, N_slacks)

solitary_node = pg.nodes[one_solitary_idx]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])

reduced_eigenvalues

amplitude_factors = all_amplitude_factors[1]
Makie.vlines(sqrt.(reduced_eigenvalues))

ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

ω_range = ω_min:0.001:ω_max
p_range = map(ω -> mean_power_flow_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), ω_range)

Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega$")

Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P - α ω$")
# Makie.plot!(ax, ω_range, p_range)
Makie.lines!(ax, ω_range, p_range, label = L"numerical $p$")

Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))

leg = axislegend(position = :rt)
Fig
save(joinpath(plot_path, "grid_1_node_74_self_consistency_intersection.pdf"), Fig)


# with network modes
network_modes = sqrt.(reduced_eigenvalues[2:end] .- α^2/4)
network_modes_weights = amplitude_factors[2:end] ./ maximum(amplitude_factors[2:end])

for l in 1:length(network_modes)
    Makie.vlines!(sign(P_solitary) * network_modes[l], color=(:red, network_modes_weights[l]), linewidth=1)
end
Fig
save(joinpath(plot_path, "grid_1_node_74_self_consistency_intersection_modes.pdf"), Fig)

# with numerical solitary frequencies
tail_statistics = FileIO.load(joinpath(data_path_local, "tail_statistics_grid_"*string(grid_idx)*"_200t_50s_only_dw.jld"))
mean_ωs = tail_statistics["mean_ωs"]
mean_ωs[:,:,:,one_solitary_idx]

Makie.vlines!(ax, vcat(mean_ωs[:,:,:,one_solitary_idx]...) , color=(:green, 0.5), linestyle=:dash)
Fig
save(joinpath(plot_path, "grid_1_node_74_self_consistency_intersection_modes_sparse_numerics.pdf"), Fig)

# use denser (but suspicious) data, has to be checked
Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega$")
Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range, label=L"$P - α ω$")
Makie.lines!(ax, ω_range, p_range, label = L"numerical $p$")
Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
leg = axislegend(position = :rt)
network_modes = sqrt.(reduced_eigenvalues[2:end] .- α^2/4)
network_modes_weights = amplitude_factors[2:end] ./ maximum(amplitude_factors[2:end])
for l in 1:length(network_modes)
    Makie.vlines!(sign(P_solitary) * network_modes[l], color=(:red, network_modes_weights[l]), linewidth=1)
end
tail_statistics = FileIO.load(joinpath(@__DIR__,"../../data/random_pgs/", "tail_statistics_grid_"*string(grid_idx)*"_200t_50s_10dphi_120dw.jld"))
mean_ωs = tail_statistics["mean_ωs"]
mean_ωs[:,:,:,one_solitary_idx]
Makie.vlines!(ax, vcat(mean_ωs[:,:,:,one_solitary_idx]...) , color=(:green, 0.5), linestyle=:dash)
Fig
save(joinpath(plot_path, "grid_1_node_74_self_consistency_intersection_modes_dense_numerics.pdf"), Fig)


# only show solitary frequencies and make markers instead of vlines
# also: legend in outer region
one_solitaries_data = FileIO.load(joinpath(@__DIR__,"../../data/random_pgs/", "solitary_frequency_data_grid_"*string(grid_idx)*name*".jld"))
where_one_solitaries_with_node_i_idx = one_solitaries_data["idxs"]
one_solitaries_mean_ωs = one_solitaries_data["ωs"]

one_solitaries_node_i_idxs = index_vectors(where_one_solitaries_with_node_i_idx)[4]
numerical_ωs_thisnode = one_solitaries_mean_ωs[findall(x -> isequal(x, one_solitary_idx), one_solitaries_node_i_idxs)]

Fig = Makie.Figure(resolution=size_pt, fontsize=scaler*12);
ax = Makie.Axis(Fig[1,1], xlabel=L"$\omega$", ylabel=L"$ $power")
straight_line = Makie.lines!(ax, ω_range, P_solitary .- α_solitary .* ω_range)
p_curve = Makie.lines!(ax, ω_range, p_range)
Makie.xlims!(ax, [ω_min, ω_max])
Makie.ylims!(ax, sort(P_solitary .- α_solitary.* [ω_min, ω_max]))
# leg = axislegend(position = :rt)
network_modes = sqrt.(reduced_eigenvalues[2:end] .- α^2/4)
network_modes_weights = amplitude_factors[2:end] ./ maximum(amplitude_factors[2:end])
for l in 1:length(network_modes)
    Makie.vlines!(sign(P_solitary) * network_modes[l], color=(:red, network_modes_weights[l]), linewidth=1, linestyle = :dash)
end
modes_vlines = Makie.vlines!([], color=(:red, 1), linewidth=1, linestyle = :dash)
numerics_scatter = Makie.scatter!(ax, numerical_ωs_thisnode, P_solitary .- α_solitary .* numerical_ωs_thisnode,
    color=(:green, 0.5), marker=:xcross)
leg = Makie.Legend(Fig[1,2], [straight_line, p_curve, modes_vlines, numerics_scatter], [L"$P - α ω$", L"numerical $p$", L"$ $network modes", L"numerical $ω_s$"])
Fig
save(joinpath(plot_path, "grid_1_node_74_self_consistency_intersection_modes_dense_numerics_only_solitaries.pdf"), Fig)





# evaluation of solutions and stability
one_solitary_solutions = find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, P_solitary, α, α_solitary, N, N_slacks), ω_min, ω_max)
one_solitary_stabilities = map(ω -> self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), one_solitary_solutions)
stable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities)]
stable_one_solitary_solutions_physical = stable_one_solitary_solutions[abs.(stable_one_solitary_solutions) .> 10*eps(Float64)]

# technically, 0 should not be a solution, because the analytic function diverges,
# but it is correct, since the synchronous state is a degenerated solitary state.


# scan whole network
one_solitary_idxs = 1:N

all_one_solitary_solutions = Vector{Vector{Float64}}(undef, N)
all_one_solitary_stabilities = Vector{Vector{Float64}}(undef, N)
all_stable_one_solitary_solutions = Vector{Vector{Float64}}(undef, N)
all_stable_one_solitary_solutions_physical = Vector{Vector{Float64}}(undef, N)

for one_solitary_idx in one_solitary_idxs
    α_solitary = pg.nodes[one_solitary_idx].D
    P_solitary = pg.nodes[one_solitary_idx].P
    reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, [one_solitary_idx])
    ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])
    one_solitary_solutions = find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, P_solitary, α, α_solitary, N, N_slacks), ω_min, ω_max)
    one_solitary_stabilities = map(ω -> self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, ω, m, α, α_solitary, N, N_slacks), one_solitary_solutions)
    stable_one_solitary_solutions = one_solitary_solutions[findall(one_solitary_stabilities)]
    stable_one_solitary_solutions_physical = stable_one_solitary_solutions[abs.(stable_one_solitary_solutions) .> 10*eps(Float64)]
    all_one_solitary_solutions[one_solitary_idx] = one_solitary_solutions
    all_one_solitary_stabilities[one_solitary_idx] = one_solitary_stabilities
    all_stable_one_solitary_solutions[one_solitary_idx] = stable_one_solitary_solutions
    all_stable_one_solitary_solutions_physical[one_solitary_idx] = stable_one_solitary_solutions_physical
end

Makie.vlines(length.(all_stable_one_solitary_solutions_physical))

all_stable_one_solitary_solutions_physical

all_stable_one_solitary_solutions_physical[74]