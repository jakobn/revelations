using FileIO
using JLD
using Distributed
# using OrdinaryDiffEq
# using PowerDynamics

include(joinpath(@__DIR__,"../../src/network_generation.jl"))
include(joinpath(@__DIR__,"../../src/helper_functions.jl"))
include(joinpath(@__DIR__,"../../src/simulation_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../ensembles")
ensemble_path_local = joinpath(ensemble_path,"random_pgs")
data_path = joinpath(@__DIR__,"../data")
data_path_local = joinpath(data_path, "../random_pgs")
plot_path = joinpath(@__DIR__,"../plots")

# only dω
# 200 time units simulation time sufficient (see plots of trajectories)
# try different t_stat

t_sim = (0.,200.) #nomenclature 200t (transient), 20s (statistics)
t_stat = 50. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/10000 : t_sim[2]
abstol = 1e-3
reltol = 1e-3


topology_data = JLD.load(joinpath(ensemble_path_local,"topology_data.jld"))
generation_parameters = JLD.load(joinpath(ensemble_path_local,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

n_initial_ϕ = 1
n_initial_ω = 10
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

node_idx_range = 1:N

@time for grid_idx in 1:1 # 1:n_grids_later

    pg, RHS, op = find_power_grid_path(ensemble_path_local, grid_idx)

    # dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic
    mean_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))
    std_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))

    # plot_idxs = collect(1:N)

    #@distributed
    for (dϕ_idx,  dω_idx, node_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
        dϕ = perturbation_range_ϕ[dϕ_idx]
        dω = perturbation_range_ω[dω_idx] + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
        solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
        # pg_solution = PowerGridSolution(solution, pg)
        # myplot = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)
        # display(myplot)
        mean_ωs[node_idx, dϕ_idx, dω_idx, :], std_ωs[node_idx, dϕ_idx, dω_idx, :] = calculate_statistics_ω(solution, t_stat_iter)
        println(dω_idx, node_idx)
    end

    FileIO.save(joinpath(data_path, "tail_statistics_grid_"*string(grid_idx)*"_200t_50s_only_dw.jld"), "mean_ωs", mean_ωs, "std_ωs", std_ωs)

    FileIO.save(joinpath(data_path, "simulation_parameters_grid_"*string(grid_idx)*"_200t_50s_only_dw.jld"),
        "perturbation_range_ϕ", perturbation_range_ϕ, "perturbation_range_ω", perturbation_range_ω,
        "t_sim", t_sim, "t_stat_iter", t_stat_iter, "node_idx_range", node_idx_range,
        "abstol", abstol, "reltol", reltol)

end

# Rodas4(): 5000 to 6000 s for one grid, all nodes, 1 ϕ, 10 ω
# Tsit5(): 270 s
