using PowerDynamics
using SyntheticNetworks
using TreeNodeClassification
using StatsBase
using Graphs
using LinearAlgebra
# using ForwardDiff
using DifferentialEquations # or # using OrdinaryDiffEq
using Roots
using IntervalRootFinding
using IntervalArithmetic


include(joinpath(@__DIR__,"../src/network_generation.jl"))
include(joinpath(@__DIR__,"../src/laplacian.jl"))
include(joinpath(@__DIR__,"../src/self_consistency.jl"))
# initialize parameters

node_type = SwingEqLVS
# this is fixed in random_PD_grid at the moment

H = .5; P = 1.; D = .1; Ω = 1.; Γ = 1.; V = 1.;
node_parameters_dict = Dict(:H=>H, :P=>P, :D=>D, :Ω=>Ω, :Γ=>Γ, :V=>V)
K = 6.

N = 100
pg, op = random_PD_grid(N, node_parameters_dict, K)

#if true
#    """
    # test generation

    P_vec = ones(N)
    turn_idx = sample(1:N, N÷2, replace=false)
    for n in turn_idx
        P_vec[n] = -1
    end
    P_vec = P .* P_vec
    @assert sum(P_vec)==0 "power imbalance!"

    RPG = RandomPowerGrid(N, 1, 1/5, 3/10, 1/3, 1/10, 0.0)
    pg_graph = generate_graph(RPG)      
    e = Graphs.edges(pg_graph)

    # collects all edges and turns them into StaticLines
    from_vec = Graphs.src.(e)
    to_vec = Graphs.dst.(e)

    lines = Array{Any}(undef, length(e))
    nodes = Array{Any}(undef, Graphs.nv(pg_graph))

    for l in 1:length(e)
        lines[l] = StaticLine(from = from_vec[l], to = to_vec[l], Y = -1im*K) 
    end
    for n in 1:Graphs.nv(pg_graph)
        nodes[n] = node_type(;node_parameters_dict...,P=P_vec[n])
    end
    newnodes = nodes
    pg = PowerGrid(newnodes, lines)
    # op = find_operationpoint(pg, sol_method=:rootfind)
    op = find_operationpoint(pg, sol_method=:nlsolve)

    # check_eigenvalues

    rpg = rhs(pg)

    #using DifferentialEquations
    using OrdinaryDiffEq

    problem = ODEProblem(rhs(pg), op.vec, (0.,25.))
    solution = solve(problem, Rodas4())
    solve(problem, Tsit5())

    #alternatively (but this does not return a "success" if successfull)
    # simulate(ChangeInitialConditions(node=1, var=:φ, f=identity), pg, op, (0., 25.))

    all(isapprox.(op[:, :v], 1.0, atol=0.01))
    all(isapprox.(op[:, :ω], 0, atol=1e-9))
    _, stable = check_eigenvalues_MM(pg, op)
    stable
    sol = simulate_from_state(pg, op.vec, (0.,25.))
    SciMLBase.successful_retcode(sol)
    """
    # obtain reduced Laplacian, eigenvectors and eigenvalues
    """
    phases = op[:,:φ]
    pg.nodes
    pg.nodes[1]
    typeof(pg.nodes[1]) == SwingEqLVS || typeof(pg.nodes[1]) == SwingEq || typeof(pg.nodes[1]) == SlackAlgebraic
    any(typeof(pg.nodes[1]) .== [SwingEqLVS, SwingEq, SlackAlgebraic])
    all(any.(typeof.(pg.nodes) .== [SwingEqLVS, SwingEq, SlackAlgebraic]) )
    typeof.(pg.nodes) .== [SwingEqLVS, SwingEq, SlackAlgebraic]
    """

    """
    # make coupling matrix (with zero diagonal)
    K_matrix = zeros(N, N)
    for line in pg.lines
        from_idx = line.from
        to_idx = line.to
        K = -imag(line.Y)
        K_matrix[from_idx,to_idx] = K
        K_matrix[to_idx,from_idx] = K
    end
    K_matrix

    # make Laplacian
    Laplacian = zeros(N, N)
    for line in pg.lines
        from_idx = line.from
        to_idx = line.to
        K = -imag(line.Y)
        Laplacian[from_idx,to_idx] = -K * cos(phases[from_idx] - phases[to_idx])
        Laplacian[to_idx,from_idx] = -K * cos(phases[from_idx] - phases[to_idx])
    end
    # diagonal
    for i in 1:N
        entry = 0
        for j in 1:N
            entry -= Laplacian[i,j]
        end
        Laplacian[i,i] = entry
    end
    Laplacian

    # test symmetry and sum property (up to precision)
    transpose(Laplacian) == Laplacian
    all(isapprox.(sum(Laplacian; dims=2), 0, atol=1e-14))

check_Laplacian_properties(Laplacian)

    # how to reduce
    # 1. add row or column z to diagonal (add diagonal matrix or entry by entry)
    # 2. remove row and column z

    solitary_idxs = [1,2]

    Laplacian_corrected_diagonal = Laplacian
    for solitary_idx in solitary_idxs
        Laplacian_corrected_diagonal += diagm((Laplacian[solitary_idx,:]))
    end

    reduced_idxs = setdiff(1:size(Laplacian,1), solitary_idxs)
    reduced_Laplacian = Laplacian_corrected_diagonal[reduced_idxs, reduced_idxs]

    function check_Laplacian_properties(Laplacian)
        @assert transpose(Laplacian) == Laplacian "Laplacian is not symmetric!"
        @assert all(isapprox.(sum(Laplacian; dims=2), 0, atol=1e-14)) "sum over row is not zero!"
    end

    check_Laplacian_properties(Laplacian)
    check_Laplacian_properties(reduced_Laplacian)

    reduced_Laplacian == reduce_Laplacian(Laplacian, solitary_idxs)

#   """
# end

# test toy model network generation and reduced Laplacians
n = 7
## star graph
pg, op = star_graph_toy_model(n, node_parameters_dict, K)
Laplacian = steady_state_Laplacian(pg, op)
reduced_Laplacian = reduce_Laplacian(Laplacian, [1])

## all-to-all
pg, op = all_to_all_toy_model(n, node_parameters_dict, K)
Laplacian = steady_state_Laplacian(pg, op)
reduced_Laplacian = reduce_Laplacian(Laplacian, [1])

## two-node toy model
pg, op = two_node_toy_model(n, node_parameters_dict, K)
Laplacian = steady_state_Laplacian(pg, op)
reduced_Laplacian = reduce_Laplacian(Laplacian, [1])

# test network reduction and Laplacian in complex case
pg, op = random_PD_grid(N, node_parameters_dict, K)
solitary_idxs = [2,3]
reduced_idxs = setdiff(1:N, solitary_idxs)

Laplacian = steady_state_Laplacian(pg, op)
reduced_Laplacian = reduce_Laplacian(Laplacian, solitary_idxs)


# test corotating_frequency after fix
# should be solitary_frequency if N_slacks > 0
# synchronization_frequency should be 0 if N_slacks > 0
α = 0.1
α_solitary = 0.1
N = 9
solitary_frequency = 6.1

N_slacks = 1
corotating_frequency(solitary_frequency, α, α_solitary, N, N_slacks)
synchronization_frequency(solitary_frequency, α, α_solitary, N, N_slacks)

N_slacks = 0
corotating_frequency(solitary_frequency, α, α_solitary, N, N_slacks)
synchronization_frequency(solitary_frequency, α, α_solitary, N, N_slacks)


# self-consistency equation

## test toy model case where results are known analytically
n = 7
pg, op = two_node_toy_model(n, node_parameters_dict, K)
solitary_idxs = [1]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, solitary_idxs)

N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))


##
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, solitary_idxs)

α = D
m = 2*H
α_solitary = α
m_solitary = m
P_solitary = P

self_consistency_equation_1_solitary(reduced_eigenvalues, amplitude_factors, solitary_frequency, P_solitary, α, α_solitary, N, N_slacks)

self_consistency_stability_1_solitary(reduced_eigenvalues, amplitude_factors, solitary_frequency, α, α_solitary, N, N_slacks)

# test several solitary_frequencies
N = 100
pg, op = random_PD_grid(N, node_parameters_dict, K)
N_slacks = length(findall(node -> typeof(node)==SlackAlgebraic, pg.nodes))

solitary_idxs = [2,3]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, solitary_idxs)
all_P_solitary = broadcast(x -> x.P, pg.nodes[solitary_idxs])
all_α_solitary = broadcast(x -> x.D, pg.nodes[solitary_idxs])

solitary_frequencies = [10, -5]

self_consistency_equation_several_solitaries(reduced_eigenvalues, all_amplitude_factors, solitary_frequencies, all_P_solitary, α, all_α_solitary, N, N_slacks)



# same result for 1_solitary and several_solitary functions if only one solitary? yes!
solitary_idxs = [1]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, solitary_idxs)
# test preparation: works for solitary_idx as Integer or Array
prepare_self_consistency_equation(pg, op, [1]) == prepare_self_consistency_equation(pg, op, 1)

solitary_frequency = 5.
solitary_frequencies = [solitary_frequency]

self_consistency_equation_1_solitary(reduced_eigenvalues, all_amplitude_factors[1], solitary_frequency, all_P_solitary[1], α, all_α_solitary[1], N, N_slacks)
self_consistency_equation_several_solitaries(reduced_eigenvalues, all_amplitude_factors, [solitary_frequency], all_P_solitary, α, all_α_solitary, N, N_slacks)

# find solutions (for 1)
solitary_idxs = [1]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, solitary_idxs)
all_P_solitary = broadcast(x -> x.P, pg.nodes[solitary_idxs])
all_α_solitary = broadcast(x -> x.D, pg.nodes[solitary_idxs])
P_solitary = all_P_solitary[1]
α_solitary = all_α_solitary[1]
ω_min, ω_max = 1.1 .* sort([P_solitary/α_solitary, -sign(P_solitary)])

self_consistency_equation_several_solitaries(reduced_eigenvalues, all_amplitude_factors, [3], all_P_solitary, α, all_α_solitary, N, N_slacks)

find_zeros(ω -> self_consistency_equation_several_solitaries(reduced_eigenvalues, all_amplitude_factors, [ω], all_P_solitary, α, all_α_solitary, N, N_slacks)[1], ω_min, ω_max)

# note the "[1]" not necessary here
find_zeros(ω -> self_consistency_equation_1_solitary(reduced_eigenvalues, all_amplitude_factors[1], ω, all_P_solitary[1], α, all_α_solitary[1], N, N_slacks)[1], ω_min, ω_max)

# does not work like that
roots(ω -> self_consistency_equation_several_solitaries(reduced_eigenvalues, all_amplitude_factors, [ω], all_P_solitary, α, all_α_solitary, N, N_slacks)[1], ω_min..ω_max)

# find solutions (for 2)
solitary_idxs = [2,3]
reduced_eigenvalues, all_amplitude_factors = prepare_self_consistency_equation(pg, op, solitary_idxs)
all_P_solitary = broadcast(x -> x.P, pg.nodes[solitary_idxs])
all_α_solitary = broadcast(x -> x.D, pg.nodes[solitary_idxs])
intervals = IntervalBox(broadcast((x,y) -> sort([x,y]), all_P_solitary./all_α_solitary, -sign.(all_P_solitary)))


solitary_solutions = roots(ω -> self_consistency_equation_several_solitaries(reduced_eigenvalues, all_amplitude_factors, ω, all_P_solitary, α, all_α_solitary, N, N_slacks)[1], intervals)
println(solitary_solutions)

findall([true, false])

findall(x -> isequal.(x,1), [[1,2],[3,4]])

typeof(64) == Int64

typeof(pg)
typeof(op)

prepare_self_consistency_equation(pg, op, [1]) == prepare_self_consistency_equation(pg, op, 1)

eigenvalues, amplitude_factors = prepare_self_consistency_equation(pg, op, [1])

amplitude_factors

eigenvalues

typeof(6.12) == Float64